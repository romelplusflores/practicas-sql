# practicas SQL

## estudiante: teddy romel flores ortega 


# curso: database programing with SQL
[1. crear tablas (create tabla - all_tables - drop table)](solucionario/solucionario1-sql.md)

[2. Ingresar registros (insert into- select)](solucionario/solucionario2-sql.md)

[3. Tipos de datos ](solucionario/solucionario3-sql.md)

[4. Recuperar algunos campos (select)](solucionario/solucionario4-sql.md)

[5. Recuperar algunos registros (where)](solucionario/solucionario5-sql.md)

[6. Operadores relacionales](solucionario/solucionario6-sql.md)

[7. Borrar registros (delete)](solucionario/solucionario7-sql.md)

[8. Actualizar registros (update)](solucionario/solucionario8-sql.md)

[9. Comentarios](solucionario/solucionario9-sql.md)

[10. Valores nulos (null)](solucionario/solucionario10-sql.md)

[11. Operadores relacionales (is null)](solucionario/solucionario11-sql.md)

[12. Clave primaria (primary key)](solucionario/solucionario12-sql.md)

[13. Vaciar la tabla (truncate table)](solucionario/solucionario13-sql.md)

[14. Tipos de datos alfanuméricos](solucionario/solucionario14-sql.md)

[15. Tipos de datos numéricos](solucionario/solucionario15-sql.md)

[16. Ingresar algunos campos](solucionario/solucionario16-sql.md)

[17. Valores por defecto (default)](solucionario/solucionario17-sql.md)

[18. Operadores aritméticos y de concatenación (columnas calculadas)](solucionario/solucionario18-sql.md)

[19.  Alias (encabezados de columnas)](solucionario/solucionario19-sql.md)

[20. Funciones string](solucionario/solucionario20-sql.md)

[21. Funciones matemáticas](solucionario/solucionario21-sql.md)

[22. Funciones de fechas y horas](solucionario/solucionario22-sql.md)

[23. Ordenar registros (order by)](solucionario/solucionario23-sql.md)

[24. Operadores lógicos (and - or - not)](solucionario/solucionario24-sql.md)

[25. Otros operadores relacionales (between)](solucionario/solucionario25-sql.md)

[26. Otros operadores relacionales (in)](solucionario/solucionario26-sql.md)

[27. Búsqueda de patrones (like - not like)](solucionario/solucionario27-sql.md)

[28. Contar registros (count)](solucionario/solucionario28-sql.md)

[29. Funciones de grupo (count - max - min - sum - avg)](solucionario/solucionario29-sql.md)

[30. Agrupar registros (group by)](solucionario/solucionario30-sql.md)

[31. Seleccionar grupos (Having)](solucionario/solucionario31-sql.md)

[32. Registros duplicados (Distinct)](solucionario/solucionario32-sql.md)

[33. Clave primaria compuesta](solucionario/solucionario33-sql.md)

[34. Secuencias (create sequence - currval - nextval - drop sequence)](solucionario/solucionario34-sql.md)

[35. Alterar secuencia (alter sequence)](solucionario/solucionario35-sql.md)

[36. Integridad de datos](solucionario/solucionario36-sql.md)

[37. Restricción primary key](solucionario/solucionario37-sql.md)

[38. Restricción unique](solucionario/solucionario38-sql.md)

[39. Restriccioncheck](solucionario/solucionario39-sql.md)

[40. Restricciones: validación y estados (validate - novalidate - enable - disable)](solucionario/solucionario40-sql.md)

[41. Restricciones: información (user_constraints - user_cons_columns)](solucionario/solucionario41-sql.md)

[42. Restricciones: eliminación (alter table - drop constraint)](solucionario/solucionario42-sql.md)

[43. Indices](solucionario/solucionario43-sql.md)

[44. Indices (Crear . Información)](solucionario/solucionario44-sql.md)

[45. Indices (eliminar)](solucionario/solucionario45-sql.md)

[46. Varias tablas (join)](solucionario/solucionario46-sql.md)

[47. Combinación interna (join)](solucionario/solucionario47-sql.md)

[48. Combinación externa izquierda (left join)](solucionario/solucionario48-sql.md)

[49. Combinación externa derecha (right join)](solucionario/solucionario49-sql.md)

[50. Combinación externa completa (full join)](solucionario/solucionario50-sql.md)

[51. Combinaciones cruzadas (cross)](solucionario/solucionario51-sql.md)

[52. Autocombinación](solucionario/solucionario52-sql.md)

[53. Combinaciones y funciones de agrupamiento](solucionario/solucionario53-sql.md)

[54. Combinar más de 2 tablas](solucionario/solucionario54-sql.md)

[55. Otros tipos de combinaciones](solucionario/solucionario55-sql.md)

[56. Clave foránea](solucionario/solucionario56-sql.md)

[57. Restricciones (foreign key)](solucionario/solucionario57-sql.md)

[58. Restricciones foreign key en la misma tabla](solucionario/solucionario58-sql.md)

[59. Restricciones foreign key (eliminación)](solucionario/solucionario59-sql.md)

[60. Restricciones foreign key deshabilitar y validar](solucionario/solucionario60-sql.md)

[61. Restricciones foreign key (acciones)](solucionario/solucionario61-sql.md)

[62. Información de user_constraints](solucionario/solucionario62-sql.md)

[63. Restricciones al crear la tabla](solucionario/solucionario63-sql.md)

[64. Unión](solucionario/solucionario64-sql.md)

[65. Intersección](solucionario/solucionario65-sql.md)

[66. Minus](solucionario/solucionario66-sql.md)

[67. Agregar campos (alter table-add)](solucionario/solucionario67-sql.md)

[68. Modificar campos (alter table - modify)](solucionario/solucionario68-sql.md)

[69. Eliminar campos (alter table - drop)](solucionario/solucionario69-sql.md)

[70.  Agregar campos y restricciones (alter table)](solucionario/solucionario70-sql.md)

[71. Subconsultas](solucionario/solucionario71-sql.md)

[72. Subconsultas como expresion](solucionario/solucionario72-sql.md)

[73. Subconsultas con in](solucionario/solucionario73-sql.md)

[74. Subconsultas any- some - all](solucionario/solucionario74-sql.md)

[75. Subconsultas correlacionadas](solucionario/solucionario75-sql.md)

[76. Exists y No Exists](solucionario/solucionario76-sql.md)

[77. Subconsulta simil autocombinacion](solucionario/solucionario77-sql.md)

[78. Subconsulta conupdate y delete](solucionario/solucionario78-sql.md)

[79. Subconsulta e insert](solucionario/solucionario79-sql.md)

[80. Crear tabla a partir de otra (create table-select)](solucionario/solucionario80-sql.md)

[81. Vistas (create view)](solucionario/solucionario81-sql.md)

[82. Vistas (información)](solucionario/solucionario82-sql.md)

[83. Vistas eliminar (drop view)](solucionario/solucionario83-sql.md)

[84. Vistas (modificar datos a través de ella)](solucionario/solucionario84-sql.md)

[85. Vistas (with read only)](solucionario/solucionario85-sql.md)

[86. Vistas modificar (create or replace view)](solucionario/solucionario86-sql.md)

[87. Vistas (with check option)](solucionario/solucionario87-sql.md)

[88. Vistas (otras consideraciones: force)](solucionario/solucionario88-sql.md)

[89. Vistas materializadas (materialized view)](solucionario/solucionario89-sql.md)

[90. Procedimientos almacenados](solucionario/solucionario90-sql.md)

[91. Procedimientos Almacenados (crear- ejecutar)](solucionario/solucionario91-sql.md)

[92. Procedimientos Almacenados (eliminar)](solucionario/solucionario92-sql.md)

[93. Procedimientos almacenados (parámetros de entrada)](solucionario/solucionario93-sql.md)

[94. Procedimientos almacenados (variables)](solucionario/solucionario94-sql.md)

[95. Procedimientos Almacenados (informacion)](solucionario/solucionario95-sql.md)

[96. Funciones](solucionario/solucionario96-sql.md)

[97. Control de flujo (if)](solucionario/solucionario97-sql.md)

[98. Control de flujo (case)](solucionario/solucionario98-sql.md)

[99. Control de flujo (loop)](solucionario/solucionario99-sql.md)

[100. Control de flujo (for)](solucionario/solucionario100-sql.md)

[101. Control de flujo (while loop)](solucionario/solucionario101-sql.md)

[102. Disparador (trigger)](solucionario/solucionario102-sql.md)

[103. Disparador (información)](solucionario/solucionario103-sql.md)

[104. Disparador de inserción a nivel de sentencia](solucionario/solucionario104-sql.md)

[105. Disparador de insercion a nivel de fila (insert trigger for each row)](solucionario/solucionario105-sql.md)

[106. Disparador de borrado (nivel de sentencia y de fila)](solucionario/solucionario106-sql.md)

[107. Disparador de actualizacion a nivel de sentencia (update trigger)](solucionario/solucionario107-sql.md)

[108. Disparador de actualización a nivel de fila (update trigger)](solucionario/solucionario108-sql.md)

[109. Disparador de actualización - lista de campos (update trigger)](solucionario/solucionario109-sql.md)

[110. Disparador de múltiples eventos](solucionario/solucionario110-sql.md)

[111. Disparador (old y new)](solucionario/solucionario111-sql.md)

[112. Disparador condiciones (when)](solucionario/solucionario112-sql.md)

[113. Disparador de actualizacion - campos (updating)](solucionario/solucionario113-sql.md)

[114. Disparadores (habilitar y deshabilitar)](solucionario/solucionario114-sql.md)

[115. Disparador (eliminar)](solucionario/solucionario115-sql.md)

[116. Errores definidos por el usuario en trigger](solucionario/solucionario116-sql.md)

[117. Seguridad y acceso a Oracle](solucionario/solucionario117-sql.md)

[118. Usuarios (crear)](solucionario/solucionario118-sql.md)

[119. Permiso de conexión](solucionario/solucionario119-sql.md)

[120. Privilegios del sistema (conceder)](solucionario/solucionario120-sql.md)

[121. Privilegios del sistema (with admin option)](solucionario/solucionario121-sql.md)

[122. Modelado de base de datos](solucionario/solucionario122-sql.md)
