# Creación de usuario HR y otorgando privilegios 
```sql
CREATE USER teddy2
IDENTIFIED BY teddy2
DEFAULT TABLESPACE USERS
TEMPORARY TABLESPACE TEMP;
ALTER USER teddy2 QUOTA UNLIMITED ON USERS;
GRANT CREATE SESSION, CREATE VIEW TO FRANK;
GRANT RESOURCE TO teddy2;
ALTER USER teddy2 DEFAULT ROLE RESOURCE;
/ 
-- Conocer que privilegios tiene el usuario FRANK
SELECT * FROM DBA_SYS_PRIVS
WHERE GRANTEE = 'FRANK';
/
-- Conocer que rol tiene el usuario FRANK
SELECT * FROM DBA_ROLE_PRIVS
WHERE GRANTEE = 'FRANK';
/
-- Conocer que privilegios tiene un ROL
SELECT * FROM DBA_SYS_PRIVS
WHERE GRANTEE = 'RESOURCE';

SELECT * FROM DBA_SYS_PRIVS
WHERE GRANTEE = 'CONNECT';

-- Script de creación del usuario HR usando Oracle XE (BD en el localhost)
CREATE USER HR                           -- Se le asigna un nombre de usuario
IDENTIFIED BY HR                         -- Se le asigna una contraseña (No es necesario usar doble comillas para hacerla case sensitive)
DEFAULT TABLESPACE USERS                 -- En Oracle XE se usa el tablespace USERS para almacenar los objetos de los usuarios
TEMPORARY TABLESPACE TEMP;               -- Asignar un tablespace temporal para los datos temporales de la sesión del usuario
ALTER USER HR QUOTA UNLIMITED ON USERS;  -- Asignar una couta de almacenamiento en el tablespace USERS
GRANT CREATE SESSION TO HR;              -- Privilegio que permite que el usuario se pueda conectar a la BD (Iniciar Sesión)
GRANT CREATE VIEW TO HR;                 -- Privilegio de sistema que permite crear vistas (necesario para ejecutar script hr.sql)
GRANT RESOURCE TO HR;                    -- Asignar el rol "RESOURCE" (permite crear TABLE, SEQUENCE, TRIGGER, PROCEDURE, etc.)
ALTER USER HR DEFAULT ROLE RESOURCE;     -- Se le asigna RESOURCE como el rol por defecto

   
--* Script para desbloquear el usuario HR en Oracle XE 18c
ALTER USER HR IDENTIFIED BY HR ACCOUNT UNLOCK;

ALTER SESSION SET NLS_DATE_FORMAT = 'DD-MM-YYYY';
ALTER SESSION SET NLS_DATE_FORMAT = 'DD-MON-YYYY HH24:MI:SS';
ALTER SESSION SET NLS_DATE_LANGUAGE = 'SPANISH';

GRANT CREATE SESSION, CREATE VIEW, CREATE TABLE, ALTER SESSION, CREATE SEQUENCE TO HR;
GRANT CREATE SYNONYM, CREATE DATABASE LINK, RESOURCE, UNLIMITED TABLESPACE TO HR;

```