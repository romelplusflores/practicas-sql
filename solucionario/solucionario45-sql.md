# [45. Indices (eliminar)](solucionario/solucionario45.md)
# solucionario numero 45 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table alumnos;

create table alumnos(
    legajo char(5) not null,
    documento char(8) not null,
    nombre varchar2(30),
    curso char(1) not null,
    materia varchar2(20) not null,
    notafinal number(4,2)
);

```
## 2
```sql
 create index I_alumnos_nombre
  on alumnos(nombre);
```
## 3
```sql
 alter table alumnos
  add constraint PK_alumnos_legajo
  primary key (legajo);
```
## 4
```sql
 select constraint_name, constraint_type, index_name
  from user_constraints
  where table_name='ALUMNOS';
```
## 5
```sql
 select index_name,uniqueness
  from user_indexes
  where table_name='ALUMNOS';
```
## 6
```sql
 drop index PK_alumnos_legajo;

```
## 7
```sql
 create unique index I_alumnos_documento
  on alumnos(documento);
```
## 8
```sql
 alter table alumnos
  add constraints UQ_alumnos_documento
  unique (documento);

   select constraint_name, constraint_type, index_name
  from user_constraints
  where table_name='ALUMNOS';
```
## 9
```sql
 drop index I_alumnos_documento;

```
## 10
```sql
 alter table alumnos
  drop constraint UQ_ALUMNOS_DOCUMENTO;
```
## 11
```sql
select index_name,uniqueness
  from user_indexes
  where table_name='ALUMNOS';
```
## 12
```sql
 drop index I_alumnos_documento;

```
## 13
```sql
 drop index I_alumnos_nombre;

```
## 14
```sql
 alter table alumnos
 drop constraint PK_ALUMNOS_LEGAJO;
```
## 15
```sql
 select index_name,uniqueness
  from user_indexes
  where table_name='ALUMNOS';

```
## 16
```sql
 create index I_alumnos_cursomateria
  on alumnos(curso,materia);
```
## 17
```sql
 select index_name,uniqueness
  from user_indexes
  where table_name='ALUMNOS';
```
## 18
```sql
 drop table alumnos;
 
 select index_name,uniqueness
  from user_indexes
  where table_name='ALUMNOS';

```