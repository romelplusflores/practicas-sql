# [112. Disparador condiciones (when)](solucionario/solucionario112.md)
# solucionario numero 112 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table empleados;

```
## 2
```sql
create table empleados(
    documento char(8),
    apellido varchar2(20),
    nombre varchar2(20),
    seccion varchar2(30),
    sueldo number(8,2)
);

```
## 3
```sql
insert into empleados values('22333444','ACOSTA','Ana','Secretaria',500);
insert into empleados values('22555666','CASEROS','Carlos','Contaduria',900);
insert into empleados values('22777888','DOMINGUEZ','Daniel','Secretaria',560);
insert into empleados values('22999000','FUENTES','Federico','Sistemas',680);
insert into empleados values('23444555','GOMEZ','Gabriela','Sistemas',1200);
insert into empleados values('23666777','JUAREZ','Juan','Contaduria',1000);

```
## 4
```sql
drop table control;
create table control(
    usuario varchar2(30),
    fecha date,
    documento char(8),
    antiguosueldo number(8,2),
    nuevosueldo number(8,2)
);

```
## 5
```sql
 create or replace trigger tr_aumentar_sueldo_empleados
  before update of sueldo
  on empleados
  for each row when(new.sueldo>old.sueldo)
  begin
   insert into control values(user,sysdate,:old.documento,:old.sueldo,:new.sueldo);
  end;
```
## 6
```sql
 update empleados set sueldo=1000 where seccion='Sistemas';

```
## 7
```sql
 select *from control;

```
## 8
```sql
 update empleados set seccion='Contaduria' where documento='22333444';

```
## 9
```sql
 select *from control;

```
## 10
```sql
 create or replace trigger tr_ingresar_empleados
  before insert
  on empleados
  for each row
  begin
   :new.apellido := upper(:new.apellido);
   if (:new.sueldo is null) then
    :new.sueldo := 0;
   end if;
  end;
```
## 11
```sql
 insert into empleados values('25666777','Lopez','Luisa','Secretaria',650);

```
## 12
```sql
 select *from empleados where documento='25666777';

```
## 13
```sql
 insert into empleados (documento,apellido,nombre, seccion) values('26777888','Morales','Marta','Secretaria');
 insert into empleados values('26999000','Perez','Patricia','Contaduria',null);
```
## 14
```sql
 select *from empleados;

```
## 15
```sql
create or replace trigger tr_ingresar_empleados
  before insert
  on empleados
  for each row when (new.sueldo>1000 or new.sueldo<500)
  begin
   :new.sueldo := null;
  end tr_ingresar_empleados;
```
## 16
```sql
 select *from empleados where documento='31313131';

```
## 17
```sql
SELECT * FROM control;

```