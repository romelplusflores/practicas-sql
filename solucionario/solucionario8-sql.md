# [8.actualizar registros ](solucionario/solucionario8.md)
# solucionario numero 8 fecha 6/6/2023
## ejercicio numero 1
## 1
```sql
drop table agenda;

create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);
salida del codigo:
Table AGENDA borrado.

Table AGENDA creado.
```
## 2
```sql
insert into agenda (apellido,nombre,domicilio,telefono) values ('Acosta','Alberto','Colon 123','4234567');
insert into agenda (apellido,nombre,domicilio,telefono) values ('Juarez','Juan','Avellaneda 135','4458787');
insert into agenda (apellido,nombre,domicilio,telefono) values ('Lopez','Maria','Urquiza 333','4545454');
insert into agenda (apellido,nombre,domicilio,telefono) values ('Lopez','Jose','Urquiza 333','4545454');
insert into agenda (apellido,nombre,domicilio,telefono) values ('Suarez','Susana','Gral. Paz 1234','4123456');

salida del codigo:
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
```
## 3
```sql
update agenda set nombre= 'Juan Jose' where nombre= 'Juan';

salida del codigo:
1 fila actualizadas.

```
## 4
```sql
update agenda set telefono = '4445566' where telefono = '4545454';

salida del codigo:
2 filas actualizadas.

```
## 5
```sql
update agenda set nombre= 'Juan Jose' where nombre= 'Juan';

salida del codigo:
0 filas actualizadas.

```
## resultado de la tabla
```sql 

APELLIDO NOMBRE     DOMICILIO       TELEFONO
Acosta	Alberto  	Colon 123	    4234567
Juarez	Juan Jose	Avellaneda 135	4458787
Lopez	Maria	    Urquiza 333  	4445566
Lopez	Jose	    Urquiza 333 	4445566
Suarez	Susana	    Gral. Paz 1234	4123456 

```
## EJERCICIO NUMERO 2
## 1
```sql
drop table libros;

create table libros (
    titulo varchar2(30),
    autor varchar2(20),
    editorial varchar2(15),
    precio number(5,2)
);
salida del codigo:
Table LIBROS borrado.

Table LIBROS creado.
```
## 2
```sql
insert into libros (titulo, autor, editorial, precio) values ('El aleph','Borges','Emece',25.00);
insert into libros (titulo, autor, editorial, precio) values ('Martin Fierro','Jose Hernandez','Planeta',35.50);
insert into libros (titulo, autor, editorial, precio) values ('Aprenda PHP','Mario Molina','Emece',45.50);
insert into libros (titulo, autor, editorial, precio) values ('Cervantes y el quijote','Borges','Emece',25);
insert into libros (titulo, autor, editorial, precio) values ('Matematica estas ahi','Paenza','Siglo XXI',15);

salida del codigo:
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
```
## 3
```sql
select *from libros;

salida del codigo:
TITULO                  AUTOR           EDITORIAL PRECIO
El aleph	            Borges	        Emece	  25
Martin Fierro	        Jose Hernandez	Planeta	  35,5
Aprenda PHP	            Mario Molina	Emece	  45,5
Cervantes y el quijote	Borges	        Emece	  25
Matematica estas ahi	Paenza	       Siglo XXI  15
```
## 4
```sql
update libros set autor = 'Adrian Paenza' where autor = 'Paenza';

salida del codigo:
1 fila actualizadas.

```
## 5
```sql
update libros set autor = 'Adrian Paenza' where autor = 'Paenza';

salida del codigo:
0 filas actualizadas.

```
## 6
```sql
update libros set precio = '27' where autor = 'Mario Molina';

salida del codigo:
1 fila actualizadas.

```
## 7
```sql
update libros set editorial = 'Emece S. A.' where editorial = 'Emece';

salida del codigo:
3 filas actualizadas.

```