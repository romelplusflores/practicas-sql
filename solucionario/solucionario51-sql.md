# [51. Combinaciones cruzadas (cross)](solucionario/solucionario51.md)
# solucionario numero 51 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table mujeres;
drop table varones;

create table mujeres(
    nombre varchar2(30),
    domicilio varchar2(30),
    edad number(2)
);

create table varones(
    nombre varchar2(30),
    domicilio varchar2(30),
    edad number(2)
);

```
## 2
```sql
insert into mujeres values('Maria Lopez','Colon 123',45);
insert into mujeres values('Liliana Garcia','Sucre 456',35);
insert into mujeres values('Susana Lopez','Avellaneda 98',41);
insert into varones values('Juan Torres','Sarmiento 755',44);
insert into varones values('Marcelo Oliva','San Martin 874',56);
insert into varones values('Federico Pereyra','Colon 234',38);
insert into varones values('Juan Garcia','Peru 333',50);

```
## 3
```sql
 select m.nombre,m.edad,v.nombre,v.edad
  from mujeres m
  cross join varones v;
```
## 4
```sql
 select m.nombre,m.edad,v.nombre,v.edad
  from mujeres m
  cross join varones v
  where m.edad>40 and
  v.edad>40;
```
## 5
```sql
select m.nombre,m.edad,v.nombre,v.edad
  from mujeres m
  cross join varones v
  where m.edad-v.edad between -10 and 10;
```
