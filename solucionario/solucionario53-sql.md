# [53. Combinaciones y funciones de agrupamiento](solucionario/solucionario53.md)
# solucionario numero 53 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table visitantes;
drop table ciudades;

```
## 2
```sql
create table visitantes(
    nombre varchar2(30),
    edad number(2),
    sexo char(1) default 'f',
    domicilio varchar2(30),
    codigociudad number(2),
    mail varchar(30),
    montocompra decimal (6,2)
);

create table ciudades(
    codigo number(2),
    nombre varchar(20)
);

```
## 3
```sql
insert into ciudades values(1,'Cordoba');
insert into ciudades values(2,'Carlos Paz');
insert into ciudades values(3,'La Falda');
insert into ciudades values(4,'Cruz del Eje');
insert into visitantes values ('Susana Molina', 35,'f','Colon 123', 1, null,59.80);
insert into visitantes values ('Marcos Torres', 29,'m','Sucre 56', 1, 'marcostorres@hotmail.com',150.50);
insert into visitantes values ('Mariana Juarez', 45,'f','San Martin 111',2,null,23.90);
insert into visitantes values ('Fabian Perez',36,'m','Avellaneda 213',3,'fabianperez@xaxamail.com',0);
insert into visitantes values ('Alejandra Garcia',28,'f',null,2,null,280.50);
insert into visitantes values ('Gaston Perez',29,'m',null,5,'gastonperez1@gmail.com',95.40);
insert into visitantes values ('Mariana Juarez',33,'f',null,2,null,90);

```
## 4
```sql
select c.nombre,
  count(*) as cantidad
  from ciudades c
  join visitantes v
  on codigociudad=c.codigo
  group by c.nombre;
```
## 5
```sql
 select c.nombre,sexo,
  avg(montocompra) as "promedio de compra"
  from ciudades c
  join visitantes v
  on codigociudad=c.codigo
  group by c.nombre,sexo;
```
## 6
```sql
 select c.nombre,
  count(mail) as "tienen mail"
  from ciudades c
  join visitantes v
  on codigociudad=c.codigo
  group by c.nombre;
```
## 7
```sql
 select c.nombre,
  max(montocompra)
  from visitantes v
  join ciudades c
  on codigociudad=c.codigo
  group by c.nombre;
```
## 8
```sql
 select c.nombre,
  max(montocompra)
  from visitantes v
  left join ciudades c
  on codigociudad=c.codigo
  group by c.nombre;
```

