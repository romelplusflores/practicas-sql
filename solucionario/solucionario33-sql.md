# [33. Clave primaria compuesta](solucionario/solucionario33.md)
# solucionario numero 33 fecha 14/6/2023 
## ejercicio numero 1

## 1
```sql
drop table consultas;

```
## 2
```sql

```
## 3
```sql
AL.TER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';

```
## 4
```sql
create table consultas(
    fechayhora date not null,
    medico varchar2(30) not null,
    documento char(8) not null,
    paciente varchar2(30),
    obrasocial varchar2(30),
    primary key(fechayhora,medico)
);

```
## 5
```sql
insert into consultas
values ('05/11/2006 8:00','Lopez','12222222','Acosta Betina','PAMI');

insert into consultas
values ('05/11/2006 8:30','Lopez','23333333','Fuentes Carlos','PAMI');

```
## 6
```sql
insert into consultas
values ('05/11/2006 8:00','Perez','34444444','Garcia Marisa','IPAM');

insert into consultas
values ('05/11/2006 8:00','Duarte','45555555','Pereyra Luis','PAMI');

```
## 7
```sql
SELECT COUNT(*) FROM tu_tabla
WHERE fechayhora = '2023-06-14 09:00:00' AND medico = 'Perez';

```
## 8
```sql
insert into consultas
values ('05/11/2006 8:30','Lopez','23333333','Acosta Betina','PAMI');

```
## 9
```sql
UPDATE tu_tabla
SET fechayhora = '2023-06-14 09:30:00'
WHERE medico = 'Acosta' AND paciente = 'Betina' AND fechayhora = '2023-06-14 09:00:00';

```
## 10
```sql
INSERT INTO consultas (fechayhora, medico, documento, paciente, obrasocial)
VALUES (TO_DATE('06/11/2006 10:00', 'DD/MM/YYYY HH24:MI'), 'Perez', '56666666', 'Gonzalez Ana', 'IPAM');

```
## 11
```sql
SELECT *
FROM consultas
WHERE medico = 'Lopez';

```
## 12
```sql
SELECT *
FROM consultas
WHERE fechayhora = TO_DATE('05/11/2006 8:00', 'DD/MM/YYYY HH24:MI');

```
## 13
```sql
SELECT TO_CHAR(fechayhora, 'DD/MM') AS dia_mes
FROM consultas
WHERE medico = 'Lopez';

```
## ejercicio numero 2

## 1
```sql
drop table inscriptos;

```
## 2
```sql

```
## 3
```sql
create table inscriptos(
    documento char(8) not null,
    nombre varchar2(30),
    deporte varchar2(15) not null,
    año date,
    matricula char(1),
    primary key(documento,deporte,año)
);

```
## 4
```sql
SELECT documento, nombre, deporte, DATE_FORMAT(año, '%Y') AS año, matricula
FROM inscriptos;

```
## 5
```sql
insert into inscriptos
values ('12222222','Juan Perez','tenis','2005','s');

insert into inscriptos
values ('23333333','Marta Garcia','tenis','2005','s');

insert into inscriptos
values ('34444444','Luis Perez','tenis','2005','n');

```
## 6
```sql
insert into inscriptos
values ('12222222','Juan Perez','futbol','2005','s');

insert into inscriptos
values ('12222222','Juan Perez','natacion','2005','s');

insert into inscriptos
values ('12222222','Juan Perez','basquet','2005','n');

```
## 7
```sql
insert into inscriptos
values ('12222222','Juan Perez','tenis','2006','s');

insert into inscriptos
values ('12222222','Juan Perez','tenis','2007','s');

```
## 8
```sql
INSERT INTO inscriptos (documento, nombre, deporte, año, matricula)
VALUES ('12345678', 'Nombre Socio', 'Fútbol', '2023-01-01', 'A');

```
## 9
```sql
UPDATE inscriptos
SET documento = '12345679', año = '2022-01-01'
WHERE documento = '12345678' AND deporte = 'Fútbol' AND año = '2022-01-01';

```
## 10
```sql
SELECT nombre, YEAR(año) AS año
FROM inscriptos
WHERE deporte = 'tenis'
LIMIT 5;

```
## 11
```sql
SELECT nombre, deporte
FROM inscriptos
WHERE YEAR(año) = 2005
LIMIT 6;

```
## 12
```sql
SELECT deporte, YEAR(año) AS año
FROM inscriptos
WHERE documento = '12222222'
LIMIT 6;

```