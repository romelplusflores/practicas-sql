# [107. Disparador de actualizacion a nivel de sentencia (update trigger)](solucionario/solucionario107.md)
# solucionario numero 107 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table empleados;
drop table control;

```
## 2
```sql
create table empleados(
    documento char(8),
    apellido varchar2(20),
    nombre varchar2(20),
    seccion varchar2(30),
    sueldo number(8,2)
);

create table control(
    usuario varchar2(30),
    fecha date
);

```
## 3
```sql
insert into empleados values('22333444','ACOSTA','Ana','Secretaria',500);
insert into empleados values('22777888','DOMINGUEZ','Daniel','Secretaria',560);
insert into empleados values('22999000','FUENTES','Federico','Sistemas',680);
insert into empleados values('22555666','CASEROS','Carlos','Contaduria',900);
insert into empleados values('23444555','GOMEZ','Gabriela','Sistemas',1200);
insert into empleados values('23666777','JUAREZ','Juan','Contaduria',1000);

```
## 4
```sql
create or replace trigger tr_actualizar_empleados
  before update
  on empleados
 begin
  insert into control values(user,sysdate);
 end tr_actualizar_empleados;
```
## 5
```sql
 select *from user_triggers where trigger_name ='TR_ACTUALIZAR_EMPLEADOS';

```
## 6
```sql
update empleados set nombre='Graciela' where documento='23444555';

```
## 7
```sql
 select *from control;

```
## 8
```sql
 update empleados set sueldo=sueldo+sueldo*0.1 where seccion='Secretaria';

```
## 9
```sql
 select *from control;

```
