# [104. Disparador de inserción a nivel de sentencia](solucionario/solucionario104.md)
# solucionario numero 104 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table libros;
drop table ofertas;
drop table control;

```
## 2
```sql
create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table ofertas(
    titulo varchar2(40),
    autor varchar2(30),
    precio number(6,2)
);

create table control(
    usuario varchar2(30),
    fecha date
);

```
## 3
```sql
 alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';

```
## 4
```sql
create or replace trigger tr_ingresar_ofertas
  before insert
  on ofertas
 begin
  insert into Control values(user,sysdate);
 end tr_ingresar_ofertas;
```
## 5
```sql
 select *from user_triggers where trigger_name ='TR_INGRESAR_OFERTAS';

```
## 6
```sql
insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(102,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(105,'El aleph','Borges','Emece',32);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);

```
## 7
```sql
 insert into ofertas select titulo,autor,precio from libros where precio<30;

```
## 8
```sql
 select *from control;

```