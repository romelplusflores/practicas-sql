# [31. Seleccionar grupos (Having)](solucionario/solucionario31.md)
# solucionario numero 31 fecha 14/6/2023 
## ejercicio numero 1

## 1
```sql
drop table clientes;

```
## 2
```sql
create table clientes (
    nombre varchar2(30) not null,
    domicilio varchar2(30),
    ciudad varchar2(20),
    provincia varchar2(20),
    telefono varchar2(11)
);

```
## 3
```sql
insert into clientes
values ('Lopez Marcos','Colon 111','Cordoba','Cordoba','null');

insert into clientes
values ('Perez Ana','San Martin 222','Cruz del Eje','Cordoba','4578585');

insert into clientes
values ('Garcia Juan','Rivadavia 333','Villa del Rosario','Cordoba','4578445');

insert into clientes
values ('Perez Luis','Sarmiento 444','Rosario','Santa Fe',null);

insert into clientes
values ('Pereyra Lucas','San Martin 555','Cruz del Eje','Cordoba','4253685');

insert into clientes
values ('Gomez Ines','San Martin 666','Santa Fe','Santa Fe','0345252525');

insert into clientes
values ('Torres Fabiola','Alem 777','Villa del Rosario','Cordoba','4554455');

insert into clientes
values ('Lopez Carlos',null,'Cruz del Eje','Cordoba',null);

insert into clientes
values ('Ramos Betina','San Martin 999','Cordoba','Cordoba','4223366');

insert into clientes
values ('Lopez Lucas','San Martin 1010','Posadas','Misiones','0457858745');

```
## 4
```sql
SELECT ciudad, provincia, COUNT(*) AS total_registros
FROM clientes
GROUP BY ciudad, provincia;

```
## 5
```sql
SELECT ciudad, provincia, COUNT(*) AS total_registros
FROM clientes
GROUP BY ciudad, provincia
HAVING COUNT(*) >= 2;

```
## 6
```sql
SELECT provincia, COUNT(*) AS total_clientes
FROM clientes
WHERE domicilio LIKE '%San Martin%'
GROUP BY provincia
HAVING COUNT(*) < 2 AND ciudad <> 'Cordoba';

```

## ejercicio numero 2

## 1
```sql
drop table visitantes;

```
## 2
```sql
create table visitantes(
    nombre varchar2(30),
    edad number(2),
    sexo char(1),
    domicilio varchar2(30),
    ciudad varchar2(20),
    telefono varchar2(11),
    montocompra number(6,2) not null
);

```
## 3
```sql
insert into visitantes
values ('Susana Molina',28,'f',null,'Cordoba',null,45.50);

insert into visitantes
values ('Marcela Mercado',36,'f','Avellaneda 345','Cordoba','4545454',22.40);

insert into visitantes
values ('Alberto Garcia',35,'m','Gral. Paz 123','Alta Gracia','03547123456',25);

insert into visitantes
values ('Teresa Garcia',33,'f',default,'Alta Gracia','03547123456',120);

insert into visitantes
values ('Roberto Perez',45,'m','Urquiza 335','Cordoba','4123456',33.20);

insert into visitantes
values ('Marina Torres',22,'f','Colon 222','Villa Dolores','03544112233',95);

insert into visitantes
values ('Julieta Gomez',24,'f','San Martin 333','Alta Gracia',null,53.50);

insert into visitantes
values ('Roxana Lopez',20,'f','null','Alta Gracia',null,240);

insert into visitantes
values ('Liliana Garcia',50,'f','Paso 999','Cordoba','4588778',48);

insert into visitantes
values ('Juan Torres',43,'m','Sarmiento 876','Cordoba',null,15.30);

```
## 4
```sql
SELECT ciudad, sexo, SUM(montocompra) AS total_compras
FROM visitantes
GROUP BY ciudad, sexo
HAVING SUM(montocompra) > 50;

```
## 5
```sql
SELECT ciudad, sexo, SUM(montocompra) AS total_compras
FROM visitantes
WHERE montocompra > 50 AND telefono IS NOT NULL
GROUP BY ciudad, sexo
HAVING ciudad <> 'Cordoba'
ORDER BY ciudad;

```
## 6
```sql
SELECT ciudad, MAX(montocompra) AS monto_mayor
FROM visitantes
WHERE sexo = 'f' AND domicilio IS NOT NULL
GROUP BY ciudad
HAVING MAX(montocompra) > 50;

```
## 7
```sql

SELECT ciudad, sexo, COUNT(*) AS total_visitantes, SUM(montocompra) AS suma_compras, AVG(montocompra) AS promedio_compras
FROM visitantes
GROUP BY ciudad, sexo
HAVING AVG(montocompra) > 30
ORDER BY suma_compras DESC;

```