# [91. Procedimientos Almacenados (crear- ejecutar)](solucionario/solucionario91.md)
# solucionario numero 91 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table empleados;

create table empleados(
    documento char(8),
    nombre varchar2(20),
    apellido varchar2(20),
    sueldo number(6,2),
    cantidadhijos number(2,0),
    fechaingreso date,
    primary key(documento)
);

```
## 2
```sql
insert into empleados values('22222222','Juan','Perez',200,2,'10/10/1980');
insert into empleados values('22333333','Luis','Lopez',250,0,'01/02/1990');
insert into empleados values('22444444','Marta','Perez',350,1,'02/05/1995');
insert into empleados values('22555555','Susana','Garcia',400,2,'15/12/2018');
insert into empleados values('22666666','Jose Maria','Morales',500,3,'25/08/2015');

```
## 3
```sql
create or replace procedure pa_aumentarsueldo
 as
 begin
   update empleados set sueldo=sueldo+(sueldo*0.2)
   where sueldo<(select max(sueldo) from empleados);
 end;
```
## 4
```sql
 exec pa_aumentarsueldo;

```
## 5
```sql
 select *from empleados;

```
## 6
```sql
 exec pa_aumentarsueldo;

```
## 7
```sql
 select *from empleados;

```
## 8
```sql
 drop table empleados_antiguos;

```
## 9
```sql
create table empleados_antiguos(
    documento char(8),
    nombre varchar2(40)
);

```
## 10
```sql
create or replace procedure pa_empleados_antiguos
 as
 begin
  insert into empleados_antiguos
  select documento,nombre||' '||apellido
   from empleados
   where (extract(year from current_date)-extract(year from fechaingreso))>10;
 end;
```
## 11
```sql
 execute pa_empleados_antiguos;

```
## 12
```sql
 select * from empleados_antiguos;

```