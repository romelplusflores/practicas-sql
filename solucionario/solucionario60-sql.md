# [60. Restricciones foreign key deshabilitar y validar](solucionario/solucionario60.md)
# solucionario numero 60 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table clientes;
drop table provincias;

```
## 2
```sql
create table clientes (
    codigo number(5),
    nombre varchar2(30),
    domicilio varchar2(30),
    ciudad varchar2(20),
    codigoprovincia number(2),
    primary key(codigo)
);

create table provincias(
    codigo number(2),
    nombre varchar2(20),
    primary key (codigo)
);

```
## 3
```sql
insert into provincias values(1,'Cordoba');
insert into provincias values(2,'Santa Fe');
insert into provincias values(3,'Misiones');
insert into provincias values(4,'Rio Negro');
insert into clientes values(100,'Perez Juan','San Martin 123','Carlos Paz',1);
insert into clientes values(101,'Moreno Marcos','Colon 234','Rosario',2);
insert into clientes values(102,'Garcia Juan','Sucre 345','Cordoba',1);
insert into clientes values(103,'Lopez Susana','Caseros 998','Posadas',3);
insert into clientes values(104,'Marcelo Moreno','Peru 876','Viedma',4);
insert into clientes values(105,'Lopez Sergio','Avellaneda 333','La Plata',5);

```
## 4
```sql
 alter table clientes
  add constraint FK_clientes_codigoprovincia
  foreign key (codigoprovincia)
  references provincias(codigo);
```
## 5
```sql
alter table clientes
  add constraint FK_clientes_codigoprovincia
  foreign key (codigoprovincia)
  references provincias(codigo) novalidate;
```
## 6
```sql
select constraint_name,status,validated
  from user_constraints
  where table_name='CLIENTES';
```
## 7
```sql
 alter table clientes
  disable novalidate
  constraint FK_clientes_codigoprovincia;
```
## 8
```sql
select constraint_name,status,validated
  from user_constraints
  where table_name='CLIENTES';
```
## 9
```sql
 insert into clientes values(110,'Garcia Omar','San Martin 100','La Pampa',6);

```
## 10
```sql
 update clientes set codigoprovincia=9 where codigo=104;

```
## 11
```sql
 alter table clientes
  enable novalidate
 constraint FK_clientes_codigoprovincia;
```
## 12
```sql
 update clientes set codigoprovincia=9 where codigoprovincia=1;

```
## 13
```sql
  alter table clientes
   enable validate
   constraint FK_clientes_codigoprovincia;
```
## 14
```sql
 delete from clientes where codigoprovincia not in (1,2,3,4);

 alter table clientes
  enable validate
  constraint FK_clientes_codigoprovincia;
```
## 15
```sql
 select status,validated
  from user_constraints
  where constraint_name='FK_CLIENTES_CODIGOPROVINCIA';
```