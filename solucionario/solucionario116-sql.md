# [116. Errores definidos por el usuario en trigger](solucionario/solucionario116.md)
# solucionario numero 116 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table empleados;
drop table control;

```
## 2
```sql
create table empleados(
    documento char(8),
    apellido varchar2(30),
    nombre varchar2(30),
    domicilio varchar2(30),
    seccion varchar2(20),
    sueldo number(8,2)
);

create table control(
    usuario varchar2(30),
    fecha date,
    operacion varchar2(30)
);

```
## 3
```sql
insert into empleados values('22222222','Acosta','Ana','Avellaneda 11','Secretaria',1800);
insert into empleados values('23333333','Bustos','Betina','Bulnes 22','Gerencia',5000);
insert into empleados values('24444444','Caseres','Carlos','Colon 333','Contaduria',3000);

```
## 4
```sql
 create or replace trigger tr_insertar_empleados
 before insert
 on empleados
 for each row
 begin
   if (:new.sueldo>5000)then
    raise_application_error(-20000,'El sueldo no puede superar los $5000');
   end if;
   insert into control values(user,sysdate,'insercion');
 end tr_insertar_empleados;
```
## 5
```sql
 create or replace trigger tr_eliminar_empleados
 before delete
 on empleados
 for each row
 begin
   if (:old.seccion='Gerencia')then
    raise_application_error(-20001,'No puede eliminar empleados de gerencia');
   end if;
   insert into control values(user,sysdate,'borrado');
 end;
```
## 6
```sql
 create or replace trigger tr_actualizar_empleados
 before update
 on empleados
 for each row
 begin
   if updating('documento') then
    raise_application_error(-20002,'No se puede modificar el documento de los empleados');
   else
    insert into control values(user,sysdate,'actualizacion');
   end if;
 end;
```
## 7
```sql
insert into empleados values('25555555','Duarte','Dario','Dominicana 444','Secretaria',5800);

```
## 8
```sql
insert into empleados values('25555555','Duarte','Dario','Dominicana 444','Secretaria',2800);

```
## 9
```sql
 delete from empleados where documento='23333333';

```
## 10
```sql
 delete from empleados where documento='25555555';

```
## 11
```sql
 update empleados set documento='28888888' where documento='22222222';

```
## 12
```sql
 update empleados set nombre='Ana Laura' where nombre='Ana';

```
## 13
```sql
 select *from control;

```