# [72. Subconsultas como expresion](solucionario/solucionario72.md)
# solucionario numero 72 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table alumnos;

```
## 2
```sql
create table alumnos(
  .documento char(8),
  .nombre varchar2(30),
  .nota number(4,2),
  .primary key(documento),
  .constraint CK_alumnos_nota_valores check (nota>=0 and nota <=10)
);

```
## 3
```sql
insert into alumnos values('30111111','Ana Algarbe',5.1);
insert into alumnos values('30222222','Bernardo Bustamante',3.2);
insert into alumnos values('30333333','Carolina Conte',4.5);
insert into alumnos values('30444444','Diana Dominguez',9.7);
insert into alumnos values('30555555','Fabian Fuentes',8.5);
insert into alumnos values('30666666','Gaston Gonzalez',9.70);

```
## 4
```sql
 select alumnos.*
  from alumnos
  where nota=
   (select max(nota) from alumnos);
```
## 5
```sql
 select alumnos.*
  from alumnos
  where nota=
   (select nombre, max(nota) from alumnos);
```
## 6
```sql
 select alumnos.*,
 (select avg(nota) from alumnos)-nota as diferencia
  from alumnos
  where nota<
   (select avg(nota) from alumnos);
```
## 7
```sql
 update alumnos set nota=4
  where nota=
   (select min(nota) from alumnos);
```
## 8
```sql
 delete from alumnos
 where nota<
   (select avg(nota) from alumnos);
```