# [64. Unión](solucionario/solucionario64.md)
# solucionario numero 64 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table medicos;
drop table pacientes;

```
## 2
```sql
create table medicos(
    legajo number(3),
    documento varchar2(8) not null,
    nombre varchar2(30),
    domicilio varchar2(30),
    especialidad varchar2(30),
    primary key(legajo)
);

create table pacientes(
    documento varchar2(8) not null,
    nombre varchar2(30),
    domicilio varchar2(30),
    obrasocial varchar2(20),
    primary key(documento)
);

```
## 3
```sql
insert into medicos values(1,'20111222','Ana Acosta','Avellaneda 111','clinica');
insert into medicos values(2,'21222333','Betina Bustos','Bulnes 222','clinica');
insert into medicos values(3,'22333444','Carlos Caseros','Colon 333','pediatria');
insert into medicos values(4,'23444555','Daniel Duarte','Duarte Quiros 444','oculista');
insert into medicos values(5,'24555666','Estela Esper','Esmeralda 555','alergia');
insert into pacientes values('24555666','Estela Esper','Esmeralda 555','IPAM');
insert into pacientes values('23444555','Daniel Duarte','Duarte Quiros 444','OSDOP');
insert into pacientes values('30111222','Fabiana Fuentes','Famatina 666','PAMI');
insert into pacientes values('31222333','Gaston Gonzalez','Guemes 777','PAMI');

```
## 4
```sql
 select nombre, domicilio from medicos
  union
  select nombre, domicilio from pacientes;
```
## 5
```sql
 select nombre, domicilio from medicos
  union all
  select nombre, domicilio from pacientes;
```
## 6
```sql
 select nombre, domicilio from medicos
  union all
   select nombre, domicilio from pacientes
  order by nombre;
```
## 7
```sql
 select nombre, domicilio, 'médico' as condicion from medicos
  union
   select nombre, domicilio,'paciente' from pacientes
  order by condicion;
```
