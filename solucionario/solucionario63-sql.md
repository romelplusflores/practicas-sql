# [63. Restricciones al crear la tabla](solucionario/solucionario63.md)
# solucionario numero 63 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table inscriptos;
drop table socios;
drop table deportes;
drop table profesores;

```

## 3
```sql
create table profesores(
    documento char(8) not null,
    nombre varchar2(30),
    domicilio varchar2(30),
    constraint PK_profesores_documento
    primary key (documento)
);

create table deportes(
    codigo number(2),
    nombre varchar2(20) not null,
    dia varchar2(9) default 'sabado',
    documentoprofesor char(8),
    constraint CK_deportes_dia_lista
    check (dia in ('lunes','martes','miercoles','jueves','viernes','sabado')),
    constraint PK_deportes_codigo
    primary key (codigo),
    constraint FK_deportes_profesor
    foreign key (documentoprofesor)
    references profesores(documento)
    on delete set null
);

create table socios(
    numero number(4),
    documento char(8),
    nombre varchar2(30),
    domicilio varchar2(30),
    constraint PK_socios_numero
    primary key (numero),
    constraint UQ_socios_documento
    unique (documento)
);

create table inscriptos(
    numerosocio number(4),
    codigodeporte number(2),
    matricula char(1),
    constraint PK_inscriptos_numerodeporte
    primary key (numerosocio,codigodeporte),
    constraint FK_inscriptos_deporte
    foreign key (codigodeporte)
    references deportes(codigo),
    constraint FK_inscriptos_socios
    foreign key (numerosocio)
    references socios(numero)
    on delete cascade,
    constraint CK_matricula_valores
    check (matricula in ('s','n'))
);

```
## 4
```sql
insert into profesores values('21111111','Andres Acosta','Avellaneda 111');
insert into profesores values('22222222','Betina Bustos','Bulnes 222');
insert into profesores values('23333333','Carlos Caseros','Colon 333');

```
## 5
```sql
insert into deportes values(1,'basquet','lunes',null);
insert into deportes values(2,'futbol','lunes','23333333');
insert into deportes values(3,'natacion',null,'22222222');
insert into deportes values(4,'padle',default,'23333333');
insert into deportes values(5,'tenis','jueves',null);

```
## 6
```sql
insert into socios values(100,'30111111','Martina Moreno','America 111');
insert into socios values(200,'30222222','Natalia Norte','Bolivia 222');
insert into socios values(300,'30333333','Oscar Oviedo','Caseros 333');
insert into socios values(400,'30444444','Pedro Perez','Dinamarca 444');

```
## 7
```sql
insert into inscriptos values(100,3,'s');
insert into inscriptos values(100,5,'s');
insert into inscriptos values(200,1,'s');
insert into inscriptos values(400,1,'n');
insert into inscriptos values(400,4,'s');

```
## 8
```sql
 select s.*,d.nombre as deporte,d.dia,p.nombre as profesor
  from socios s
  join inscriptos i
  on numero=i.numerosocio
  join deportes d
  on d.codigo=i.codigodeporte
  left join profesores p
  on d.documentoprofesor=p.documento;
```
## 9
```sql
  select s.*,d.nombre as deporte,d.dia,p.nombre as profesor
  from socios s
  full join inscriptos i
  on numero=i.numerosocio
  left join deportes d
  on d.codigo=i.codigodeporte
  left join profesores p
  on d.documentoprofesor=p.documento;
```
## 10
```sql
  select p.*,d.nombre as deporte,d.dia
  from profesores p
  left join deportes d
  on d.documentoprofesor=p.documento
  order by documento;
```
## 11
```sql
  select d.nombre,count(i.codigodeporte) as cantidad
  from deportes d
  left join inscriptos i
  on d.codigo=i.codigodeporte
  group by d.nombre; 
```
## 12
```sql
select constraint_name, constraint_type, status, validated
  from user_constraints where table_name='SOCIOS'
```
## 13
```sql
 select constraint_name, constraint_type, status, validated, search_condition
  from user_constraints where table_name='DEPORTES';
```
## 14
```sql
select *from user_cons_columns
  where constraint_name='FK_DEPORTES_PROFESOR';
```
## 15
```sql
 select constraint_name, constraint_type, status, validated, search_condition
from user_constraints where table_name='PROFESORES';
```
## 16
```sql
 select constraint_name, constraint_type, status, validated, search_condition
  from user_constraints
  where table_name='INSCRIPTOS';
```
## 17
```sql
 select *from user_cons_columns
  where table_name='INSCRIPTOS';
```
## 18
```sql
 delete from profesores where documento='22222222';

```
## 19
```sql
 select *from deportes;

```
## 20
```sql
 delete from socios where numero=200;

```
## 21
```sql
 select *from inscriptos;

```
## 22
```sql
 delete from deportes where codigo=4;

```
## 23
```sql
 drop table socios;

```
## 24
```sql
 drop table inscriptos;

```
## 25
```sql
 drop table socios;

```
## 26
```sql
 drop table profesores;

```
## 27
```sql
 drop table deportes;

```
## 28
```sql
 drop table profesores;

```
