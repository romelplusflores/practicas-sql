# [106. Disparador de borrado (nivel de sentencia y de fila)](solucionario/solucionario106.md)
# solucionario numero 106 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table empleados;
drop table control;

```
## 2
```sql
create table empleados(
    documento char(8),
    apellido varchar2(20),
    nombre varchar2(20),
    seccion varchar2(30),
    sueldo number(8,2)
);

create table control(
  usuario varchar2(30),
  fecha date
);

```
## 3
```sql
insert into empleados values('22333444','ACOSTA','Ana','Secretaria',500);
insert into empleados values('22777888','DOMINGUEZ','Daniel','Secretaria',560);
insert into empleados values('22999000','FUENTES','Federico','Sistemas',680);
insert into empleados values('22555666','CASEROS','Carlos','Contaduria',900);
insert into empleados values('23444555','GOMEZ','Gabriela','Sistemas',1200);
insert into empleados values('23666777','JUAREZ','Juan','Contaduria',1000);

```
## 4
```sql
create or replace trigger tr_borrar_empleados
  before delete
  on empleados
  for each row
 begin
  insert into control values(user,sysdate);
 end tr_borrar_empleados;
```
## 5
```sql
 select *from user_triggers where trigger_name ='TR_BORRAR_EMPLEADOS';

```
## 6
```sql
 delete from empleados where sueldo>800;

```
## 7
```sql
 select *from control;

```
## 8
```sql
 create or replace trigger tr_borrar_empleados
  before delete
  on empleados
 begin
  insert into control values(user,sysdate);
 end tr_borrar_empleados;
```
## 9
```sql
 select *from user_triggers where trigger_name ='TR_BORRAR_EMPLEADOS';

```
## 10
```sql
 delete from empleados where seccion='Secretaria';

```
## 11
```sql
 select *from control;

```