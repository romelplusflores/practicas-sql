# [68. Modificar campos (alter table - modify)](solucionario/solucionario68.md)
# solucionario numero 68 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table empleados;
drop table secciones;

create table secciones(
    codigo number(2),
    nombre varchar(20),
    primary key (codigo)
);

create table empleados(
    apellido varchar2(20) not null,
    nombre varchar2(20),
    domicilio varchar2(30),
    seccion number(2),
    fechaingreso date,
    telefono number(7),
    constraint FK_empleados_seccion
    foreign key (seccion)
    references secciones(codigo)
    on delete set null
);

```
## 2
```sql
  alter table empleados
   modify telefono varchar(11);
```
## 3
```sql
insert into secciones values(8,'Secretaria');
insert into secciones values(9,'Contaduria');
insert into secciones values(10,'Sistemas');
insert into empleados values('Lopez','Luciano','Colon 123',8,'10/10/1980','4819977');
insert into empleados values('Gonzalez',null,'Avellaneda 222',9,'01/05/1990','4515566');

```
## 4
```sql
 alter table empleados
  modify nombre varchar2(10);

 describe empleados;
```
## 5
```sql
 alter table empleados
  modify nombre varchar2(6);
```
## 6
```sql
 delete empleados where apellido='Lopez';

```
## 7
```sql
 alter table empleados
  modify nombre varchar2(6);
```
## 8
```sql
 alter table secciones
  modify codigo char(2);
```
## 9
```sql
 alter table secciones
  modify codigo number(3);
```
## 10
```sql
 alter table empleados
  modify nombre not null;
```
## 11 
```sql

 update empleados set nombre='Marina' where nombre is null;

 alter table empleados
  modify nombre not null;
```
## 12
```sql
 describe empleados;

```