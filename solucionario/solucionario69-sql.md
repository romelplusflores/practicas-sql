# [69. Eliminar campos (alter table - drop)](solucionario/solucionario69.md)
# solucionario numero 69 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table empleados;
drop table secciones;

create table secciones(
    codigo number(2),
    nombre varchar(20),
    primary key (codigo)
);

create table empleados(
    apellido varchar2(20),
    nombre varchar2(20) not null,
    domicilio varchar2(30),
    seccion number(2),
    sueldo number(8,2),
    constraint CK_empleados_sueldo
    check (sueldo>=0) disable,
    fechaingreso date,
    constraint FK_empleados_seccion
    foreign key (seccion)
    references secciones(codigo)
    on delete set null
 );

```
## 2
```sql
insert into secciones values(8,'Secretaria');
insert into secciones values(9,'Contaduria');
insert into secciones values(10,'Sistemas');
insert into empleados values('Lopez','Juan','Colon 123',8,505.50,'10/10/1980');
insert into empleados values('Gonzalez','Juana','Avellaneda 222',9,600,'01/05/1990');
insert into empleados values('Perez','Luis','Caseros 987',10,800,'12/09/2000');

```
## 3
```sql
 alter table empleados
  drop column domicilio;
  describe empleados;
```
## 4
```sql
 select *from user_constraints
   where table_name='EMPLEADOS';
```
## 5
```sql
 alter table secciones
  drop column codigo;
```
## 6
```sql
alter table empleados
drop constraint FK_empleados_seccion;
alter table secciones
drop column codigo; 

 describe secciones;
```
## 7
```sql
select *from user_constraints
where table_name='SECCIONES';
```
## 8
```sql
alter table empleados
drop column sueldo;
select *from user_constraints
where table_name='EMPLEADOS';
```
## 9
```sql
create index I_empleados_apellido
on empleados(apellido);
select *from user_indexes where table_name='EMPLEADOS';
```
## 10
```sql
 alter table empleados
  drop column apellido;

 select *from user_indexes
   where table_name='EMPLEADOS';
```
## 11
```sql
 alter table empleados
  drop column fechaingreso;
  alter table empleados
  drop column seccion; 
   describe empleados;
```
## 12
```sql
 alter table empleados
  drop column nombre;
```