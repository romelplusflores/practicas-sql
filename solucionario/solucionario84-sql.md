# [84. Vistas (modificar datos a través de ella)](solucionario/solucionario84.md)
# solucionario numero 84 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table inscriptos;
drop table socios;
drop table cursos;

```
## 2
```sql
create table socios(
    documento char(8) not null,
    nombre varchar2(40),
    domicilio varchar2(30),
    constraint PK_socios_documento
    primary key (documento)
);

create table cursos(
    numero number(2),
    deporte varchar2(20),
    dia varchar2(15),
    constraint CK_inscriptos_dia check (dia in('lunes','martes','miercoles','jueves','viernes','sabado')),
    profesor varchar2(20),
    constraint PK_cursos_numero
    primary key (numero)
);

create table inscriptos(
    documentosocio char(8) not null,
    numero number(2) not null,
    matricula char(1),
    constraint PK_inscriptos_documento_numero
    primary key (documentosocio,numero),
    constraint FK_inscriptos_documento
    foreign key (documentosocio)
    references socios(documento),
    constraint FK_inscriptos_numero
    foreign key (numero)
    references cursos(numero)
);

```
## 3
```sql
insert into socios values('30000000','Fabian Fuentes','Caseros 987');
insert into socios values('31111111','Gaston Garcia','Guemes 65');
insert into socios values('32222222','Hector Huerta','Sucre 534');
insert into socios values('33333333','Ines Irala','Bulnes 345');
insert into cursos values(1,'tenis','lunes','Ana Acosta');
insert into cursos values(2,'tenis','martes','Ana Acosta');
insert into cursos values(3,'natacion','miercoles','Ana Acosta');
insert into cursos values(4,'natacion','jueves','Carlos Caseres');
insert into cursos values(5,'futbol','sabado','Pedro Perez');
insert into cursos values(6,'futbol','lunes','Pedro Perez');
insert into cursos values(7,'basquet','viernes','Pedro Perez');
insert into inscriptos values('30000000',1,'s');
insert into inscriptos values('30000000',3,'n');
insert into inscriptos values('30000000',6,null);
insert into inscriptos values('31111111',1,'s');
insert into inscriptos values('31111111',4,'s');
insert into inscriptos values('32222222',1,'s');
insert into inscriptos values('32222222',7,'s');

```
## 4
```sql
 select documento,nombre,domicilio,c.numero,deporte,dia, profesor,matricula
  from socios s
  join inscriptos i
  on s.documento=documentosocio
  join cursos c
  on c.numero=i.numero;
```
## 5
```sql
  drop view vista_cursos;

```
## 6
```sql
 create view vista_cursos
  as
  select numero,deporte,dia
   from cursos;
```
## 7
```sql
 select *from vista_cursos order by deporte;

```
## 8
```sql
 insert into vista_cursos values(8,'futbol','martes');
 select *from cursos;
```
## 9
```sql
 update vista_cursos set dia='miercoles' where numero=8;
 select *from cursos;

```
## 10
```sql
 delete from vista_cursos where numero=8;
 select *from cursos;

```
## 11
```sql
 delete from vista_cursos where numero=1;

```
## 12
```sql
drop view vista_inscriptos;
 create view vista_inscriptos
  as
  select i.documentosocio,s.nombre,i.numero,c.deporte,dia
  from inscriptos i
  join socios s
  on s.documento=documentosocio
  join cursos c
  on c.numero=i.numero;
```
## 13
```sql
insert into vista_inscriptos values('32222222','Hector Huerta',6,'futbol','lunes');

```
## 14
```sql
 update vista_inscriptos set documentosocio='30000111' where documentosocio='30000000';

```
## 15
```sql
 delete from vista_inscriptos where documentosocio='30000000' and deporte='tenis';

```
## 16
```sql
 select *from inscriptos;

```