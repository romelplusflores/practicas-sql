# [114. Disparadores (habilitar y deshabilitar)](solucionario/solucionario114.md)
# solucionario numero 114 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table libros;
drop table control;

```
## 2
```sql
create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table control(
    usuario varchar2(30),
    fecha date,
    operacion varchar2(20)
);

```
## 3
```sql
insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(103,'El aleph','Borges','Emece',28);
insert into libros values(105,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
insert into libros values(145,'Alicia en el pais de las maravillas','Carroll','Planeta',35);

```
## 4
```sql
 create or replace trigger tr_actualizar_libros
  before update
  on libros
  for each row
 begin
  if updating('codigo') then
   insert into control values(user,sysdate,'codigo');
  end if;
  if updating('titulo') then
   insert into control values(user,sysdate,'titulo');
  end if;
  if updating('autor') then
   insert into control values(user,sysdate,'autor');
  end if;
  if updating('editorial') then
   insert into control values(user,sysdate,'editorial');
  end if;
  if updating('precio') then
   insert into control values(user,sysdate,'precio');
  end if;
 end tr_actualizar_libros;
```
## 5
```sql
 create or replace trigger tr_ingresar_libros
  before insert
  on libros
  for each row
 begin
   insert into control values(user,sysdate,'insercion');
 end tr_ingresar_libros;
```
## 6
```sql
 create or replace trigger tr_eliminar_libros
  before delete
  on libros
  for each row
 begin
   insert into control values(user,sysdate,'borrado');
 end tr_eliminar_libros;
```
## 7
```sql
 select trigger_name, triggering_event, status
  from user_triggers
  where table_name = 'LIBROS';
```
## 8
```sql
 insert into libros values(150,'El experto en laberintos','Gaskin','Planeta',38);
 select *from control;
```
## 9
```sql
 alter trigger tr_ingresar_libros disable;

```
## 10
```sql
 select trigger_name, status
  from user_triggers
  where table_name = 'LIBROS';
```
## 11
```sql
 insert into libros values(152,'El anillo del hechicero','Gaskin','Planeta',22);
 select *from control;
```
## 12
```sql
 update libros set editorial='Sudamericana' where editorial='Planeta';
 select *from control;
```
## 13
```sql
 alter trigger tr_actualizar_libros disable;

```
## 14
```sql
 select trigger_name, status
  from user_triggers
  where table_name ='LIBROS';
```
## 15
```sql
 delete from libros where codigo=152;
 select *from control;
```
## 16
```sql
 alter  trigger tr_eliminar_libros disable;

```
## 17
```sql
 select trigger_name, status
  from user_triggers
  where table_name = 'EMPLEADOS';
```
## 18
```sql
 delete from libros where codigo=150;
 select *from libros where codigo=150;
 select *from control;
```
## 19
```sql
 alter trigger tr_actualizar_libros enable;

```
## 20
```sql
 update libros set autor='Adrian Paenza' where autor='Paenza';
 select *from control;
```
## 21
```sql
 alter table libros enable all triggers;

```
## 22
```sql
 select trigger_name, triggering_event, status
  from user_triggers
  where table_name = 'LIBROS';
```
