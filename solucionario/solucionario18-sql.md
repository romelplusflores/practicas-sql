# [18.operadores aritmeticos y de concatenacion ](solucionario/solucionario18.md)
# solucionario numero 18 fecha 8/6/2023
## ejercicio 

## 1
```sql
drop table articulos;

```
## 2
```sql
create table articulos(
    codigo number(4),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(8,2),
    cantidad number(3) default 0,
    primary key (codigo)
);
```
## 3 
```sql
insert into articulos
values (101,'impresora','Epson Stylus C45',400.80,20);

insert into articulos
values (203,'impresora','Epson Stylus C85',500,30);

insert into articulos
values (205,'monitor','Samsung 14',800,10);

insert into articulos
values (300,'teclado','ingles Biswal',100,50);
```
## 4
```sql
UPDATE articulos
SET precio = precio * 1.15;

```
## 5
```sql
select * from articulos;

```
## 6
```sql
select nombre||', '||descripcion from articulos;

```
## 7
```sql
UPDATE articulos SET cantidad = cantidad - 5 WHERE nombre = 'impresora';

```
## 8
```sql
select * from articulos;

```
## 9
```sql

```