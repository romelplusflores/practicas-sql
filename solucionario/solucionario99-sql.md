# [99. Control de flujo (loop)](solucionario/solucionario99.md)
# solucionario numero 99 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
 drop table empleados;
 create table empleados(
  nombre varchar2(40),
  sueldo number(6,2)
 );

```
## 2
```sql
 insert into empleados values('Acosta Ana',550); 
 insert into empleados values('Bustos Bernardo',850); 
 insert into empleados values('Caseros Carolina',900); 
 insert into empleados values('Dominguez Daniel',490); 
 insert into empleados values('Fuentes Fabiola',820); 
 insert into empleados values('Gomez Gaston',740); 
 insert into empleados values('Huerta Hernan',1050); 

```
## 3
```sql
 select sum(sueldo) from empleados;

```
## 4
```sql
 declare
  total number;
 begin
  loop
    update empleados set sueldo=sueldo+(sueldo*0.1);
    select sum(sueldo) into total from empleados;
    exit when total>7000;     
  end loop;
 end;
```
## 5
```sql
 select sueldo from empleados;
 select sum(sueldo) from empleados;
```
## 6
```sql
 select max(sueldo) from empleados;

```
## 7
```sql
set serveroutput on;
 execute dbms_output.enable (20000);
 declare
  maximo number;
  contador number:=0;
 begin
  loop
    update empleados set sueldo=sueldo+(sueldo*0.05);
    contador:=contador+1;
    select max(sueldo) into maximo from empleados;
    exit when maximo>1600;
  end loop;
  dbms_output.put_line(contador);
 end;
```
## 8
```sql
 select sueldo from empleados;
 select max(sueldo) from empleados;
```
## 9
```sql
 select min(sueldo) from empleados;

```
## 10
```sql
 declare
  minimo number;
 begin
  loop
    select min(sueldo) into minimo from empleados;
    if (minimo+minimo*0.1>900) then exit;
    else
      update empleados set sueldo=sueldo+(sueldo*0.1);
    end if;
  end loop;
 end;
```
## 11
```sql
 select min(sueldo) from empleados;

```