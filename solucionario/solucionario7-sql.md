# [7.borrar registros ](solucionario/solucionario7.md)
# solucionario numero 7  fecha  6/6/2023
## ejercicio numero 1
## 1
```sql
drop table agenda;

salida del codigo:
Table AGENDA borrado.

```
## 2
```sql
create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);
salida del codigo:
Table AGENDA creado.

```
## 3
```sql
insert into agenda(apellido,nombre,domicilio,telefono) values ('Alvarez','Alberto','Colon 123','4234567');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Juarez','Juan','Avellaneda 135','4458787');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez','Maria','Urquiza 333','4545454');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez','Jose','Urquiza 333','4545454');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Salas','Susana','Gral. Paz 1234','4123456');

salida del codigo:
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.

```
## 4
```sql
delete from agenda where nombre = 'Juan';

salida del codigo:
1 fila eliminado

```
## 5
```sql
delete from agenda where telefono = '4545454';

salida del codigo:
2 filas eliminado

```
## 6
```sql
delete from agenda;

salida del codigo
2 filas eliminado

```
## ejercicio numero 2
## 1
```sql
drop table articulos;

salida del codigo:
Table ARTICULOS borrado.

```
## 2
```sql
create table articulos(
    codigo number(4,0),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(7,2),
    cantidad number(3)
);
salida del codigo:
Table ARTICULOS creado.

```
## 3
```sql
describe articulos;

salida del codigo:
Nombre      ¿Nulo? Tipo         
----------- ------ ------------ 
CODIGO             NUMBER(4)    
NOMBRE             VARCHAR2(20) 
DESCRIPCION        VARCHAR2(30) 
PRECIO             NUMBER(7,2)  
CANTIDAD           NUMBER(3)    

```
## 4
```sql
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (1,'impresora','Epson Stylus C45',400.80,20);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (2,'impresora','Epson Stylus C85',500,30);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (3,'monitor','Samsung 14',800,10);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (4,'teclado','ingles Biswal',100,50);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (5,'teclado','español Biswal',90,50);

salida del codigo:
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
1 fila insertadas
```
## 5
```sql
delete from articulos where precio >= '500';

salida del codigo:
2 filas eliminado

```
## 6
```sql
delete from articulos where nombre = 'impresora'; 

salida del codigo:
1 fila eliminado

```
## 7
```sql
delete from articulos where codigo <> '4';

salida del codigo:
1 fila eliminado
```
# la sadia final de la tabla 
```sql
select *from articulos;

SALIDA DEL CODIGO:
CODI. NOMBRE    DESCRIPCION    PRECIO  CANTIDAD
4	  teclado	ingles Biswal	100	    50