# [93. Procedimientos almacenados (parámetros de entrada)](solucionario/solucionario93.md)
# solucionario numero 93 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
 drop table empleados;

create table empleados(
    documento char(8),
    nombre varchar2(20),
    apellido varchar2(20),
    sueldo number(6,2),
    fechaingreso date
);

```
## 2
```sql
insert into empleados values('22222222','Juan','Perez',300,'10/10/1980');
insert into empleados values('22333333','Luis','Lopez',300,'12/05/1998');
insert into empleados values('22444444','Marta','Perez',500,'25/08/1990');
insert into empleados values('22555555','Susana','Garcia',400,'05/05/2000');
insert into empleados values('22666666','Jose Maria','Morales',400,'24/10/2005');

```
## 3
```sql
 create or replace procedure pa_empleados_aumentarsueldo(ayear in number, aporcentaje in number)
 as
 begin
  update empleados set sueldo=sueldo+(sueldo*aporcentaje/100)
  where (extract(year from current_date)-extract(year from fechaingreso))>ayear;
 end;
```
## 4
```sql
 execute pa_empleados_aumentarsueldo(10,20);

```
## 5
```sql
 select *from empleados;

```
## 6
```sql
 execute pa_empleados_aumentarsueldo(8,10);

```
## 7
```sql
 select *from empleados;

```
## 8
```sql
 execute pa_empleados_aumentarsueldo;

```
## 9
```sql
create or replace procedure pa_empleados_ingresar
  (adocumento in char, anombre in varchar2, aapellido in varchar2)
 as
 begin
  insert into empleados values(adocumento, anombre, aapellido, null,null);
 end;
```
## 10
```sql
  execute pa_empleados_ingresar('30000000','Ana', 'Acosta');
 select *from empleados;
```
## 11
```sql
 create or replace procedure pa_empleados_ingresar
  (adocumento in char default null, afecha in date default current_date)
 as
 begin
  insert into empleados values(adocumento, null, null, null,afecha);
 end;
```
## 12
```sql
 execute pa_empleados_ingresar('32222222','10/10/2007');
 select *from empleados;
```
## 13
```sql
 execute pa_empleados_ingresar ('15/12/2000');

```
## 14
```sql
 create or replace procedure pa_empleado_eliminar(adocumento in varchar2)
 as
 begin
   delete from empleados where documento=adocumento;
 end;
```
## 15
```sql
 execute pa_empleado_eliminar('30000000');

```
## 16
```sql
 select *from empleados;

```