# [30. Agrupar registros (group by)](solucionario/solucionario30.md)
# solucionario numero 30 fecha 13/6/2023 
## ejercicio numero 1
## 1
```sql
drop table visitantes;

create table visitantes(
    nombre varchar2(30),
    edad number(2),
    sexo char(1) default 'f',
    domicilio varchar2(30),
    ciudad varchar2(20) default 'Cordoba',
    telefono varchar2(11),
    mail varchar2(30) default 'no tiene',
    montocompra number(6,2)
);

```
## 2
```sql
insert into visitantes
values ('Susana Molina',35,default,'Colon 123',default,null,null,59.80);

insert into visitantes
values ('Marcos Torres',29,'m',default,'Carlos Paz',default,'<marcostorres@hotmail.com>',150.50);

insert into visitantes
values ('Mariana Juarez',45,default,default,'Carlos Paz',null,default,23.90);

insert into visitantes (nombre, edad,sexo,telefono, mail)
values ('Fabian Perez',36,'m','4556677','<fabianperez@xaxamail.com>');

insert into visitantes (nombre, ciudad, montocompra)
values ('Alejandra Gonzalez','La Falda',280.50);

insert into visitantes (nombre, edad,sexo, ciudad, mail,montocompra)
values ('Gaston Perez',29,'m','Carlos Paz','<gastonperez1@gmail.com>',95.40);

insert into visitantes
values ('Liliana Torres',40,default,'Sarmiento 876',default,default,default,85);

insert into visitantes
values ('Gabriela Duarte',21,null,null,'Rio Tercero',default,'<gabrielaltorres@hotmail.com>',321.50);

```
## 3
```sql
SELECT ciudad, COUNT(*) AS cantidad_de_visitantes
FROM visitantes
GROUP BY ciudad;

```
## 4
```sql
SELECT ciudad, COUNT(*) AS cantidad_de_visitantes
FROM visitantes
WHERE telefono IS NOT NULL
GROUP BY ciudad;

```
## 5
```sql
SELECT COALESCE(sexo, 'N/A') AS sexo, SUM(montocompra) AS total_monto_compras
FROM visitantes
GROUP BY sexo;

```
## 6
```sql
SELECT sexo, ciudad, MAX(montocompra) AS max_valor_compra, MIN(montocompra) AS min_valor_compra
FROM visitantes
GROUP BY sexo, ciudad;

```
## 7
```sql
SELECT ciudad, AVG(montocompra) AS promedio_valor_compra
FROM visitantes
GROUP BY ciudad;

```
## 8
```sql
SELECT ciudad, COUNT(*) AS cantidad_visitantes
FROM visitantes
WHERE mail IS NOT NULL
GROUP BY ciudad;

```

## ejercicio numero 2

## 1
```sql
drop table empleados;

create table empleados(
    nombre varchar2(30),
    documento char(8),
    domicilio varchar2(30),
    seccion varchar2(20),
    sueldo number(6,2),
    cantidadhijos number(2),
    fechaingreso date,
    primary key(documento)
);

```
## 2
```sql
insert into empleados
values('Juan Perez','22333444','Colon 123','Gerencia',5000,2,'10/05/1980');

insert into empleados
values('Ana Acosta','23444555','Caseros 987','Secretaria',2000,0,'12/10/1980');

insert into empleados
values('Lucas Duarte','25666777','Sucre 235','Sistemas',4000,1,'25/05/1985');

insert into empleados
values('Pamela Gonzalez','26777888','Sarmiento 873','Secretaria',2200,3,'25/06/1990');

insert into empleados
values('Marcos Juarez','30000111','Rivadavia 801','Contaduria',3000,0,'01/05/1996');

insert into empleados
values('Yolanda Perez','35111222','Colon 180','Administracion',3200,1,'01/05/1996');

insert into empleados
values('Rodolfo Perez','35555888','Coronel Olmedo 588','Sistemas',4000,3,'01/05/1996');

insert into empleados
values('Martina Rodriguez','30141414','Sarmiento 1234','Administracion',3800,4,'01/09/2000');

insert into empleados
values('Andres Costa','28444555',default,'Secretaria',null,null,null);

```
## 3
```sql
SELECT seccion, COUNT(*) as cantidad_empleados
FROM empleados
GROUP BY seccion
ORDER BY cantidad_empleados DESC
LIMIT 5;

```
## 4
```sql
SELECT seccion, AVG(cantidadhijos) as promedio_hijos
FROM empleados
GROUP BY seccion
ORDER BY promedio_hijos DESC
LIMIT 5;

```
## 5
```sql
SELECT EXTRACT(YEAR FROM fechaingreso) AS anio_ingreso, COUNT(*) AS cantidad_empleados
FROM empleados
GROUP BY EXTRACT(YEAR FROM fechaingreso)
ORDER BY anio_ingreso DESC
LIMIT 6;

```
## 6
```sql
SELECT seccion, AVG(sueldo) AS promedio_sueldo
FROM empleados
WHERE cantidadhijos > 0
GROUP BY seccion
ORDER BY promedio_sueldo DESC
LIMIT 4;

```
