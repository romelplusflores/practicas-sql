# [52. Autocombinación](solucionario/solucionario52.md)
# solucionario numero 52 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table clientes;

create table clientes(
    nombre varchar2(30),
    sexo char(1),--'f'=femenino, 'm'=masculino
    edad number(2),
    domicilio varchar2(30)
);

```
## 2
```sql
insert into clientes values('Maria Lopez','f',45,'Colon 123');
insert into clientes values('Liliana Garcia','f',35,'Sucre 456');
insert into clientes values('Susana Lopez','f',41,'Avellaneda 98');
insert into clientes values('Juan Torres','m',44,'Sarmiento 755');
insert into clientes values('Marcelo Oliva','m',56,'San Martin 874');
insert into clientes values('Federico Pereyra','m',38,'Colon 234');
insert into clientes values('Juan Garcia','m',50,'Peru 333');

```
## 3
```sql
 select cm.nombre,cm.edad,cv.nombre,cv.edad
  from clientes cm
  cross join clientes cv
  where cm.sexo='f' and cv.sexo='m';
```
## 4
```sql
 select cm.nombre,cm.edad,cv.nombre,cv.edad
  from clientes cm
  join clientes cv
  on cm.nombre<>cv.nombre
  where cm.sexo='f' and cv.sexo='m';
```
## 5
```sql
 select cm.nombre,cm.edad,cv.nombre,cv.edad
  from clientes cm
  cross join clientes cv
  where cm.sexo='f' and cv.sexo='m' and
  cm.edad-cv.edad between -5 and 5;
```
