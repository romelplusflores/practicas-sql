# [5.recuperar algunos registros ](solucionario/solucionario5.md)
# solucionario numero 5 
## ejercicio 1
###### Trabaje con la tabla "agenda" en la que registra los datos de sus amigos.

#### Elimine "agenda"
```sql
drop table agenda;

salida del codigo:
Table AGENDA borrado.
```

#### Cree la tabla, con los siguientes campos: apellido (cadena de 30), nombre (cadena de 20), domicilio (cadena de 30) y telefono (cadena de 11):
```sql
create table agenda(
    apellido varchar2(30),
    nombre varchar2(30),
    domicilio varchar2(30),
    telefono varchar2(11)
);
salida del codigo
Table AGENDA creado.

```
#### Visualice la estructura de la tabla "agenda" (4 campos)
```sql
describe agenda;

salida del codigo:
Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
APELLIDO         VARCHAR2(30) 
NOMBRE           VARCHAR2(30) 
DOMICILIO        VARCHAR2(30) 
TELEFONO         VARCHAR2(11)
```
#### Ingrese los siguientes registros ("insert into"):
```sql
insert into agenda(apellido,nombre,domicilio,telefono) values ('Acosta', 'Ana', 'Colon 123', '4234567');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Bustamante', 'Betina', 'Avellaneda 135', '4458787');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez', 'Hector', 'Salta 545', '4887788'); 
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez', 'Luis', 'Urquiza 333', '4545454');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez', 'Marisa', 'Urquiza 333', '4545454');

salida del codigo:
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
```
#### Seleccione todos los registros de la tabla (5 registros)
```sql
select apellido,nombre,domicilio,telefono from agenda;

salida del codigo:
APELLIDO    NOMBRE  DOMICILIO       TELEFONO
Acosta	    Ana	    Colon 123	    4234567
Bustamante	Betina	Avellaneda 135	4458787
Lopez	    Hector	Salta 545	    4887788
Lopez	    Luis	Urquiza 333	    4545454
Lopez	    Marisa	Urquiza 333	    4545454
```

#### Seleccione el registro cuyo nombre sea "Marisa" (1 registro)
```sql
select nombre from agenda where nombre = 'Marisa'

salida del codigo:
NOMBRE
Marisa
```

#### Seleccione los nombres y domicilios de quienes tengan apellido igual a "Lopez" (3 registros)
```sql
select nombre,domicilio from agenda where apellido = 'Lopez'

salida del codigo:
NOMBRE  DOMICILIO
Hector	Salta 545
Luis	Urquiza 333
Marisa	Urquiza 333
```

#### Seleccione los nombres y domicilios de quienes tengan apellido igual a "lopez" (en minúsculas)
No aparece ningún registro, ya que la cadena "Lopez" no es igual a la cadena "lopez".
```sql
select nombre,domicilio from agenda where apellido = 'lopez'

salida del codigo:
No aparece ningún registro, ya que la cadena "Lopez" no es igual a la cadena "lopez".
```

#### Muestre el nombre de quienes tengan el teléfono "4545454" (2 registros)
```sql
select telefono from agenda where telefono = '4545454'

salida del codigo:
TELEFONO
4545454
4545454
```
## EJERCICIO 2
###### Un comercio que vende artículos de computación registra los datos de sus artículos en una tabla llamada "articulos".

#### Elimine la tabla si existe.
```sql
drop table articulos;

salida de codigos:
Table ARTICULOS borrado.
```
#### Cree la tabla "articulos" con la siguiente estructura:
```sql
create table articulos(
    codigo number(5),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(7,2)
);
salida de codigos:
Table ARTICULOS creado.

```
#### Vea la estructura de la tabla:
```sql
describe articulos;

salida de codigos:
Nombre      ¿Nulo? Tipo         
----------- ------ ------------ 
CODIGO             NUMBER(5)    
NOMBRE             VARCHAR2(20) 
DESCRIPCION        VARCHAR2(30) 
PRECIO             NUMBER(7,2)
```
#### Ingrese algunos registros:
```sql
insert into articulos (codigo, nombre, descripcion, precio) values (1,'impresora','Epson Stylus C45',400.80);
insert into articulos (codigo, nombre, descripcion, precio) values (2,'impresora','Epson Stylus C85',500);
insert into articulos (codigo, nombre, descripcion, precio) values (3,'monitor','Samsung 14',800);
insert into articulos (codigo, nombre, descripcion, precio) values (4,'teclado','ingles Biswal',100);
insert into articulos (codigo, nombre, descripcion, precio) values (5,'teclado','español Biswal',90);

salida de codigos:
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
```
#### Seleccione todos los datos de los registros cuyo nombre sea "impresora" (2 registros)
```sql
select * from articulos where nombre = 'impresora'

salida de codigos:
CODIGO NOMBRE   DESCRIPCION         PRECIO
1	impresora	Epson Stylus C45	400,8
2	impresora	Epson Stylus C85	500
```
#### Muestre sólo el código, descripción y precio de los teclados (2 registros)
```sql
select codigo,descripcion,precio from articulos where nombre = 'teclado'

salida del codigo:
CODIGO DESCIPCION       PRECIO
4	   ingles Biswal	100
5	   español Biswal	90
```