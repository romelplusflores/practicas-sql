# [13.vaciar la tabla ](solucionario/solucionario13.md)
# solucionario numero 13 fecha 7/6/2023
## ejercicio 1
## 1
## Aprendimos que para borrar todos los registro de una tabla se usa "delete" sin condición "where".También podemos eliminar todos los registros de una tabla con "truncate table". Sintaxis:
```sql
truncate table NOMBRETABLA;


Por ejemplo, queremos vaciar la tabla "libros", usamos:

truncate table libros;
```