# [25. Otros operadores relacionales (between)](solucionario/solucionario25.md)
# solucionario numero 25 fecha 13/6/2023 
## ejercicio numero 1

## 1
```sql
drop table visitas;

create table visitas (
    nombre varchar2(30) default 'Anonimo',
    mail varchar2(50),
    pais varchar2(20),
    fecha date
);
```
## 2
```sql
insert into visitas
values ('Ana Maria Lopez','<AnaMaria@hotmail.com>','Argentina','10/10/2016');

insert into visitas
values ('Gustavo Gonzalez','<GustavoGGonzalez@gotmail.com>','Chile','10/10/2016');

insert into visitas
values ('Juancito','<JuanJosePerez@hotmail.com>','Argentina','11/10/2016');

insert into visitas
values ('Fabiola Martinez','<MartinezFabiola@hotmail.com>','Mexico','12/10/2016');

insert into visitas
values ('Fabiola Martinez','<MartinezFabiola@hotmail.com>','Mexico','12/09/2016');

insert into visitas
values ('Juancito','<JuanJosePerez@gmail.com>','Argentina','12/09/2016');

insert into visitas
values ('Juancito','<JuanJosePerez@hotmail.com>','Argentina','15/09/2016');

insert into visitas
values ('Federico1','<federicogarcia@xaxamail.com>','Argentina',null);

```
## 3
```sql
SELECT *
FROM visitas
WHERE fecha >= TO_DATE('12/09/2016', 'DD/MM/YYYY') AND fecha <= TO_DATE('11/10/2016', 'DD/MM/YYYY');

```
## ejercicio numero 2

## 1
```sql
drop table medicamentos;

create table medicamentos(
    codigo number(6) not null,
    nombre varchar2(20),
    laboratorio varchar2(20),
    precio number(6,2),
    cantidad number(4),
    fechavencimiento date not null,
    primary key(codigo)
);

```
## 2
```sql
insert into medicamentos
values(102,'Sertal','Roche',5.2,10,'01/02/2020');

insert into medicamentos
values(120,'Buscapina','Roche',4.10,200,'01/12/2017');

insert into medicamentos
values(230,'Amoxidal 500','Bayer',15.60,100,'28/12/2017');

insert into medicamentos
values(250,'Paracetamol 500','Bago',1.90,20,'01/02/2018');

insert into medicamentos
values(350,'Bayaspirina','Bayer',2.10,150,'01/12/2019');

insert into medicamentos
values(456,'Amoxidal jarabe','Bayer',5.10,250,'01/10/2020');

```
## 3
```sql
SELECT nombre, precio
FROM medicamentos
WHERE precio BETWEEN 5 AND 15
LIMIT 2;

```
## 4
```sql
SELECT *
FROM tu_tabla
WHERE cantidad BETWEEN 100 AND 200
LIMIT 3;

```
## 5
```sql
SELECT *
FROM remedios
WHERE vencimiento BETWEEN CURDATE() AND '2028-01-01';

```
## 6
```sql
DELETE FROM remedios
WHERE YEAR(vencimiento) BETWEEN 2017 AND 2018
LIMIT 3;

```
