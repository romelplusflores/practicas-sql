# [35. Alterar secuencia (alter sequence)](solucionario/solucionario35.md)
# solucionario numero 35 fecha 14/6/2023 
## ejercicio numero 1

## 1
```sql
drop table empleados;

```
## 2
```sql
create table empleados(
    legajo number(3),
    documento char(8) not null,
    nombre varchar2(30) not null,
    primary key(legajo)
);

```
## 3
```sql
BEGIN
    EXECUTE IMMEDIATE 'DROP SEQUENCE sec_legajoempleados';
EXCEPTION
    WHEN OTHERS THEN
        NULL;
END;
```
## 4
```sql
insert into empleados
values (sec_legajoempleados.currval,'22333444','Ana Acosta');

insert into empleados
values (sec_legajoempleados.nextval,'23444555','Betina Bustamante');

insert into empleados
values (sec_legajoempleados.nextval,'24555666','Carlos Caseros');

```
## 5
```sql
SELECT * FROM libros;

```
## 6
```sql
SELECT sec_legajoempleados.CURRVAL FROM dual;

```
## 7
```sql
insert into empleados
values (sec_legajoempleados.nextval,'25666777','Diana Dominguez');

```
## 8
```sql
ALTER SEQUENCE sec_legajoempleados MAXVALUE 999;

```
## 9
```sql
SELECT sequence_name, min_value, max_value, increment_by, last_number
FROM user_sequences
WHERE sequence_name = 'SEC_LEGAJOEMPLEADOS';

```
## 10
```sql
INSERT INTO empleados (legajo, documento, nombre) 
VALUES (7, '12345678', 'Juan Pérez');

```
## 11
```sql
SELECT * FROM empleados;

```
## 12
```sql
ALTER SEQUENCE nombre_secuencia INCREMENT BY 1;

```
## 13
```sql
insert into empleados
values (sec_legajoempleados.nextval,'26777888','Federico Fuentes');

```
## 14
```sql
SELECT * FROM empleados;


```
## 15
```sql

DROP SEQUENCE nombre_secuencia;

```
## 16
```sql

SELECT sequence_name 
FROM all_sequences 
WHERE sequence_owner = 'nombre_esquema' 
AND sequence_name = 'sec_legajoempleados';

```

