# [44. Indices (Crear . Información)](solucionario/solucionario44.md)
# solucionario numero 44 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
ALTER SESSION SET NLS_DATE_FORMAT = 'HH24:MI';

 drop table alumnos;

 create table alumnos(
  legajo char(5) not null,
  documento char(8) not null,
  nombre varchar(30),
  curso char(1),
  materia varchar2(30),
  notafinal number(4,2)
 );
```

## 3
```sql
alter table vehiculos
add constraint CK_vehiculos_tipo
check (tipo in ('a','m'));

```
## 4
```sql
ALTER TABLE vehiculos ADD CONSTRAINT PK_vehiculos PRIMARY KEY (patente, horallegada);

```
## 5
```sql
INSERT INTO vehiculos (patente, tipo, horallegada, horasalida) VALUES ('ABC123', 'a', SYSDATE, NULL);

```
## 6
```sql

INSERT INTO vehiculos (patente, tipo, horallegada, horasalida) VALUES ('ABC123', 'm', SYSDATE, NULL);

```
## 7
```sql

INSERT INTO vehiculos (patente, tipo, horallegada, horasalida) VALUES ('ABC123', 'm', TO_DATE('2023-06-15 10:30', 'YYYY-MM-DD HH24:MI'), NULL);

```
## 8
```sql

INSERT INTO vehiculos (patente, tipo, horallegada, horasalida) VALUES ('XYZ789', 'a', SYSDATE, NULL);

```
## 9
```sql
SELECT constraint_name, constraint_type
FROM user_constraints
WHERE table_name = 'VEHICULOS';

```
## 10
```sql
ALTER TABLE vehiculos DROP CONSTRAINT PK_vehiculos;

```
## 11
```sql
SELECT constraint_name, constraint_type
FROM user_constraints
WHERE table_name = 'VEHICULOS';

```
## 12
```sql
-- Buscar el nombre de la restricción
SELECT constraint_name
FROM user_constraints
WHERE table_name = 'VEHICULOS' AND column_name = 'PATENTE';

-- Eliminar la restricción
ALTER TABLE vehiculos DROP CONSTRAINT <nombre_de_restriccion>;

```
## 13
```sql
SELECT constraint_name, constraint_type
FROM user_constraints
WHERE table_name = 'VEHICULOS';

```
## 14
```sql
ALTER TABLE vehiculos ADD CONSTRAINT PK_vehiculos PRIMARY KEY (patente, horallegada);

```
## 15
```sql

ALTER TABLE vehiculos DROP CONSTRAINT CK_vehiculos_tipo;


ALTER TABLE vehiculos ADD CONSTRAINT CK_vehiculos_tipo CHECK (tipo IN ('a', 'm', 'c'));

```
## 16
```sql
SELECT search_condition
FROM user_constraints
WHERE constraint_name = 'CK_vehiculos_tipo';

```