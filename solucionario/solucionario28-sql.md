# [28. Contar registros (count)](solucionario/solucionario28.md)
# solucionario numero 28 fecha 13/6/2023 
## ejercicio numero 1
## 1
```sql
drop table medicamentos;

```
## 2
```sql
create table medicamentos(
    codigo number(5),
    nombre varchar2(20),
    laboratorio varchar2(20),
    precio number(6,2),
    cantidad number(3),
    fechavencimiento date not null,
    numerolote number(6) default null,
    primary key(codigo)
);

```
## 3
```sql
insert into medicamentos
values(120,'Sertal','Roche',5.2,1,'01/02/2015',123456);

insert into medicamentos
values(220,'Buscapina','Roche',4.10,3,'01/02/2016',null);

insert into medicamentos
values(228,'Amoxidal 500','Bayer',15.60,100,'01/05/2017',null);

insert into medicamentos
values(324,'Paracetamol 500','Bago',1.90,20,'01/02/2018',null);

insert into medicamentos
values(587,'Bayaspirina',null,2.10,null,'01/12/2019',null);

insert into medicamentos
values(789,'Amoxidal jarabe','Bayer',null,null,'15/12/2019',null);


```
## 4
```sql
SELECT COUNT(*)
FROM medicamentos;
```
## 5
```sql
SELECT COUNT(*)
FROM medicamentos
WHERE laboratorio IS NOT NULL;

```
## 6
```sql
SELECT COUNT(precio) AS cantidad_precios, COUNT(cantidad) AS cantidad_cantidades
FROM medicamentos
WHERE precio IS NOT NULL AND cantidad IS NOT NULL;

```
## 7
```sql
SELECT COUNT(*)
FROM medicamentos
WHERE precio IS NOT NULL AND laboratorio LIKE 'B%';

```
## 8 
```sql
SELECT COUNT(*)
FROM medicamentos
WHERE numerolote IS NOT NULL;

```
## 9
```sql
SELECT COUNT(*)
FROM medicamentos
WHERE fechavencimiento IS NOT NULL;

```