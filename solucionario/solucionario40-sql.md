# [40. Restricciones: validación y estados (validate - novalidate - enable - disable)](solucionario/solucionario40.md)
# solucionario numero 40 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table empleados;

```
## 2
```sql
create table empleados (
    codigo number(6),
    documento char(8),
    nombre varchar2(30),
    seccion varchar2(20),
    sueldo number(6,2)
);

insert into empleados
values (1,'22222222','Alberto Acosta','Sistemas',-10);

insert into empleados
values (2,'33333333','Beatriz Benitez','Recursos',3000);

insert into empleados
values (3,'34444444','Carlos Caseres','Contaduria',4000);

```
## 3
```sql
alter table empleados
add constraint CK_empleados_sueldo_positivo
check (sueldo>=0);

```
## 4
```sql
 alter table empleados
 add constraint CK_empleados_sueldo_positivo
 check (sueldo>=0) novalidate;
```
## 5
```sql
 insert into empleados
  values (4,'35555555','Daniel Duarte','Administracion',-2000);

```
## 6
```sql
alter table empleados
  disable novalidate
  constraint CK_empleados_sueldo_positivo;
  insert into empleados
  values (4,'35555555','Daniel Duarte','Administracion',-2000);
```
## 7
```sql
alter table empleados
add constraint CK_empleados_seccion_lista
check (seccion in ('Sistemas','Administracion','Contaduria'));

```
## 8
```sql
alter table empleados
 add constraint CK_empleados_seccion_lista
 check (seccion in ('Sistemas','Administracion','Contaduria')) novalidate;

```
## 9
```sql
 select constraint_name, constraint_type, status, validated
  from user_constraints
  where table_name='EMPLEADOS';
```
## 10
```sql
 alter table empleados
  enable novalidate constraint CK_empleados_sueldo_positivo;

```
## 11
```sql
 update empleados set seccion='Recursos' where nombre='Carlos Caseres';

```
## 12
```sql
alter table empleados
  disable novalidate constraint CK_empleados_seccion_lista;
   update empleados set seccion='Recursos' where nombre='Carlos Caseres';


```
## 13
```sql
alter table empleados
 add constraint PK_empleados_codigo
 primary key (codigo) disable;
```
## 14
```sql
 insert into empleados
  values (4,'22333444','Federico Fuentes',null,null);
```
## 15
```sql
 alter table empleados
  enable novalidate constraint PK_empleados_codigo;
```
## 16
```sql
 update empleados set codigo=5 where nombre='Federico Fuentes';

```
## 17
```sql
 alter table empleados
  enable novalidate constraint PK_empleados_codigo;
```
## 18
```sql
 alter table empleados
 add constraint UQ_empleados_documento
 unique(documento);

```
## 19
```sql
 select constraint_name, constraint_type, status, validated
  from user_constraints
  where table_name='EMPLEADOS';

```
## 20
```sql
alter table empleados
  disable constraint PK_empleados_codigo;
 alter table empleados
  disable constraint UQ_empleados_documento;
 alter table empleados
  disable constraint CK_empleados_sueldo_positivo;
 alter table empleados
  disable constraint CK_empleados_seccion_lista
```
## 21
```sql
 insert into empleados
  values (1,'33333333','Federico Fuentes','Gerencia',-1500);

```
## 22
```sql
 alter table empleados
  enable novalidate constraint CK_empleados_sueldo_positivo;

```
## 23
```sql
 alter table empleados
  enable novalidate constraint CK_empleados_seccion_lista;

```
## 24
```sql
alter table empleados
  enable novalidate constraint PK_empleados_codigo;

 
```
## 25
```sql
alter table empleados
  enable novalidate constraint UQ_empleados_documento;

```
## 26
```sql
 delete empleados where seccion='Gerencia';

```
## 27
```sql
 alter table empleados
  enable novalidate constraint PK_empleados_codigo;
 alter table empleados
  enable novalidate constraint UQ_empleados_documento;

```
## 28
```sql
 select constraint_name, constraint_type, status, validated
  from user_constraints
  where table_name='EMPLEADOS';
```
