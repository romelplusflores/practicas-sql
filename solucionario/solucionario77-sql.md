# [77. Subconsulta simil autocombinacion](solucionario/solucionario77.md)
# solucionario numero 77 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table deportes;

```
## 2
```sql
create table deportes(
    nombre varchar2(15),
    profesor varchar2(30),
    dia varchar2(10),
    cuota number(5,2)
);

```
## 3
```sql
insert into deportes values('tenis','Ana Lopez','lunes',20);
insert into deportes values('natacion','Ana Lopez','martes',15);
insert into deportes values('futbol','Carlos Fuentes','miercoles',10);
insert into deportes values('basquet','Gaston Garcia','jueves',15);
insert into deportes values('padle','Juan Huerta','lunes',15);
insert into deportes values('handball','Juan Huerta','martes',10);

```
## 4
```sql
 select distinct d1.profesor
  from deportes d1
  where d1.profesor in
  (select d2.profesor
    from deportes d2 
    where d1.nombre <> d2.nombre);
```
## 5
```sql
 select distinct d1.profesor
  from deportes d1
  join deportes d2
  on d1.profesor=d2.profesor
  where d1.nombre<>d2.nombre;
```
## 6
```sql
 select nombre
  from deportes
  where nombre<>'natacion' and
  dia =
   (select dia
     from deportes
     where nombre='natacion');
```
## 7
```sql
 select d1.nombre
  from deportes d1
  join deportes d2
  on d1.dia=d2.dia
  where d2.nombre='natacion' and
  d1.nombre<>d2.nombre;
```
