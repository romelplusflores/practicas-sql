# [9.comentarios ](solucionario/solucionario9.md)
# SOLUCIONARIO NUMERO 9  FECHA 6/6/2023
## EJERCICIO NUMERO 1
## 1
```SQL
 select titulo, autor 
 /*mostramos títulos y
 nombres de los autores*/
 from libros;

SALIDA DEL CODIGO:
TITULO                  AUTOR
El aleph	            Borges
Martin Fierro	        Jose Hernandez
Aprenda PHP	            Mario Molina
Cervantes y el quijote	Borges
Matematica estas ahi	Paenza
```