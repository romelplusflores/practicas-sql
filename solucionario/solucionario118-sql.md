# [118. Usuarios (crear)](solucionario/solucionario118.md)

# solucionario numero 118 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
 drop user director cascade;

```
## 2
```sql
 create user director identified by escuela
 default tablespace system
 quota 100M on system;
```
## 3
```sql
 drop user profesor cascade;

```
## 4
```sql
 create user profesor identified by maestro
 default tablespace system;
```
## 5
```sql
 select username, password, default_tablespace, created from dba_users;

```
