# [12.clave primaria ](solucionario/solucionario12.md)
# solucionario numero 12 fecha 7/6/2023
## ejercicio 1
## 1
```sql
drop table libros;

```
## 2
```sql
create table libros(
    codigo number(4) not null,
    titulo varchar2(40) not null,
    autor varchar2(20),
    editorial varchar2(15),
    primary key (codigo)
);
```
## 3
```sql
insert into libros (codigo,titulo,autor,editorial) values (1,'El aleph','Borges','Emece');
insert into libros (codigo,titulo,autor,editorial) values (2,'Martin Fierro','Jose Hernandez','Planeta');
insert into libros (codigo,titulo,autor,editorial) values (3,'Aprenda PHP','Mario Molina','Nuevo Siglo');

```
## 4
```sql
insert into libros (codigo,titulo,autor,editorial) values (3,'Aprenda PHP','Mario Molina','Nuevo Siglo');

``` 
## 5
```sql
insert into libros (codigo,titulo,autor,editorial) values (null,'Sheccid','Mario Molina','Nuevo Siglo');

```
## 6
```sql
update libros set codigo = '1' where titulo = 'Martin Fierro';

```
## 7
```sql
update libros set codigo = '10' where titulo = 'Martin Fierro';

```
## 8
```sql
select uc.table_name, column_name from user_cons_columns ucc
    join user_constraints uc
    on ucc.constraint_name=uc.constraint_name
    where uc.constraint_type='P' and
    uc.table_name='LIBROS';

```
## 9
```sql
select uc.table_name, column_name from user_cons_columns ucc
    join user_constraints uc
    on ucc.constraint_name=uc.constraint_name
    where uc.constraint_type='P' and
    uc.table_name='libros';
```
## ejercicio numero 2
## 1
```sql
drop table alumnos;

```
## 2
```sql
create table alumnos(
    legajo varchar2(4) not null,
    documento varchar2(8),
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(codigo),
    primary key(documento)
);
```
## 3
```sql
create table alumnos(
    legajo varchar2(4) not null,
    documento varchar2(8),
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(documento)
);
```
## 4
```sql
describe alumnos;


```
## 5
```sql
insert into alumnos (legajo,documento,nombre,domicilio) values('A233','22345345','Perez Mariana','Colon 234');
insert into alumnos (legajo,documento,nombre,domicilio) values('A567','23545345','Morales Marcos','Avellaneda 348');

```
## 6
```sql
insert into alumnos (legajo,documento,nombre,domicilio) values('A233','22345345',' Mariana','Colon 234');

```
## 7
```sql
insert into alumnos (legajo,documento,nombre,domicilio) values('A233',null,'Perez','Colon 234');

```
## 8
```sql
select uc.table_name, column_name from user_cons_columns ucc
    join user_constraints uc
    on ucc.constraint_name=uc.constraint_name
    where uc.constraint_type='P' and
    uc.table_name='ALUMNOS';
```
