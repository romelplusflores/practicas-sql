# [110. Disparador de múltiples eventos](solucionario/solucionario110.md)
# solucionario numero 110 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table control;
drop table libros;

```
## 2
```sql
create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table control(
    usuario varchar2(30),
    fecha date,
    operacion varchar2(20)
);

```
## 3
```sql
insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(103,'El aleph','Borges','Emece',28);
insert into libros values(105,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
insert into libros values(145,'Alicia en el pais de las maravillas','Carroll','Planeta',35);

```
## 4
```sql
create or replace trigger tr_cambios_libros
 before insert or update or delete
 on libros
 for each row
begin
 if inserting then
  if ((to_char(sysdate,'dy','nls_date_language=SPANISH') in ('sáb')) and
     (to_number(to_char(sysdate,'HH24')) between 8 and 11)) then
   insert into control values (user, sysdate,'ingreso');
  else
     raise_application_error(-20000,'Los ingresos sólo los Sab. de 8 a 12 hs.');
  end if;
 end if;
 if deleting then
  if (to_char(sysdate,'dy','nls_date_language=SPANISH') in ('sáb')) and
     (to_number(to_char(sysdate,'HH24')) between 8 and 11) then
   insert into control values (user, sysdate,'borrado');
  else
    raise_application_error(-20001,'Las eliminaciones solo los Sab. de 8 a 12 hs.');
  end if;
 end if; 
 if updating then
  if ((to_char(sysdate,'dy','nls_date_language=SPANISH') in ('lun','mar','mié','jue','vie')) and
     (to_number(to_char(sysdate,'HH24')) between 8 and 19)) or
     ((to_char(sysdate,'dy','nls_date_language=SPANISH') in('sáb')) and
     (to_number(to_char(sysdate,'HH24')) between 8 and 11))then
   insert into control values (user, sysdate,'actualización');
  else
    raise_application_error(-20002,'Las actualizaciones solo de L a V de 8 a 20 o S de 8 a 12 hs.');
  end if;
 end if;
end tr_cambios_libros;
```
## 5
```sql
 insert into libros values(150,'El experto en laberintos','Gaskin','Planeta',25);

```
## 6
```sql
 insert into libros values(150,'El experto en laberintos','Gaskin','Planeta',25);

```
## 7
```sql
 insert into libros values(150,'El experto en laberintos','Gaskin','Planeta',25);

```
## 8
```sql
 select *from libros;
 select *from control;
```
## 9
```sql
 update libros set precio=45 where codigo=150;

```
## 10
```sql
  update libros set precio=45 where codigo=150;

```
## 11
```sql
 update libros set precio=45 where codigo=150;

```
## 12
```sql
select *from libros;
 select *from control;
```
## 13
```sql
 update libros set precio=50 where codigo=150;

```
## 14
```sql
 select *from libros;
 select *from control;
```
## 15
```sql
 delete from libros where codigo=150;

```
## 16
```sql
 delete from libros where codigo=150;

```
## 17
```sql
 delete from libros where codigo=150;

```
## 18
```sql
 select *from libros;
 select *from control;
```