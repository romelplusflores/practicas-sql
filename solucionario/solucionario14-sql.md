# [14.tipos de datos alfanumericos ](solucionario/solucionario14.md)
# solucionario numero 14 fecha 7/6/2023
## ejercicio numero 1
## 1
```sql
drop table autos;

```
## 2
```sql
create table autos(
    patente char(6),
    marca varchar2(20),
    modelo char(4),
    precio number(8,2),
    primary key (patente)
);

```
## 3
```sql
insert into autos (patente,marca,modelo,precio) values('ABC123','Fiat 128','1970',15000);
insert into autos (patente,marca,modelo,precio) values('BCD456','Renault 11','1990',40000);
insert into autos (patente,marca,modelo,precio) values('CDE789','Peugeot 505','1990',80000);
insert into autos (patente,marca,modelo,precio) values('DEF012','Renault Megane','1998',95000);

```
## 4
```sql
insert into autos (patente,marca,modelo,precio) values('DEF012','Renault Megane',1998,95000);

``` 
## 5
```sql
select *from autos;
```
## 6
```sql
select *from autos where modelo= '1990';

```
## 7
```sql
insert into autos (patente,marca,modelo,precio) values('DEF0123','Renault Megane','1998',95000);

```
## 8
```sql
insert into autos (patente,marca,modelo,precio) values('DEF012','Renault Megane',1998,95000);

```

## ejercicio numero 2
## 1
```sql
drop table clientes;

```
## 2
```sql
create table clientes(
    documento char(8) not null,
    apellido varchar2(20),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2 (11)
);
```
## 3
```sql
describe clientes;

```
## 4
```sql
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('22333444','Perez','Juan','Sarmiento 980','4223344');
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('23444555','Perez','Ana','Colon 234',null);
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('30444555','Garcia','Luciana','Caseros 634',null);

``` 
## 5
```sql
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('30444555','Flores','Yumpio','Caseros 634','123456789123');

```
## 6
```sql
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('123456789','Flores','Yumpio','Caseros 634','123456789123');

```
## 7
```sql
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('123456789',Flores,'Yumpio','Caseros 634','123456789123');

```
## 8
```sql
select *from clientes where apellido = 'Perez';
```
