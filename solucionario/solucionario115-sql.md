# [115. Disparador (eliminar)](solucionario/solucionario115.md)

# solucionario numero 115 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table ventas;
drop table articulos;

```
## 2
```sql
create table articulos(
    codigo number(4) not null,
    descripcion varchar2(40),
    precio number (6,2),
    stock number(4),
    constraint PK_articulos_codigo
    primary key (codigo)
);

create table ventas(
    codigo number(4),
    cantidad number(4),
    fecha date,
    constraint FK_ventas_articulos
    foreign key (codigo)
    references articulos(codigo)
);

```
## 3
```sql
 drop sequence sec_codigoart;
 create sequence sec_codigoart
  start with 1
  increment by 1;
```
## 4
```sql
 set serveroutputon;
 execute dbms_output.enable (20000);
```
## 5
```sql
 create or replace trigger tr_insertar_codigo_articulos
  before insert
  on articulos
  for each row
 begin
  select sec_codigoart.nextval into :new.codigo from dual;
  dbms_output.put_line('"tr_insertar_codigo_articulos" activado');
 end tr_insertar_codigo_articulos;
```
## 6
```sql
insert into articulos (descripcion, precio, stock) values ('cuaderno rayado 24h',4.5,100);
insert into articulos (descripcion, precio, stock) values ('cuaderno liso 12h',3.5,150);
insert into articulos (descripcion, precio, stock) values ('lapices color x6',8.4,60);

```
## 7
```sql
 select *from articulos;

```
## 8
```sql
insert into articulos values(160,'regla 20cm.',6.5,40);
insert into articulos values(173,'compas metal',14,35);
insert into articulos values(234,'goma lapiz',0.95,200);

```
## 9
```sql
 select *from articulos;

```
## 10
```sql
 create or replace trigger tr_insertar_ventas
  before insert
  on ventas
  for each row
 declare
   canti number:=null;
 begin
  dbms_output.put_line('"tr_insertar_ventas" activado');
  select stock into canti from articulos where codigo=:new.codigo;
   if :new.cantidad>canti then
    raise_application_error(-20001,'Sólo hay '|| to_char(canti)||' en stock');
   else
     update articulos set stock=stock-:new.cantidad where codigo=:new.codigo; 
   end if; 
 end tr_insertar_ventas;
```
## 11
```sql
  insert into ventas values(200,10,sysdate);

```
## 12
```sql
 select *from ventas;

```
## 13
```sql
  insert into ventas values(2,10,sysdate);

```
## 14
```sql
 select *from articulos;
 select *from ventas;
```
## 15
```sql
  insert into ventas values(2,300,sysdate);

```
## 16
```sql
 select *from articulos;
 select *from ventas;
```
## 17
```sql
 create or replace trigger tr_insertar_ventas
  before insert
  on ventas
  for each row
 declare
   canti number:=null;
 begin
  dbms_output.put_line('"tr_insertar_ventas" activado');
  if (to_char(sysdate, 'DY','nls_date_language=SPANISH') in ('LUN','MAR','MIE','JUE','VIE')) then 
    if(to_number(to_char(sysdate, 'HH24')) between 8 and 17) then
      select stock into canti from articulos where codigo=:new.codigo;
      if :new.cantidad>canti then
        raise_application_error(-20001,'Sólo hay '|| to_char(canti)||' en stock');
      else
        update articulos set stock=stock-:new.cantidad where codigo=:new.codigo; 
      end if;
    else--hora
      raise_application_error(-20002,'Solo se pueden realizar ventas entre las 8 y 18 hs.');
    end if;--hora
  else
    raise_application_error(-20003,'Solo se pueden realizar ventas de lunes a viernes.');
  end if; --dia
 end tr_insertar_ventas;
```
## 18
```sql
 insert into ventas values(3,20,sysdate);

```
## 19
```sql
 select *from articulos;
 select *from ventas;
```
## 20
```sql
 insert into ventas values(3,20,sysdate);

```
## 21
```sql
 insert into ventas values(3,20,sysdate);

```
## 22
```sql
 create or replace trigger tr_articulos
  before insert or update or delete
  on articulos
  for each row
 begin
  dbms_output.put_line('"tr_update_articulos" activado');
  if (to_char(sysdate, 'DY','nls_date_language=SPANISH')='SÁB')then
     if(to_number(to_char(sysdate, 'HH24')) not between 8 and 11) then
       raise_application_error(-20004,'Esta tarea en sábado sólo de 8 a 12 hs.');
     end if;
  else
    if(to_char(sysdate, 'DY','nls_date_language=SPANISH')='DOM')then
      raise_application_error(-20005,'Esta tarea no en domingo.');
    else 
      if updating ('stock') then
        if  (to_number(to_char(sysdate, 'HH24')) not between 8 and 17) then
           raise_application_error(-20006,'Esta tarea de LUN a VIE de 8 a 18 hs. o SAB de 8 a 12 hs.');
        end if;
      else
         raise_application_error(-20007,'Esta tarea solo SAB de 8 a 12 hs.');
      end if;--stock
    end if;--domingo
  end if;--sab
 end tr_articulos;
```
## 23
```sql
insert into articulos values(234,'cartulina',0.95,100);

```
## 24
```sql
 delete from articulos where codigo=1;

```
## 25
```sql
 update articulos set precio=precio+precio*0.1 where codigo=5;

```
## 26
```sql
 update articulos set precio=precio+precio*0.1 where codigo=5;

```
## 27
```sql
 insert into ventas values(3,5,sysdate);

```
## 28
```sql
select table_name, trigger_name, triggering_event from user_triggers
  where table_name ='ARTICULOS' or table_name='VENTAS';

```
## 29
```sql
 drop trigger tr_insertar_ventas;

```
## 30
```sql
 drop table ventas;
 drop table articulos;

```
## 31
```sql
select * from user_triggers where trigger_name='TR_ARTICULOS' or trigger_name='TR_INSERTAR_CODIGO_ARTICULOS'; 

```
