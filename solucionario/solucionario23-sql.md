# [23. Ordenar registros (order by)](solucionario/solucionario23.md)
# solucionario numero 23 fecha 8/6/2023 
## ejercicio numero 1

## 1
```sql
drop table visitas;

create table visitas (
    nombre varchar2(30) default 'Anonimo',
    mail varchar2(50),
    pais varchar2(20),
    fecha date
);

```
## 2
```sql
insert into visitas
values ('Ana Maria Lopez','<AnaMaria@hotmail.com>','Argentina',to_date('2020/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'));

insert into visitas
values ('Gustavo Gonzalez','<GustavoGGonzalez@hotmail.com>','Chile',to_date('2020/02/13 11:08:10', 'yyyy/mm/dd hh24:mi:ss'));

insert into visitas
values ('Juancito','<JuanJosePerez@hotmail.com>','Argentina',to_date('2020/07/02 14:12:00', 'yyyy/mm/dd hh24:mi:ss'));

insert into visitas
values ('Fabiola Martinez','<MartinezFabiola@hotmail.com>','Mexico',to_date('2020/06/17 20:00:00', 'yyyy/mm/dd hh24:mi:ss'));

insert into visitas
values ('Fabiola Martinez','<MartinezFabiola@hotmail.com>','Mexico',to_date('2020/02/08 20:05:40', 'yyyy/mm/dd hh24:mi:ss'));

insert into visitas
values ('Juancito','<JuanJosePerez@hotmail.com>','Argentina',to_date('2020/07/06 18:00:00', 'yyyy/mm/dd hh24:mi:ss'));

insert into visitas
values ('Juancito','<JuanJosePerez@hotmail.com>','Argentina',to_date('2019/10/05 23:00:00', 'yyyy/mm/dd hh24:mi:ss'));

```
## 3
```sql
select *from visitas
order by fecha desc;

```
## 4
```sql

```
## 5
```sql
select *from visitas
order by pais asc, fecha desc;
```
