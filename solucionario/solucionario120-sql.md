# [120. Privilegios del sistema (conceder)](solucionario/solucionario120.md)
# solucionario numero 120 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop user director cascade;

```
## 2
```sql
create user director identified by escuela
 default tablespace system
 quota 100M on system;
```
## 3
```sql
 select *from dba_sys_privs where grantee='DIRECTOR';

```
## 4
```sql
 grant create session, create table
  to director;
```
## 5
```sql
 select *from dba_sys_privs where grantee='DIRECTOR';

```
## 6
```sql
 drop user profesor cascade;
 drop user alumno cascade;
```
## 7
```sql
 create user profesor identified by maestro
 default tablespace system
 quota 100M on system;
```
## 8
```sql
create user estudiante identified by alumno
 default tablespace system;
```
## 9
```sql

 select username, password, default_tablespace, created from dba_users;
 ```
## 10
```sql

 grant create session
  to profesor, estudiante;
  ```
## 11
```sql

 grant create table
  to estudiante;
  ```
## 12
```sql

 select *from dba_sys_privs where grantee='DIRECTOR'
 or grantee='PROFESOR'
 or grantee='ESTUDIANTE';
 ```
## 13
```sql

 create table prueba(
  nombre varchar2(30),
  apellido varchar2(30)
 );
 ```
## 14
```sql

 create table prueba(
  nombre varchar2(30),
  apellido varchar2(30)
 );
 ```
## 15
```sql

 select *from user_sys_privs;
 ```
## 16
```sql

 grant create table
  to profesor;
  ```
## 17
```sql

 create table prueba(
  nombre varchar2(30),
  apellido varchar2(30)
 );
 ```
## 18
```sql

 select *from user_sys_privs;
 ```
## 19
```sql

 create table prueba(
  nombre varchar2(30),
  apellido varchar2(30)
 );
 ```
## 20
```sql

 select *from dba_objects where object_name='PRUEBA';
 ```









 

