# [76. Exists y No Exists](solucionario/solucionario76.md)
# solucionario numero 76 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table inscriptos;
drop table socios;

```
## 2
```sql
create table socios(
    numero number(4),
    documento char(8),
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key (numero)
);
 
create table inscriptos (
    numerosocio number(4) not null,
    deporte varchar(20) not null,
    cuotas number(2) default 0,
    constraint CK_inscriptos_cuotas
    check (cuotas>=0 and cuotas<=10),
    primary key(numerosocio,deporte),
    constraint FK_inscriptos_socio
    foreign key (numerosocio)
    references socios(numero)
    on delete cascade
);

```
## 3
```sql
insert into socios values(1,'23333333','Alberto Paredes','Colon 111');
insert into socios values(2,'24444444','Carlos Conte','Sarmiento 755');
insert into socios values(3,'25555555','Fabian Fuentes','Caseros 987');
insert into socios values(4,'26666666','Hector Lopez','Sucre 344');
insert into inscriptos values(1,'tenis',1);
insert into inscriptos values(1,'basquet',2);
insert into inscriptos values(1,'natacion',1);
insert into inscriptos values(2,'tenis',9);
insert into inscriptos values(2,'natacion',1);
insert into inscriptos values(2,'basquet',default);
insert into inscriptos values(2,'futbol',2);
insert into inscriptos values(3,'tenis',8);
insert into inscriptos values(3,'basquet',9);
insert into inscriptos values(3,'natacion',0);
insert into inscriptos values(4,'basquet',10);

```
## 4
```sql
SELECT s.nombre, s.domicilio, 
    (SELECT COUNT(*) FROM inscriptos i WHERE i.numerosocio = s.numero) AS cantidad_deportes
FROM socios s;

```
## 5
```sql
SELECT s.nombre,
    (SELECT COUNT(*) * 10 FROM inscriptos i WHERE i.numerosocio = s.numero) AS total_cuotas_debe_pagar,
    (SELECT SUM(i.cuotas) FROM inscriptos i WHERE i.numerosocio = s.numero) AS total_cuotas_pagas
FROM socios s;

```
## 6
```sql
SELECT s.nombre, 
    COUNT(i.numerosocio) * 10 AS total_cuotas_debe_pagar,
    SUM(i.cuotas) AS total_cuotas_pagas
FROM socios s
LEFT JOIN inscriptos i ON s.numero = i.numerosocio
GROUP BY s.nombre;

```
