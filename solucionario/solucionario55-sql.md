# [55. Otros tipos de combinaciones](solucionario/solucionario55.md)
# solucionario numero 55 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table clientes;
drop table provincias;

create table clientes (
    codigo number(5),
    nombre varchar2(30),
    domicilio varchar2(30),
    ciudad varchar2(20),
    codigoprovincia number(2)
);

create table provincias(
    codigoprovincia number(2),
    nombre varchar2(20)
);

```
## 2
```sql
insert into provincias values(1,'Cordoba');
insert into provincias values(2,'Santa Fe');
insert into provincias values(3,'Corrientes');
insert into provincias values(null,'Salta');
insert into clientes values (100,'Lopez Marcos','Colon 111','Córdoba',1);
insert into clientes values (101,'Perez Ana','San Martin 222','Cruz del Eje',1);
insert into clientes values (102,'Garcia Juan','Rivadavia 333','Villa Maria',1);
insert into clientes values (103,'Perez Luis','Sarmiento 444','Rosario',2);
insert into clientes values (104,'Gomez Ines','San Martin 666','Santa Fe',2);
insert into clientes values (105,'Torres Fabiola','Alem 777','La Plata',4);
insert into clientes values (106,'Garcia Luis','Sucre 475','Santa Rosa',null);

```
## 3
```sql
 select c.nombre,domicilio,ciudad, p.nombre as provincia
  from clientes c
  left join provincias p
  on c.codigoprovincia = p.codigoprovincia;
```
## 4
```sql
select c.nombre,domicilio,ciudad, p.nombre as provincia
  from clientes c
  join provincias p
  on c.codigoprovincia = p.codigoprovincia(+);
```
## 5
```sql
 select c.nombre,domicilio,ciudad, p.nombre as provincia
  from clientes c
  right join provincias p
  on c.codigoprovincia = p.codigoprovincia;
```
## 6
```sql
select c.nombre,domicilio,ciudad, p.nombre as provincia
  from clientes c
  join provincias p
  on c.codigoprovincia(+)= p.codigoprovincia;
```
## 7
```sql
select c.nombre,domicilio,ciudad, p.nombre as provincia
  from clientes c
  join provincias p
  on codigoprovincia(+)= p.codigo(+);
```
## 8
```sql
 select c.nombre,ciudad,p.nombre as provincia
  from clientes c
  natural join
  provincias p;
```
## 9
```sql
select c.nombre,ciudad,p.nombre as provincia
  from clientes c
  join provincias p
  using (codigoprovincia);
```