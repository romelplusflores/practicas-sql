# [100. Control de flujo (for)](solucionario/solucionario100.md)
# solucionario numero 100 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
 set serveroutput on;
 execute dbms_output.enable(20000);
 begin
  for numero in 1..20 loop
   if mod(numero,2)=0 then
    dbms_output.put_line(numero);
   end if;
  end loop;
 end;
```
## 2
```sql
declare
 sumatoria number:=0;
 begin
  for numero in 1..5 loop
   sumatoria:=sumatoria+numero;
  end loop;
  dbms_output.put_line(sumatoria);
 end;
```
## 3
```sql
create or replace function f_factorial(avalor number)
  return number
 is
  valorretornado number:=1;
 begin
   for f in reverse 1..avalor loop
     valorretornado:=valorretornado*f;
   end loop;
   return valorretornado;
 end;
```
## 4
```sql
 select f_factorial(5) from dual;
 select f_factorial(4) from dual;
```
## 5
```sql
 create or replace procedure pa_tabla(anumero number, alimite number)
  as 
 begin
  dbms_output.put_line('Tabla del '||to_char(anumero));
  for f in 1..alimite loop
    dbms_output.put_line(to_char(anumero)||'x'||to_char(f)||'='||to_char(f*anumero));
  end loop;
 end;
```
## 6
```sql
 execute pa_tabla(6,20);

```
## 7
```sql
 execute pa_tabla(9,10);

```
`