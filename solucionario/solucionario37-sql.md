# [37. Restricción primary key](solucionario/solucionario37.md)
# solucionario numero 37 fecha 14/6/2023 
## ejercicio numero 1

## 1
```sql
drop table empleados;

```
## 2
```sql
create table empleados (
    documento char(8),
    nombre varchar2(30),
    seccion varchar2(20)
);

```
## 3
```sql
INSERT INTO empleados (documento, nombre, seccion) 
VALUES ('12345678', 'Juan Pérez', 'Ventas');

INSERT INTO empleados (documento, nombre, seccion) 
VALUES ('87654321', 'María Gómez', 'Administración');

INSERT INTO empleados (documento, nombre, seccion) 
VALUES ('12345678', 'Pedro Rodríguez', 'Marketing');

```
## 4
```sql
ALTER TABLE empleados
ADD CONSTRAINT pk_empleados PRIMARY KEY (documento);
```
## 5
```sql
ALTER TABLE empleados
ADD CONSTRAINT pk_empleados PRIMARY KEY (legajo);

```
## 6
```sql
UPDATE empleados
SET documento = '12345678'
WHERE legajo = 1;

```
## 7
```sql
ALTER TABLE empleados
ADD CONSTRAINT pk_nombre PRIMARY KEY (nombre);

```
## 8
```sql
SELECT constraint_name, constraint_type
FROM user_constraints
WHERE table_name = 'empleados' 
    AND constraint_type = 'P';

```
## 9
```sql
SELECT table_name, constraint_name, column_name
FROM user_cons_columns
WHERE table_name = 'empleados';


```

## ejercicio numero 2

## 1
```sql
drop table remis;

```
## 2
```sql
create table remis(
    numero number(5),
    patente char(6),
    marca varchar2(15),
    modelo char(4)
);

```
## 3
```sql
INSERT INTO remis (numero, patente, marca, modelo) 
VALUES (10001, 'ABC123', 'Ford', '2022');

INSERT INTO remis (numero, patente, marca, modelo) 
VALUES (10002, 'DEF456', 'Chevrolet', '2021');

INSERT INTO remis (numero, patente, marca, modelo) 
VALUES (10001, 'GHI789', 'Toyota', '2020');

```
## 4
```sql
INSERT INTO remis (numero, patente, marca, modelo) 
VALUES (10003, NULL, 'Toyota', '2023');

```
## 5
```sql
ALTER TABLE remis
ADD CONSTRAINT pk_remis PRIMARY KEY (numero);

```
## 6
```sql
ALTER TABLE remis
ADD CONSTRAINT uk_patente UNIQUE (patente);

```
## 7
```sql
UPDATE remis
SET patente = 'NuevaPatente'
WHERE numero = 10001;

```
## 8
```sql
ALTER TABLE remis
ADD CONSTRAINT pk_remis PRIMARY KEY (numero);

```
## 9
```sql
SELECT constraint_name, constraint_type, table_name, column_name
FROM user_constraints
WHERE table_name = 'REMIS' 
    AND constraint_type = 'P';

```
## 10
```sql
SELECT table_name, constraint_name, column_name
FROM user_cons_columns
WHERE constraint_name = 'PK_REMIS';

```