# [98. Control de flujo (case)](solucionario/solucionario98.md)
# solucionario numero 98 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
  drop table alumnos;

```
## 2
```sql
 create table alumnos(
  legajo char(5) not null,
  nombre varchar2(30),
  promedio number(4,2)
);

```
## 3
```sql
 insert into alumnos values(3456,'Perez Luis',8.5);
 insert into alumnos values(3556,'Garcia Ana',7.0);
 insert into alumnos values(3656,'Ludueña Juan',9.6);
 insert into alumnos values(2756,'Moreno Gabriela',4.8);
 insert into alumnos values(4856,'Morales Hugo',3.2);
 insert into alumnos values(7856,'Gomez Susana',6.4);

```
## 4
```sql
select legajo,promedio,
  case trunc(promedio)
   when 0 then 'reprobado'
   when 1 then 'reprobado'
   when 2 then 'reprobado'
   when 3 then 'reprobado'
   when 4 then 'regular'
   when 5 then 'regular'
   when 6 then 'regular'
   when 7 then 'promocionado'
   when 8 then 'promocionado'
   when 9 then 'promocionado'
   else 'promocionado'
  end as Condicion
 from alumnos;
```
## 5
```sql
 drop table alumnos;

```
## 6
```sql
 create table alumnos(
  legajo char(5) not null,
  nombre varchar2(30),
  nota number(4,2)
);

```
## 7
```sql
 insert into alumnos values(3456,'Perez Luis',8.5);
 insert into alumnos values(3456,'Perez Luis',9.9);
 insert into alumnos values(3456,'Perez Luis',7.8);
 insert into alumnos values(3556,'Garcia Ana',7.0);
 insert into alumnos values(3556,'Garcia Ana',6.0);
 insert into alumnos values(3656,'Ludueña Juan',9.6);
 insert into alumnos values(3656,'Ludueña Juan',10);
 insert into alumnos values(2756,'Moreno Gabriela',4.2);
 insert into alumnos values(2756,'Moreno Gabriela',2.6);
 insert into alumnos values(2756,'Moreno Gabriela',2);
 insert into alumnos values(4856,'Morales Hugo',3.2);
 insert into alumnos values(4856,'Morales Hugo',4.7);
 insert into alumnos values(7856,'Gomez Susana',6.4);
 insert into alumnos values(7856,'Gomez Susana',8.6);

```
## 8
```sql
select legajo,trunc(avg(nota),2),
  case trunc(avg(nota))
   when 0 then 'reprobado'
   when 1 then 'reprobado'
   when 2 then 'reprobado'
   when 3 then 'reprobado'
   when 4 then 'regular'
   when 5 then 'regular'
   when 6 then 'regular'
   when 7 then 'promocionado'
   when 8 then 'promocionado'
   when 9 then 'promocionado'
   else 'promocionado'
  end as Condicion
 from alumnos
 group by legajo;
```
## 9
```sql
  drop table alumnosCondicion;
  create table alumnosCondicion(
   legajo char(5),
   notafinal number(4,2),
   condicion varchar2(15)
  );

```
## 10
```sql
 create or replace procedure pa_cargarCondicion
 as
 begin
  insert into alumnoscondicion
  select legajo,avg(nota),
  case trunc(avg(nota))
   when 0 then 'libre'
   when 1 then 'libre'
   when 2 then 'libre'
   when 3 then 'libre'
   when 4 then 'regular'
   when 5 then 'regular'
   when 6 then 'regular'
   when 7 then 'promocionado'
   when 8 then 'promocionado'
   when 9 then 'promocionado'
   else 'promocionado'
  end
  from alumnos
  group by legajo;
 end;
```
## 11
```sql
 execute pa_CargarCondicion;
 select *from alumnoscondicion;
```