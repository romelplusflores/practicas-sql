# [75. Subconsultas correlacionadas](solucionario/solucionario75.md)
# solucionario numero 75 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table inscriptos;
drop table socios;

```
## 2
```sql
create table socios(
    numero number(4),
    documento char(8),
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key (numero)
);

create table inscriptos (
    numerosocio number(4) not null,
    deporte varchar(20) not null,
    cuotas number(2) default 0,
    constraint CK_inscriptos_cuotas
    check (cuotas>=0 and cuotas<=10),
    primary key(numerosocio,deporte),
    constraint FK_inscriptos_socio
    foreign key (numerosocio)
    references socios(numero)
    on delete cascade
);

```
## 3
```sql
insert into socios values(1,'23333333','Alberto Paredes','Colon 111');
insert into socios values(2,'24444444','Carlos Conte','Sarmiento 755');
insert into socios values(3,'25555555','Fabian Fuentes','Caseros 987');
insert into socios values(4,'26666666','Hector Lopez','Sucre 344');
insert into inscriptos values(1,'tenis',1);
insert into inscriptos values(1,'basquet',2);
insert into inscriptos values(1,'natacion',1);
insert into inscriptos values(2,'tenis',9);
insert into inscriptos values(2,'natacion',1);
insert into inscriptos values(2,'basquet',default);
insert into inscriptos values(2,'futbol',2);
insert into inscriptos values(3,'tenis',8);
insert into inscriptos values(3,'basquet',9);
insert into inscriptos values(3,'natacion',0);
insert into inscriptos values(4,'basquet',10);

```
## 4
```sql
 select nombre,domicilio,
  (select count(*)
    from inscriptos i
    where s.numero=i.numerosocio) as deportes
 from socios s;
```
## 5
```sql
 select nombre,
  (select (count(*)*10)
    from inscriptos i
    where s.numero=i.numerosocio) as total,
  (select sum(i.cuotas)
    from inscriptos i
    where s.numero=i.numerosocio) as pagas
 from socios s;
```
## 6
```sql
 select nombre,
  count(i.deporte)*10 as total,
  sum(i.cuotas) as pagas
  from socios s
  join inscriptos i
  on numero=numerosocio
  group by nombre;
```
