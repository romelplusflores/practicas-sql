# [61. Restricciones foreign key (acciones)](solucionario/solucionario61.md)
# solucionario numero 61 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table clientes;
drop table provincias;

```
## 2
```sql
create table clientes (
    codigo number(5),
    nombre varchar2(30),
    domicilio varchar2(30),
    ciudad varchar2(20),
    codigoprovincia number(2),
    primary key(codigo)
);

create table provincias(
    codigo number(2),
    nombre varchar2(20),
    primary key (codigo)    );

```
## 3
```sql
insert into provincias values(1,'Cordoba');
insert into provincias values(2,'Santa Fe');
insert into provincias values(3,'Misiones');
insert into provincias values(4,'Rio Negro');
insert into clientes values(100,'Perez Juan','San Martin 123','Carlos Paz',1);
insert into clientes values(101,'Moreno Marcos','Colon 234','Rosario',2);
insert into clientes values(102,'Acosta Ana','Avellaneda 333','Posadas',3);

```
## 4
```sql
 alter table clientes
 add constraint FK_clientes_codigoprovincia
  foreign key (codigoprovincia)
  references provincias(codigo)
  on delete set null;
```
## 5
```sql
delete from provincias where codigo=3;
 select *from clientes;

  
```
## 6
```sql
select constraint_name, constraint_type, delete_rule
  from user_constraints
  where table_name='CLIENTES';
```
## 7
```sql
 update provincias set codigo=9 where codigo=2;

```
## 8
```sql
 alter table clientes
  drop constraint FK_CLIENTES_CODIGOPROVINCIA;
```
## 9
```sql
 alter table clientes
 add constraint FK_clientes_codigoprovincia
  foreign key (codigoprovincia)
  references provincias(codigo)
  on delete cascade;
```
## 10
```sql
 select constraint_name, delete_rule
  from user_constraints
  where table_name='CLIENTES' and
  constraint_type= 'R';
```
## 11
```sql
 delete from provincias where codigo=2;

```
## 12
```sql
 select *from provincias;
 select *from clientes;
```
## 13
```sql
 alter table clientes
  drop constraint FK_CLIENTES_CODIGOPROVINCIA;
```
## 14
```sql
 alter table clientes
 add constraint FK_clientes_codigoprovincia
  foreign key (codigoprovincia)
  references provincias(codigo);
```
## 15
```sql
 delete from provincias where codigo=1;

```
## 16
```sql
select constraint_type, delete_rule
  from user_constraints
  where table_name='CLIENTES' and
  constraint_name='FK_CLIENTES_CODIGOPROVINCIA';
```
## 17
```sql
 drop table provincias;

```
## 18
```sql
 alter table clientes
  drop constraint FK_CLIENTES_CODIGOPROVINCIA;
```
## 19
```sql
 drop table provincias;

```