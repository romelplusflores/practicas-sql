# [79. Subconsulta e insert](solucionario/solucionario79.md)
# solucionario numero 79 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql

drop table facturas cascade constraints;
drop table clientes;

```
## 2
```sql
create table clientes(
    codigo number(5),
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(codigo)
);

create table facturas(
    numero number(6) not null,
    fecha date,
    codigocliente number(5) not null,
    total number(6,2),
    primary key(numero),
    constraint FK_facturas_cliente
    foreign key (codigocliente)
    references clientes(codigo)
);

```
## 3
```sql
insert into clientes values(1,'Juan Lopez','Colon 123');
insert into clientes values(2,'Luis Torres','Sucre 987');
insert into clientes values(3,'Ana Garcia','Sarmiento 576');
insert into clientes values(4,'Susana Molina','San Martin 555');
insert into facturas values(1200,'15/04/2017',1,300);
insert into facturas values(1201,'15/04/2017',2,550);
insert into facturas values(1202,'15/04/2017',3,150);
insert into facturas values(1300,'20/04/2017',1,350);
insert into facturas values(1310,'22/04/2017',3,100);

```
## 4
```sql
drop table clientespref;
 
create table clientespref(
    nombre varchar2(30),
    domicilio varchar2(30)
);

```
## 5
```sql
 insert into clientespref
  select nombre,domicilio
   from clientes 
   where codigo in 
    (select codigocliente
     from clientes c
     join facturas f
     on codigocliente=codigo
     group by codigocliente
     having sum(total)>500);
```
## 6
```sql
 select *from clientespref;

```
