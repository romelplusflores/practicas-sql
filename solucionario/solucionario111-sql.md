# [111. Disparador (old y new)](solucionario/solucionario111.md)
# solucionario numero 111 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table libros;
drop table ofertas;

```
## 2
```sql
create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar(20),
    precio number(6,2)
);

create table ofertas(
    codigo number(6),
    precio number(6,2)
);

```
## 3
```sql
insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(103,'El aleph','Borges','Emece',28);
insert into libros values(105,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
insert into libros values(145,'Alicia en el pais de las maravillas','Carroll','Planeta',35);

```
## 4
```sql
create or replace trigger tr_ingresar_libros_ofertas
 before insert
 on libros
 for each row
 begin
  if (:new.precio<=30) then
   insert into ofertas values(:new.codigo,:new.precio);
  end if;
 end tr_ingresar_libros_ofertas;
```
## 5
```sql
 insert into libros values(150,'El experto en laberintos','Gaskin','Planeta',28);

```
## 6
```sql
  select *from ofertas;

```
## 7
```sql
 insert into libros values(155,'El gato con botas',null,'Planeta',38);

```
## 8
```sql
  select *from ofertas;

```
## 9
```sql
 create or replace trigger tr_modificar_libros_precio
 before update of precio
 on libros
 for each row
 begin
  if (:old.precio<=30) and (:new.precio>30) then
   delete from ofertas where codigo=:old.codigo;
  end if;
  if (:old.precio>30) and (:new.precio<=30) then
   insert into ofertas values(:new.codigo,:new.precio);
  end if;
 end tr_modificar_libros_precio;
```
## 10
```sql
 update libros set precio=50 where codigo=150;

```
## 11
```sql
  select *from libros;
  select *from ofertas;
```
## 12
```sql
 update libros set precio=30 where codigo=155;

```
## 13
```sql
  select *from libros;
  select *from ofertas;
```
## 14
```sql
 update libros set precio=precio+2 where codigo=150;

```
## 15
```sql
  select *from libros;
  select *from ofertas;
```
## 16
```sql
 create or replace trigger tr_eliminar_libros_ofertas
 before delete
 on libros
 for each row
 begin
   delete from ofertas where codigo=:old.codigo;
 end tr_eliminar_libros_ofertas;
```
## 17
```sql
 delete from libros where codigo=155;

```
## 18
```sql
 select *from libros;
  select *from ofertas
```
## 19
```sql
 delete from libros where codigo=150;

```
## 20
```sql
  select *from libros;
  select *from ofertas;
```
## 21
```sql
create table control(
  codigo number(6),
  fecha date,
  precio number(6,2)
 );
```
## 22
```sql
 create or replace trigger tr_actualizar_precio
  before update of precio
  on libros
  for each row
 begin
  insert into control values(:old.codigo,sysdate,:old.precio);
 end tr_actualizar_precio;
```
## 23
```sql
 update libros set precio=40 where codigo=120;

```
## 24
```sql
 select *from libros where codigo=120;
 select *from control
```
## 25
```sql
 update libros set precio=45 where codigo=120;

```
## 26
```sql
 select *from libros where codigo=120;
 select *from control;
```
## 27
```sql
 update libros set precio=precio+(precio*0.5) where codigo>=120;

```
## 28
```sql
 select *from libros where codigo>120;
 select *from control;
```
## 29
```sql
 select *from user_triggers where trigger_name ='TR_ACTUALIZAR_PRECIO';

```

