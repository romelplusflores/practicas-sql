# [67. Agregar campos (alter table-add)](solucionario/solucionario67.md)
# solucionario numero 67 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table empleados;

create table empleados(
    apellido varchar2(20),
    nombre varchar2(20) not null,
    domicilio varchar2(30)
);

```
## 2
```sql
alter table empleados
  add fechaingreso date;
```
## 3
```sql
 describe empleados;
```
## 4
```sql
 alter table empleados
  add seccion varchar2(30) not null;

   describe empleados;

```
## 5
```sql
insert into empleados values('Lopez','Juan','Colon 123','10/10/1980','Contaduria');
insert into empleados values('Gonzalez','Juana','Avellaneda 222','01/05/1990','Sistemas');
insert into empleados values('Perez','Luis','Caseros 987','12/09/2000','Secretaria');

```
## 6
```sql
 alter table empleados
  add sueldo number(6,2) not null;
```
## 7
```sql
 alter table empleados
  add sueldo number(6,2) default 0 not null;
```
## 8
```sql
 describe empleados;

```