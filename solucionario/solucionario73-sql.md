# [73. Subconsultas con in](solucionario/solucionario73.md)
# solucionario numero 73 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table clientes;
drop table ciudades;

```
## 2
```sql
create table ciudades(
    codigo number(2),
    nombre varchar2(20),
    primary key (codigo)
);

create table clientes (
    codigo number(4),
    nombre varchar2(30),
    domicilio varchar2(30),
    codigociudad number(2) not null,
    primary key(codigo),
    constraint FK_clientes_ciudad
    foreign key (codigociudad)
    references ciudades(codigo)
    on delete cascade
);

```
## 3
```sql
insert into ciudades values(1,'Cordoba');
insert into ciudades values(2,'Cruz del Eje');
insert into ciudades values(3,'Carlos Paz');
insert into ciudades values(4,'La Falda');
insert into ciudades values(5,'Villa Maria');
insert into clientes values (100,'Lopez Marcos','Colon 111',1);
insert into clientes values (101,'Lopez Hector','San Martin 222',1);
insert into clientes values (105,'Perez Ana','San Martin 333',2);
insert into clientes values (106,'Garcia Juan','Rivadavia 444',3);
insert into clientes values (107,'Perez Luis','Sarmiento 555',3);
insert into clientes values (110,'Gomez Ines','San Martin 666',4);
insert into clientes values (111,'Torres Fabiola','Alem 777',5);
insert into clientes values (112,'Garcia Luis','Sucre 888',5);

```
## 4
```sql
 select nombre
  from ciudades
  where codigo in
   (select codigociudad
     from clientes
     where domicilio like 'San Martin %');
```
## 5
```sql
 select distinct ci.nombre
  from ciudades ci
  join clientes cl
  on codigociudad=ci.codigo
  where domicilio like 'San Martin%';
```
## 6
```sql
 select nombre
  from ciudades
  where codigo not in
   (select codigociudad
     from clientes
     where nombre like 'G%');
```
## 7
```sql
 select codigociudad
  from clientes
  where nombre like 'G%';
```
