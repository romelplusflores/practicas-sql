# [16.ingresar algunos campos ](solucionario/solucionario16.md)
# solucionario numero 16 fecha 7/6/2023
## ejercicio  
## 1
```sql
drop table cuentas;

```
## 2
```sql
create table cuentas(
    numero number(10) not null,
    documento char(8) not null,
    nombre varchar2(30),
    saldo number(9,2)
);
```
## 3
```sql
insert into cuentas values (123456789,'87654321','Ezaud zuares','30000');

```
## 4
```sql
insert into cuentas (numero,documento) values (123456789,'87654321');

```
## 5
```sql
select * from cuentas;

```
## 6
```sql
insert into cuentas (numero,documento,nombre) values (123456789,'87654321','joshep','10000');

```
## 7
```sql
insert into cuentas (numero,documento,nombre) values (123456789,'87654321');

```
## 8
```sql
insert into cuentas  values ('87654321','','jhon','20000');

```
## 9
```sql
select * from cuentas;

```