# [1.crear tablas ](solucionario/solucionario1.md)
## Elimine la tabla "agenda"  Si no existe, un mensaje indicará tal situación.
```sql
PARA ELIMINAR LA TABLA "AGENDA"
drop table agenda;
PARA SABER SI LA TABLA YA A SIDO BORRADO
select * from all_tables;

SALIDA DEL CODIGO ELIMINE LA TABLA"AGENDA"

Table AGENDA borrado.

CUANDO EXISTE EL MENSAJE APARESE ASI:

TEDDY2	AGENDA	SENATI	VALID...

PERO AHORA ESE MENSAJE YA NO APARESE EN LA  TABLA
```
## Intente crear una tabla llamada "*agenda"
```sql

create table*agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);


LA SALIDA DEL CODIGO CREATE TABLA "AGENDA"

Informe de error -
ORA-00903: nombre de tabla no válido
00903. 00000 -  "invalid table name"
*Cause:    
*Action:
```
# LA CORRECCION DE LA TABLA "AGENDA"
```sql

create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);

LA SALIDA DEL CODIGO CREATE TABLA "AGENDA"

Table AGENDA creado.
```
## Cree una tabla llamada "agenda", debe tener los siguientes campos: apellido, varchar2(30); nombre, varchar2(20); domicilio, varchar2 (30) y telefono, varchar2(11)
```SQL
create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);
LA SALIDA DEL CODIGO ES:

Table AGENDA creado.
EL RESULTADO DE LA CREACION DE AGENDA 
AGENDA
      APELLIDO
      NOMBRE
      DOMICILIO
      TELEFONO 

```


## Intente crearla nuevamente. Aparece mensaje de indicando que el nombre ya lo tiene otro objeto.
```SQL
Informe de error -
ORA-00955: este nombre ya lo está utilizando otro objeto existente
00955. 00000 -  "name is already used by an existing object"
*Cause:    
*Action:
```

## Visualice las tablas existentes (all_tables) La tabla "agenda" aparece en la lista.
```SQL

select * from all_tables;

LA TABLA "AGENDA" SI APARECE EN LA LISTA 
TEDDY2	AGENDA	SENATI....
```

## Visualice la estructura de la tabla "agenda" (describe) Aparece la siguiente tabla:
```SQL

DESCRIBE agenda;

EL RESULTADO DEL CODIGO:
Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
APELLIDO         VARCHAR2(30) 
NOMBRE           VARCHAR2(20) 
DOMICILIO        VARCHAR2(30) 
TELEFONO         VARCHAR2(11) 
```
# Ejercicio 02
###### Necesita almacenar información referente a los libros de su biblioteca personal. Los datos que guardará serán: título del libro, nombre del autor y nombre de la editorial.

#### Elimine la tabla "libros" Si no existe, un mensaje indica tal situación.
```SQL
Error que empieza en la línea: 8 del comando :
drop table libros
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
```

#### Verifique que la tabla "libros" no existe (all_tables) No aparece en la lista.
```SQL
la tabla libros no aparece en la lista 
```

#### Cree una tabla llamada "libros". Debe definirse con los siguientes campos: titulo, varchar(20); autor, varchar2(30) y editorial, varchar2(15)
```SQL
create table libros(
    titulo varchar(20),
    autor varchar2(30),
    editorial varchar2(15)
    );
la salida de crear la tabla "libros"
Table LIBROS creado.
```

#### Intente crearla nuevamente: Aparece mensaje de error indicando que existe un objeto con el nombre "libros".
```SQL
create table libros(
    titulo varchar(20),
    autor varchar2(30),
    editorial varchar2(15)
    );

salida del codigo:

Informe de error -
ORA-00955: este nombre ya lo está utilizando otro objeto existente
00955. 00000 -  "name is already used by an existing object"
*Cause:    
*Action:
```

#### Visualice las tablas existentes
```SQL
select * from all_tables

salida del codigo:
TEDDY2	LIBROS	SENATI...
TEDDY2	AGENDA	SENATI...
```

#### Visualice la estructura de la tabla "libros": Aparece "libros" en la lista.
```SQL
DESCRIBE libros;

salida del codigo:
Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
TITULO           VARCHAR2(20) 
AUTOR            VARCHAR2(30) 
EDITORIAL        VARCHAR2(15) 
```

#### Elimine la tabla
```SQL
drop table libros;

salida del codigo:
Table LIBROS borrado.
```

#### Intente eliminar la tabla Un mensaje indica que no existe.
```SQL
drop table libros;

salida del codigo:

Error que empieza en la línea: 7 del comando :
drop table libros
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
```
