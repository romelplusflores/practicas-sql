# [17.valores por defecto ](solucionario/solucionario17.md)
# solucionario numero 17 fecha 7/6/2023
## ejercicio numero 1
## 1
```sql
drop table visitantes;

```
## 2
```sql
create table visitantes(
    nombre varchar2(30),
    edad number(2),
    sexo char(1) default 'f',
    domicilio varchar2(30),
    ciudad varchar2(20) default 'Cordoba',
    telefono varchar(11),
    mail varchar(30) default 'no tiene',
    montocompra number (6,2)
);
```
## 3
```sql
select column_name,nullable,data_default
from user_tab_columns where TABLE_NAME = 'VISITANTES';

```
## 4
```sql
insert into visitantes (domicilio,ciudad,telefono,mail,montocompra)
values ('Colon 123','Cordoba','4334455','<juanlopez@hotmail.com>',59.80);
insert into visitantes (nombre,edad,sexo,telefono,mail,montocompra)
values ('Marcos Torres',29,'m','4112233','<marcostorres@hotmail.com>',60);
insert into visitantes (nombre,edad,sexo,domicilio,ciudad)
values ('Susana Molina',43,'f','Bulnes 345','Carlos Paz');

```
## 5
```sql
select * from visitantes;

```
## 6
```sql
insert into visitantes (nombre,edad,sexo,domicilio,ciudad,telefono,mail,montocompra)
values ('jhon suares',50,default,'bulnes 123',default,'123456789',default,50);

```
## 7
```sql
select * from visitantes;

```

## ejercicio numero 2

## 1
```sql
drop table prestamos;

```
## 2
```sql
create table prestamos(
    titulo varchar2(40) not null,
    documento char(8) not null,
    fechaprestamo date not null,
    fechadevolucion date,
    devuelto char(1) default 'n'
);

```
## 3
```sql
select column_name,nullable,data_default
from user_tab_columns where TABLE_NAME = 'PRESTAMOS';
```
## 4
```sql
INSERT INTO prestamos (titulo, documento, fechaprestamo)
VALUES ('Libro A', '12345678', SYSDATE);

```
## 5
```sql
select * from prestamos;

```
## 6
```sql
INSERT INTO prestamos (titulo, documento, fechaprestamo, devuelto)
VALUES ('Libro D', '65432109', SYSDATE,default );

```
## 7
```sql
INSERT INTO prestamos (titulo, documento, fechaprestamo, )
VALUES ('Libro D', '65432109', default );

```