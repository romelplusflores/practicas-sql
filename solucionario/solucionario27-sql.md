# [27. Búsqueda de patrones (like - not like)](solucionario/solucionario27.md)
# solucionario numero 27 fecha 13/6/2023 
## ejercicio numero 1
## 1
```sql
drop table empleados;

```
## 2
```sql
create table empleados(
    nombre varchar2(30),
    documento char(8) not null,
    domicilio varchar2(30),
    fechaingreso date,
    seccion varchar2(20),
    sueldo number(6,2),
    primary key(documento)
);

```
## 3
```sql
insert into empleados
values('Juan Perez','22333444','Colon 123','08/10/1990','Gerencia',900.50);

insert into empleados
values('Ana Acosta','23444555','Caseros 987','18/12/1995','Secretaria',590.30);

insert into empleados
values('Lucas Duarte','25666777','Sucre 235','15/05/2005','Sistemas',790);

insert into empleados
values('Pamela Gonzalez','26777888','Sarmiento 873','12/02/1999','Secretaria',550);

insert into empleados
values('Marcos Juarez','30000111','Rivadavia 801','22/09/2002','Contaduria',630.70);

insert into empleados
values('Yolanda perez','35111222','Colon 180','08/10/1990','Administracion',400);

insert into empleados
values('Rodolfo perez','35555888','Coronel Olmedo 588','28/05/1990','Sistemas',800);

```
## 4
```sql
SELECT *
FROM empleados
WHERE nombre LIKE 'Perez%'
      AND NOT nombre LIKE 'perez%'
LIMIT 1;

```
## 5
```sql
SELECT *
FROM empleados
WHERE domicilio LIKE 'Co%' AND domicilio LIKE '%8%'
LIMIT 2;

```
## 6
```sql
SELECT *
FROM empleados
WHERE documento LIKE '%0' OR documento LIKE '%4'
LIMIT 2;

```
## 7
```sql
SELECT *
FROM empleados
WHERE NOT documento LIKE '2%' AND nombre LIKE '%ez'
LIMIT 1;

```
## 8
```sql
SELECT nombre
FROM empleados
WHERE nombre LIKE '%G%' OR nombre LIKE '%J%' OR apellido LIKE '%G%' OR apellido LIKE '%J%'
LIMIT 3;

```
## 9
```sql
SELECT nombre, seccion
FROM empleados
WHERE (seccion LIKE 'S_______' OR seccion LIKE 'G_______') AND LENGTH(seccion) = 8
LIMIT 3;

```
## 10
```sql
SELECT nombre, seccion
FROM empleados
WHERE seccion NOT LIKE 'S%'
LIMIT 3;

```
## 11
```sql
SELECT nombre, sueldo
FROM empleados
WHERE sueldo LIKE '5__.__' OR sueldo LIKE '59__._'
LIMIT 2;

```
## 12
```sql
SELECT *
FROM empleados
WHERE fecha_ingreso >= '1990-01-01' AND fecha_ingreso < '2000-01-01'
LIMIT 5;

```
## 13
```sql
UPDATE empleados
SET sueldo = sueldo + 0.50
WHERE sueldo = ROUND(sueldo, 0)
LIMIT 4;

```
## 14
```sql
DELETE FROM empleados
WHERE apellido LIKE 'p%'
LIMIT 2;

```