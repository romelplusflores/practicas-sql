# [57. Restricciones (foreign key)](solucionario/solucionario57.md)
# solucionario numero 57 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table clientes;
drop table provincias;

create table clientes (
    codigo number(5),
    nombre varchar2(30),
    domicilio varchar2(30),
    ciudad varchar2(20),
    codigoprovincia number(2)
);

create table provincias(
    codigo number(2),
    nombre varchar2(20)
);

```
## 2
```sql
 alter table clientes
 add constraint FK_clientes_codigoprovincia
  foreign key (codigoprovincia)
  references provincias(codigo);
```
## 3
```sql
 alter table provincias
 add constraint UQ_provincias_codigo
  unique (codigo);
```
## 4
```sql
insert into provincias values(1,'Cordoba');
insert into provincias values(2,'Santa Fe');
insert into provincias values(3,'Misiones');
insert into provincias values(4,'Rio Negro');
insert into clientes values(100,'Perez Juan','San Martin 123','Carlos Paz',1);
insert into clientes values(101,'Moreno Marcos','Colon 234','Rosario',2);
insert into clientes values(102,'Acosta Ana','Avellaneda 333','Posadas',3);
insert into clientes values(103,'Luisa Lopez','Juarez 555','La Plata',6);

```
## 5
```sql
alter table clientes
 add constraint FK_clientes_codigoprovincia
  foreign key (codigoprovincia)
  references provincias(codigo);
```
## 6
```sql
 delete from clientes where codigoprovincia=6;

 alter table clientes
 add constraint FK_clientes_codigoprovincia
  foreign key (codigoprovincia)
  references provincias(codigo);

```
## 7
```sql
 

  insert into clientes values(104,'Garcia Marcos','Colon 877','Lules',9);
Oracle no lo permite.
```
## 8
```sql
 delete from provincias where codigo=3;

```
## 9
```sql
 delete from provincias where codigo=4;

```
## 10
```sql
 update provincias set codigo=7 where codigo=1;

```
## 11
```sql
 select constraint_name, constraint_type
  from user_constraints
  where table_name='CLIENTES';

```
## 12
```sql
 select constraint_name, constraint_type
  from user_constraints
  where table_name='PROVINCIAS';
```
## 13
```sql
 drop table provincias;

```
## 14
```sql
 alter table clientes
  drop constraint FK_CLIENTES_CODIGOPROVINCIA;
  
 drop table provincias;
```