# [119. Permiso de conexión](solucionario/solucionario119.md)

# solucionario numero 119 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
 drop user director cascade;

```
## 2
```sql
 create user director identified by dire
 default tablespace system
 quota 100M on system;
```
## 3
```sql
 drop user profesor cascade;

```
## 4
```sql
create user profesor identified by maestro
 default tablespace system;
```
## 5
```sql
 drop user alumno cascade;

```
## 6
```sql
 create user alumno identified by alu
 default tablespace system;
```
## 7
```sql
 select username, password, default_tablespace, created from dba_users;

```
## 8
```sql
 select grantee, privilege from dba_sys_privs where GRANTEE='DIRECTOR' or grantee='PROFESOR' or grantee='ALUMNO';

```
## 9
```sql
grant create session
  to director;
```
## 10
```sql
 grant create session
  to profesor;
```
## 11
```sql
 select grantee,privilege from dba_sys_privs 
  where grantee='DIRECTOR' or grantee='PROFESOR' or grantee='ALUMNO';
"director" y "profesor" tienen permiso "create session".
```
## 12
```sql
 select username, privilege from user_sys_privs;

```
## 13
```sql
 select user from dual;

```
## 14
```sql
 select user from dual;

```
## 15
```sql
 grant create session
  to alumno;
```
## 16
```sql
select grantee,privilege from dba_sys_privs 
  where grantee='ALUMNO';
```
## 17
```sql
 select username,privilege from user_sys_privs;

```
## 18
```sql
 select user from dual;

```

