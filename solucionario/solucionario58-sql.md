# [58. Restricciones foreign key en la misma tabla](solucionario/solucionario58.md)
# solucionario numero 58 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table clientes;
 
create table clientes(
    codigo number(5),
    nombre varchar2(30),
    domicilio varchar2(30),
    ciudad varchar2(20),
    referenciadopor number(5),
    primary key(codigo)
);

```
## 2
```sql
insert into clientes values (50,'Juan Perez','Sucre 123','Cordoba',null);
insert into clientes values(90,'Marta Juarez','Colon 345','Carlos Paz',null);
insert into clientes values(110,'Fabian Torres','San Martin 987','Cordoba',50);
insert into clientes values(125,'Susana Garcia','Colon 122','Carlos Paz',90);
insert into clientes values(140,'Ana Herrero','Colon 890','Carlos Paz',9);

```
## 3
```sql
alter table clientes
  add constraint FK_clientes_referenciadopor
  foreign key (referenciadopor)
  references clientes (codigo);
```
## 4
```sql
update clientes set referenciadopor=90 where referenciadopor=9;

```
## 5
```sql
 alter table clientes
  add constraint FK_clientes_referenciadopor
  foreign key (referenciadopor)
  references clientes (codigo);
```
## 6
```sql
select constraint_name, constraint_type,search_condition
  from user_constraints
  where table_name='CLIENTES';
```
## 7
```sql
 insert into clientes values(150,'Karina Gomez','Caseros 444','Cruz del Eje',8);

```
## 8
```sql
 update clientes set codigo=180 where codigo=90;

```
## 9
```sql
 delete from clientes where nombre='Marta Juarez';

```
## 10
```sql
 update clientes set codigo=180 where codigo=125;

```
## 11
```sql
 delete from clientes where codigo=110;

```