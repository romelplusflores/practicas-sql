# [54. Combinar más de 2 tablas](solucionario/solucionario54.md)
# solucionario numero 54 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table socios;
drop table deportes;
drop table inscriptos;

```
## 2
```sql
create table socios(
    documento char(8) not null, 
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(documento)
);

create table deportes(
    codigo number(2),
    nombre varchar2(20),
    profesor varchar2(15),
    primary key(codigo)
);

create table inscriptos(
    documento char(8) not null, 
    codigodeporte number(2) not null,
    año char(4),
    matricula char(1),--'s'=paga, 'n'=impaga
    primary key(documento,codigodeporte,año)
);

```
## 3
```sql
insert into socios values('22222222','Ana Acosta','Avellaneda 111');
insert into socios values('23333333','Betina Bustos','Bulnes 222');
insert into socios values('24444444','Carlos Castro','Caseros 333');
insert into socios values('25555555','Daniel Duarte','Dinamarca 44');

```
## 4
```sql
insert into deportes values(1,'basquet','Juan Juarez');
insert into deportes values(2,'futbol','Pedro Perez');
insert into deportes values(3,'natacion','Marina Morales');
insert into deportes values(4,'tenis','Marina Morales');

```
## 5
```sql
insert into inscriptos values ('22222222',3,'2016','s');
insert into inscriptos values ('23333333',3,'2016','s');
insert into inscriptos values ('24444444',3,'2016','n');

```
## 6
```sql
insert into inscriptos values ('22222222',3,'2015','s');
insert into inscriptos values ('22222222',3,'2017','n');

```
## 7
```sql
insert into inscriptos values ('24444444',1,'2016','s');
insert into inscriptos values ('24444444',2,'2016','s');

```
## 8
```sql
insert into inscriptos values ('26666666',0,'2016','s');

```
## 9
```sql
 select s.nombre,d.nombre,año
  from deportes d
  right join inscriptos i
  on codigodeporte=d.codigo
  left join socios s
  on i.documento=s.documento;
```
## 10
```sql
 select s.nombre,d.nombre,año,matricula
  from deportes d
  full join inscriptos i
  on codigodeporte=d.codigo
  full join socios s
  on s.documento=i.documento;
```
## 11
```sql
select s.nombre,d.nombre,año,matricula
  from deportes d
  join inscriptos i
  on codigodeporte=d.codigo
  join socios s
  on s.documento=i.documento
  where s.documento='22222222';
```