# [87. Vistas (with check option)](solucionario/solucionario87.md)
# solucionario numero 87 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table clientes;

```
## 2
```sql
create table clientes(
    nombre varchar2(40),
    documento char(8),
    domicilio varchar2(30),
    ciudad varchar2(30)
);

```
## 3
```sql
insert into clientes values('Juan Perez','22222222','Colon 1123','Cordoba');
insert into clientes values('Karina Lopez','23333333','San Martin 254','Cordoba');
insert into clientes values('Luis Garcia','24444444','Caseros 345','Cordoba');
insert into clientes values('Marcos Gonzalez','25555555','Sucre 458','Santa Fe');
insert into clientes values('Nora Torres','26666666','Bulnes 567','Santa Fe');
insert into clientes values('Oscar Luque','27777777','San Martin 786','Santa Fe');
insert into clientes values('Pedro Perez','28888888','Colon 234','Buenos Aires');
insert into clientes values('Rosa Rodriguez','29999999','Avellaneda 23','Buenos Aires');

```
## 4
```sql
 create or replace view vista_clientes
 as
  select nombre, ciudad
  from clientes
  where ciudad <>'Cordoba';
```
## 5
```sql
create or replace view vista_clientes2
 as
  select nombre, ciudad
  from clientes
  where ciudad <>'Cordoba'
  with check option;
```
## 6
```sql
 select *from vista_clientes;
 select *from vista_clientes2;
```
## 7
```sql
 update vista_clientes2 set ciudad='Cordoba' where nombre='Pedro Perez';

```
## 8
```sql
 update vista_clientes set ciudad='Cordoba' where nombre='Pedro Perez';

```
## 9
```sql
 update vista_clientes2 set ciudad='Buenos Aires' where nombre='Oscar Luque';

```
## 10
```sql
 select *from vista_clientes2;

```
## 11
```sql
 insert into vista_clientes2 values('Viviana Valdez','Cordoba');

```
## 12
```sql
 insert into vista_clientes values('Viviana Valdez','Cordoba');

```
## 13
```sql
 insert into vista_clientes2 values('Viviana Valdez','Salta');

```
## 14
```sql
 select *from vista_clientes2;

```