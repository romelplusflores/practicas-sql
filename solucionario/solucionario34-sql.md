# [34. Secuencias (create sequence - currval - nextval - drop sequence)](solucionario/solucionario34.md)
# solucionario numero 34 fecha 14/6/2023 
## ejercicio numero 1

## 1
```sql
drop table empleados;

```
## 2
```sql
create table empleados(
    legajo number(3),
    documento char(8) not null,
    nombre varchar2(30) not null,
    primary key(legajo)
);

```
## 3
```sql
DROP SEQUENCE sec_legajoempleados;

```
## 4
```sql
insert into empleados
values (sec_legajoempleados.currval,'22333444','Ana Acosta');

insert into empleados
values (sec_legajoempleados.nextval,'23444555','Betina Bustamante');

insert into empleados
values (sec_legajoempleados.nextval,'24555666','Carlos Caseros');

insert into empleados
values (sec_legajoempleados.nextval,'25666777','Diana Dominguez');

insert into empleados
values (sec_legajoempleados.nextval,'26777888','Estela Esper');

```
## 5
```sql
SELECT * FROM empleados;

```
## 6
```sql
SELECT sec_legajoempleados.CURRVAL FROM dual;

```
## 7
```sql
SELECT sec_legajoempleados.NEXTVAL FROM dual;

```
## 8
```sql
INSERT INTO empleados (legajo, documento, nombre)
VALUES (sec_legajoempleados.CURRVAL, '27888999', 'Fernando Fernández');

```
## 9
```sql
SELECT * FROM empleados;

```
## 10
```sql
SELECT sec_legajoempleados.NEXTVAL FROM dual;

```
## 11
```sql
INSERT INTO empleados (legajo, documento, nombre)
VALUES (112, '28999000', 'Gonzalo González');

```
## 12
```sql
insert into empleados
values (sec_legajoempleados.currval,'29000111','Hector Huerta');

```
## 13
```sql
SELECT sec_legajoempleados.NEXTVAL FROM dual;

```
## 14
```sql
INSERT INTO empleados (legajo, documento, nombre)
VALUES (112, '29999111', 'Héctor Hernández');

```
## 15
```sql
SELECT * FROM empleados;

```
## 16
```sql
SELECT sequence_name FROM user_sequences;

```
## 17
```sql
SELECT object_name, object_type
FROM all_objects
WHERE object_name LIKE '%EMPLEADOS%';

```
## 18
```sql
DROP SEQUENCE sec_legajoempleados;

```
## 19
```sql
SELECT sequence_name
FROM user_sequences;

```