# [105. Disparador de insercion a nivel de fila (insert trigger for each row)](solucionario/solucionario105.md)
# solucionario numero 105 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table empleados;
drop table control;

```
## 2
```sql
create table empleados(
    documento char(8),
    apellido varchar2(30),
    nombre varchar2(30),
    seccion varchar2(20)
);

create table control(
    usuario varchar2(30),
    fecha date
);

```
## 3
```sql
create or replace trigger tr_ingresar_empleados
  before insert
  on empleados
  for each row
 begin
  insert into Control values(user,sysdate);
 end tr_ingresar_empleados;
```
## 4
```sql
 select *from user_triggers where trigger_name ='TR_INGRESAR_EMPLEADOS';

```
## 5
```sql
insert into empleados values('22333444','ACOSTA','Ana','Secretaria');
insert into empleados values('22777888','DOMINGUEZ','Daniel','Secretaria');
insert into empleados values('22999000','FUENTES','Federico','Sistemas');
insert into empleados values('22555666','CASEROS','Carlos','Contaduria');
insert into empleados values('23444555','GOMEZ','Gabriela','Sistemas');
insert into empleados values('23666777','JUAREZ','Juan','Contaduria');

```
## 6
```sql
select *from control;

```
