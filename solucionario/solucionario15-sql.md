# [15.tipos de datos numericos ](solucionario/solucionario15.md)
# solucionario numero 15 fecha 7/6/2023
## ejercicio numero 1
## 1
```sql
drop table cuentas;

```
## 2
```sql
create table cuentas(
    numero number(4) not null,
    documento char(8),
    nombre varchar2(30),
    saldo number(8,2),
    primary key (numero)
);
```
## 3
```sql
insert into cuentas(numero,documento,nombre,saldo) values('1234','25666777','Pedro Perez',500000.60);
insert into cuentas(numero,documento,nombre,saldo) values('2234','27888999','Juan Lopez',-250000);
insert into cuentas(numero,documento,nombre,saldo) values('3344','27888999','Juan Lopez',4000.50);
insert into cuentas(numero,documento,nombre,saldo) values('3346','32111222','Susana Molina',1000);

```
## 4
```sql
select *from cuentas where saldo > '4000';

```
## 5
```sql
select *from cuentas where nombre = 'Juan Lopez';

```
## 6
```sql
select *from cuentas where saldo < '1000';

```
## 7
```sql
select *from cuentas where numero >= '3000';

```

## ejercicio numero 2

## 1
```sql
drop table empleados;

```
## 2
```sql
create table empleados(
    nombre varchar2(30),
    documento char(8),
    sexo char(1),
    domicilio varchar2(30),
    sueldobasico number(7,2),--máximo estimado 99999.99
    cantidadhijos number(2)--no superará los 99
);
```
## 3
```sql
insert into empleados (nombre,documento,sexo,domicilio,sueldobasico,cantidadhijos) values ('Juan Perez','22333444','m','Sarmiento 123',500,2);
insert into empleados (nombre,documento,sexo,domicilio,sueldobasico,cantidadhijos) values ('Ana Acosta','24555666','f','Colon 134',850,0);
insert into empleados (nombre,documento,sexo,domicilio,sueldobasico,cantidadhijos) values ('Bartolome Barrios','27888999','m','Urquiza 479',10000.80,4);

```
## 4
```sql
insert into empleados (nombre,documento,sexo,domicilio,sueldobasico,cantidadhijos) values ('Bart Barrios','27888999','m','Urquiza 479',800.89,3);

```
## 5
```sql
insert into empleados (nombre,documento,sexo,domicilio,sueldobasico,cantidadhijos) values ('Anita Agosta','24555666','f','Colon 134',999999.99,2);

```
## 6
```sql
select * from empleados where sueldobasico < '900';

```
## 7
```sql
select * from empleados where cantidadhijos = '3';

```