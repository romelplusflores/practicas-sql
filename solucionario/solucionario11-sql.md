# [11.Operadores relacionales ](solucionario/solucionario11.md)
# solucionario numero 11 fecha 7/6/2023
## ejercicio 1
## 1
```sql
drop table medicamentos;
create table medicamentos(
    codigo number(5) not null,
    nombre varchar2(20) not null,
    laboratorio varchar2(20),
    precio number(5,2),
    cantidad number(3,0) not null
);

salida del codigo:
Table MEDICAMENTOS borrado.
Table MEDICAMENTOS creado.
```
## 2
```sql
describe medicamentos;

salida del codigo:
Nombre      ¿Nulo?   Tipo         
----------- -------- ------------ 
CODIGO      NOT NULL NUMBER(5)    
NOMBRE      NOT NULL VARCHAR2(20) 
LABORATORIO          VARCHAR2(20) 
PRECIO               NUMBER(5,2)  
CANTIDAD    NOT NULL NUMBER(3) 
```
## 3
```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(1,'Sertal gotas',null,null,100);
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(2,'Sertal compuesto',null,8.90,150);
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(3,'Buscapina','Roche',null,200);

salida del codigo:
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.

```
## 4
```sql
select *from medicamentos;

salida del codigo:
1	Sertal gotas			        100
2	Sertal compuesto		 8,9  	150
3	Buscapina	      Roche		    200
``` 
## 5
```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(4,'Bismutol',null,'0',500);

salida del codigo:
1 fila insertadas.

```
## 6
```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(null,'paracetamol',null,'3',900);

salida del codigo:
Error en la línea de comandos : 23 Columna : 79
Informe de error -
Error SQL: ORA-01400: no se puede realizar una inserción NULL en ("SYS"."MEDICAMENTOS"."NOMBRE")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```
## 7
```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(null,'paracetamol',null,'3',900);

salida del codigo:
Error en la línea de comandos : 23 Columna : 77
Informe de error -
Error SQL: ORA-01400: no se puede realizar una inserción NULL en ("SYS"."MEDICAMENTOS"."CODIGO")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```
## 8
```sql
select *from medicamentos where laboratorio is null;

salida del codigo:
Sertal gotas	  null		       100
Sertal compuesto  null	 	8,9	   150
Bismutol		  null	      0	   500
```
## 9
```sql
select *from medicamentos where precio is null;
select *from medicamentos where precio= '0';
salida del codigo:
1	Sertal gotas null	null	100
3	Buscapina	Roche	null	200

4   Bismutol		     0	    500
```
## 10
```sql
select *from medicamentos where laboratorio is not null;

salida del codigo:
Sertal compuesto		8,9	   150
ceteresina		        1,5    250
```
## 11
```sql
select *from medicamentos where precio > '0';
select *from medicamentos where precio is not null;
salida del codigo:
2	Sertal compuesto		8,9	150
5	ceteresina		        1,5	250

2	Sertal compuesto		8,9	150
4	Bismutol		        0	500
5	ceteresina		        1,5	250
```
## 12
```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(5,'ceteresina','','1,5',250);

salida del codigo:
1 fila insertadas.

```
## 13
```sql
select *from medicamentos where laboratorio is null;
select *from medicamentos where precio= ' 0 ';
salida del codigo:
1	Sertal gotas		null	null 100
2	Sertal compuesto	null	8,9	 150
4	Bismutol		    null    0	 500
5	ceteresina		    null    1,5	 250
5	ceteresina		    null    1,5	 250

4	Bismutol		    null     0	 500
```
## 14
```sql
select *from medicamentos where laboratorio= ' ';
select *from medicamentos where laboratorio is not null;
```
## ejercicio numero 2
## 1
```sql
drop table peliculas;
```
## 2
```sql
create table peliculas(
    codigo number(4) not null,
    titulo varchar2(40) not null,
    actor varchar2(20),
    duracion number(3)
);

```
## 3
```sql
describe peliculas;

```
## 4
```sql
insert into peliculas (codigo,titulo,actor,duracion) values(1,'Mision imposible','Tom Cruise',120);
insert into peliculas (codigo,titulo,actor,duracion) values(2,'Harry Potter y la piedra filosofal',null,180);
insert into peliculas (codigo,titulo,actor,duracion) values(3,'Harry Potter y la camara secreta','Daniel R.',null);
insert into peliculas (codigo,titulo,actor,duracion) values(0,'Mision imposible 2','',150);
insert into peliculas (codigo,titulo,actor,duracion) values(4,'Titanic','L. Di Caprio',220);
insert into peliculas (codigo,titulo,actor,duracion) values(5,'Mujer bonita','R. Gere.J. Roberts',0);

```
## 5
```sql
select *from peliculas;

```
## 6
```sql
insert into peliculas (codigo,titulo,actor,duracion) values('','Mision imposible','Tom Cruise',120);

```
## 7
```sql
select *from medicamentos where laboratorio is null;

```
## 8 
```sql
select *from peliculas where duracion is null;

```
## 9
```sql
update peliculas set actor = 'desconocido' where actor is null;

```
## 10
```sql
select *from peliculas;
```
## 11
```sql
select *from peliculas where actor is null;

```
## 12
```sql
update peliculas set duracion = null where duracion= '0';

```
## 13
```sql
select * from peliculas;

```
## 14
```sql
delete from peliculas where duracion is null;

```
## 15
```sql
select *from peliculas;

```