# [80. Crear tabla a partir de otra (create table-select)](solucionario/solucionario80.md)
# solucionario numero 80 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table empleados;
drop table sucursales;

```
## 2
```sql
create table sucursales( 
    codigo number(4),
    ciudad varchar2(30) not null,
    primary key(codigo)
);

```
## 3
```sql
create table empleados( 
    documento char(8) not null,
    nombre varchar2(30) not null,
    domicilio varchar2(30),
    seccion varchar2(20),
    sueldo number(6,2),
    codigosucursal number(4),
    primary key(documento),
    constraint FK_empleados_sucursal
    foreign key (codigosucursal)
    references sucursales(codigo)
 );

```
## 4
```sql
insert into sucursales values(1,'Cordoba');
insert into sucursales values(2,'Villa Maria');
insert into sucursales values(3,'Carlos Paz');
insert into sucursales values(4,'Cruz del Eje');
insert into empleados values('22222222','Ana Acosta','Avellaneda 111','Secretaria',500,1);
insert into empleados values('23333333','Carlos Caseros','Colon 222','Sistemas',800,1);
insert into empleados values('24444444','Diana Dominguez','Dinamarca 333','Secretaria',550,2);
insert into empleados values('25555555','Fabiola Fuentes','Francia 444','Sistemas',750,2);
insert into empleados values('26666666','Gabriela Gonzalez','Guemes 555','Secretaria',580,3);
insert into empleados values('27777777','Juan Juarez','Jujuy 777','Secretaria',500,4);
insert into empleados values('28888888','Luis Lopez','Lules 888','Sistemas',780,4);
insert into empleados values('29999999','Maria Morales','Marina 999','Contaduria',670,4);

```
## 5
```sql
 select documento,nombre,domicilio,seccion,sueldo,ciudad
  from empleados
  join sucursales
  on codigosucursal=codigo;
```
## 6
```sql
drop table secciones;

create table secciones as
  (select distinct seccion as nombre
   from empleados);

```
## 7
```sql
 select *from secciones;

```
## 8
```sql
drop table sueldosxseccion;

create table sueldosxseccion as
  (select seccion, sum(sueldo) as total
  from empleados
  group by seccion);

```
## 9
```sql
 select *from sueldosxseccion;

```
## 10
```sql
 drop table sucursalCordoba;

 create table sucursalCordoba as
 (select nombre,ciudad
  from empleados
  join sucursales
  on codigosucursal=codigo
  where ciudad='Cordoba');
```
## 11
```sql
 select *from sucursalCordoba;

```