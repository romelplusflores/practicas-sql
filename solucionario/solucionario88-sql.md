# [88. Vistas (otras consideraciones: force)](solucionario/solucionario88.md)
# solucionario numero 88 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table clientes;

```
## 2
```sql
drop view vista_clientes;

```
## 3
```sql
 create or replace view vista_clientes
 as
  select nombre, ciudad
  from clientes
  where ciudad ='Cordoba';
```
## 4
```sql
 create or replace force view vista_clientes
 as
  select nombre, ciudad
  from clientes
  where ciudad ='Cordoba';
```
## 5
```sql
create table clientes(
    nombre varchar2(40),
    documento char(8),
    domicilio varchar2(30),
    ciudad varchar2(30)
);

```
## 6
```sql
insert into clientes values('Juan Perez','22222222','Colon 1123','Cordoba');
insert into clientes values('Karina Lopez','23333333','San Martin 254','Cordoba');
insert into clientes values('Luis Garcia','24444444','Caseros 345','Cordoba');
insert into clientes values('Marcos Gonzalez','25555555','Sucre 458','Santa Fe');
insert into clientes values('Nora Torres','26666666','Bulnes 567','Santa Fe');
insert into clientes values('Oscar Luque','27777777','San Martin 786','Santa Fe');
insert into clientes values('Pedro Perez','28888888','Colon 234','Buenos Aires');
insert into clientes values('Rosa Rodriguez','29999999','Avellaneda 23','Buenos Aires');

```
## 7
```sql
 create or replace view vista_clientes
 as
  select *from clientes;
```
## 8
```sql
select *from vista_clientes;
```
## 9
```sql
 alter table clientes
 add telefono char(11);
```
## 10
```sql
 select *from vista_clientes;

```
## 11
```sql
create or replace view vista_clientes
 as
  select *from clientes;

```