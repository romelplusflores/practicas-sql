# [78. Subconsulta conupdate y delete](solucionario/solucionario78.md)
# solucionario numero 78 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table empleados;
drop table sucursales;

```
## 2
```sql
create table sucursales( 
    codigo number(2),
    ciudad varchar2(30) not null,
    provincia varchar2(30),
    primary key(codigo)
);

```
## 3
```sql
create table empleados( 
    documento char(8) not null,
    nombre varchar2(30) not null,
    codigosucursal number(2),
    sueldo number(6,2),
    primary key(documento),
    constraint FK_empleados_sucursal
    foreign key (codigosucursal)
    references sucursales(codigo)
);

```
## 4
```sql
insert into sucursales values(1,'Cordoba','Cordoba');
insert into sucursales values(2,'Tucuman','Tucuman');
insert into sucursales values(3,'Carlos Paz','Cordoba');
insert into sucursales values(4,'Cruz del Eje','Cordoba');
insert into sucursales values(5,'La Plata','Buenos Aires');
insert into empleados values('22222222','Ana Acosta',1,500);
insert into empleados values('23333333','Carlos Caseros',1,610);
insert into empleados values('24444444','Diana Dominguez',2,600);
insert into empleados values('25555555','Fabiola Fuentes',5,700);
insert into empleados values('26666666','Gabriela Gonzalez',3,800);
insert into empleados values('27777777','Juan Juarez',4,850);
insert into empleados values('28888888','Luis Lopez',4,500);
insert into empleados values('29999999','Maria Morales',5,800);

```
## 5
```sql
select documento,nombre,sueldo,ciudad,provincia from empleados e
  join sucursales s
  on e.codigosucursal=s.codigo;
```
## 6
```sql
 update empleados set sueldo=sueldo+sueldo*0.1
  where codigosucursal=
  (select codigo from sucursales
   where ciudad='Cruz del Eje');
```
## 7
```sql
 update empleados set sueldo=sueldo+sueldo*0.2
  where codigosucursal in
  (select codigo from sucursales
   where provincia='Cordoba');
```
## 8
```sql
 update empleados set sueldo=
  (select sueldo from empleados
    where nombre='Maria Morales'),
  codigosucursal=
  (select codigo from sucursales
    where ciudad='Carlos Paz')
  where nombre='Ana Acosta';
```
## 9
```sql
 update empleados set (sueldo,codigosucursal)=
  (select sueldo,codigosucursal from empleados
    where nombre='Maria Morales')
  where nombre='Carlos Caseros';
```
## 10
```sql
 delete from empleados
  where codigosucursal in
   (select codigo
    from sucursales
    where provincia='Cordoba');
```