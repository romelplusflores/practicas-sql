# [10.valores nulos ](solucionario/solucionario10.md)
# SOLUCIONARIO NUMERO 10 FECHA 6/6/2023
## EJERCICIO NUMERO 1
## 1
```SQL
drop table medicamentos;
 create table medicamentos(
    codigo number(5) not null,
    nombre varchar2(20) not null,
    laboratorio varchar2(20),
    precio number(5,2),
    cantidad number(3,0) not null
);

SALIDA DEL CODIGO:
Table MEDICAMENTOS borrado.
Table MEDICAMENTOS creado.
```
## 2
```SQL
describe medicamentos;

SALIDA DEL CODIGO:
Nombre      ¿Nulo?   Tipo         
----------- -------- ------------ 
CODIGO      NOT NULL NUMBER(5)    
NOMBRE      NOT NULL VARCHAR2(20) 
LABORATORIO          VARCHAR2(20) 
PRECIO               NUMBER(5,2)  
CANTIDAD    NOT NULL NUMBER(3) 
```
## 3
```SQL
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(1,'Sertal gotas',null,null,100); 
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(2,'Sertal compuesto',null,8.90,150);
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(3,'Buscapina','Roche',null,200);

SALIDA DEL CODIGO:
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
```
## 4
```SQL
select *from medicamentos;

SALIDA DEL CODIGO:
CODIGO NOMBRE            LABORATORIO    PRECIO   CANTIDAD
1	   Sertal gotas			                     100
2	   Sertal compuesto		             8,9	 150
3	   Buscapina	     Roche		             200
```
## 5
```SQL
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values('3','Buscapina','','0','200');

SALIDA DEL CODIGO:
1 fila insertadas.

```
## 6
```SQL
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values('3','','Roche','','200');

SALIDA DEL CODIGO:
Error en la línea de comandos : 23 Columna : 81
Informe de error -
Error SQL: ORA-01400: no se puede realizar una inserción NULL en ("SYS"."MEDICAMENTOS"."NOMBRE")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```
## 7
```SQL
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values('3','','','','150');

SALIDA DEL CODIGO:
Error en la línea de comandos : 25 Columna : 81
Informe de error -
Error SQL: ORA-01400: no se puede realizar una inserción NULL en ("SYS"."MEDICAMENTOS"."NOMBRE")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```
## 8
```SQL
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values('3','Bismutol','','1,5','300');

SALIDA DEL CODIGO:
1 fila insertadas.

```
## 9
```SQL
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values('3','Bismutol','Roche','1,5','300');

SALIDA DEL CODIGO:
1 fila insertadas.

```
## 10
```SQL
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values('3','paracetamol',NULL,'1,5','300');

SALIDA DEL CODIGO:
1 fila insertadas.

```
## ejercicio numero 2
## 1
```sql
 drop table peliculas;

salida del codigo:
Table PELICULAS borrado.

```
## 2
```sql
create table peliculas(
    codigo number(4) not null,
    titulo varchar2(40) not null,
    actor varchar2(20),
    duracion number(3)
);
salida del codigo:
Table PELICULAS creado.

```
## 3
```sql
describe peliculas;

salida del codigo:
Nombre   ¿Nulo?   Tipo         
-------- -------- ------------ 
CODIGO   NOT NULL NUMBER(4)    
TITULO   NOT NULL VARCHAR2(40) 
ACTOR             VARCHAR2(20) 
DURACION          NUMBER(3)
```
## 4 
```sql
insert into peliculas (codigo,titulo,actor,duracion) values(1,'Mision imposible','Tom Cruise',120);
insert into peliculas (codigo,titulo,actor,duracion) values(2,'Harry Potter y la piedra filosofal',null,180);
insert into peliculas (codigo,titulo,actor,duracion) values(3,'Harry Potter y la camara secreta','Daniel R.',null);
insert into peliculas (codigo,titulo,actor,duracion) values(0,'Mision imposible 2','',150);
insert into peliculas (codigo,titulo,actor,duracion) values(4,'Titanic','L. Di Caprio',220);
insert into peliculas (codigo,titulo,actor,duracion) values(5,'Mujer bonita','R. Gere.J. Roberts',0);

salida del codigo:
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.

```
## 5 
```sql
select *from peliculas WHERE actor='';

salida del codigo:
codigo  titulo actor  duracion
```
## 6 
```sql
insert into peliculas (codigo,titulo,actor,duracion) values('','rapidos y furiosos','Vin Diesel ','');

salida del codigo:
Error en la línea de comandos : 25 Columna : 61
Informe de error -
Error SQL: ORA-01400: no se puede realizar una inserción NULL en ("SYS"."PELICULAS"."CODIGO")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```
## 7
```sql
select *from peliculas;

salida del codigo:
cod. TITULO                            ACTOR          DURACION
1	Mision imposible	                Tom Cruise	   120
2	Harry Potter y la piedra filosofal		           180
3	Harry Potter y la camara secreta	Daniel R.	
0	Mision imposible 2		                           150
4	Titanic	                          L.Di Caprio	   220
5	Mujer bonita	              R. Gere.J. Roberts	0
```
## 8
```sql
update peliculas set duracion= null where duracion= '0'

salida del codigo:
1 fila actualizadas.

```
## 9
```sql
select *from peliculas;

salida del codigo:
cod. TITULO                            ACTOR          DURACION
1	Mision imposible	                Tom Cruise	   120
2	Harry Potter y la piedra filosofal		           180
3	Harry Potter y la camara secreta	Daniel R.	
0	Mision imposible 2		                           150
4	Titanic	                          L.Di Caprio	   220
5	Mujer bonita	              R. Gere.J. Roberts	
```
