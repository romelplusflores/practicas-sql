# [49. Combinación externa derecha (right join)](solucionario/solucionario49.md)
# solucionario numero 49 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table clientes;
drop table provincias;

create table clientes (
    codigo number(5),
    nombre varchar2(30),
    domicilio varchar2(30),
    ciudad varchar2(20),
    codigoprovincia number(2),
    primary key(codigo)
);

create table provincias(
    codigo number(2),
    nombre varchar2(20),
    primary key (codigo)
);

```
## 2
```sql
insert into provincias values(1,'Cordoba');
insert into provincias values(2,'Santa Fe');
insert into provincias values(3,'Corrientes');
insert into clientes values (101,'Lopez Marcos','Colon 111','Córdoba',1);
insert into clientes values (102,'Perez Ana','San Martin 222','Cruz del Eje',1);
insert into clientes values (103,'Garcia Juan','Rivadavia 333','Villa Maria',1);
insert into clientes values (104,'Perez Luis','Sarmiento 444','Rosario',2);
insert into clientes values (105,'Gomez Ines','San Martin 666','Santa Fe',2);
insert into clientes values (106,'Torres Fabiola','Alem 777','La Plata',4);
insert into clientes values (107,'Garcia Luis','Sucre 475','Santa Rosa',5);

```
## 3
```sql
 select c.nombre,domicilio,ciudad, p.nombre
  from provincias p
  right join clientes c
  on codigoprovincia = p.codigo;
```
## 4
```sql
 select c.nombre,domicilio,ciudad, p.nombre
  from clientes c
  left join provincias p
  on codigoprovincia = p.codigo;
```
## 5
```sql
 select c.nombre,domicilio,ciudad, p.nombre
  from provincias p
  right join clientes c
  on codigoprovincia = p.codigo
  where p.codigo is not null;
```
## 6
```sql
 select c.nombre,domicilio,ciudad, p.nombre
  from provincias p
  right join clientes c
  on codigoprovincia = p.codigo
  where p.codigo is null
  order by ciudad;
```
