# [32. Registros duplicados (Distinct)](solucionario/solucionario32.md)
# solucionario numero 32 fecha 14/6/2023 
## ejercicio numero 1

## 1
```sql
 drop table clientes;

 create table clientes (
  nombre varchar2(30) not null,
  domicilio varchar2(30),
  ciudad varchar2(20),
  provincia varchar2(20)
);

```
## 2
```sql
insert into clientes
values ('Lopez Marcos','Colon 111','Cordoba','Cordoba');

insert into clientes
values ('Perez Ana','San Martin 222','Cruz del Eje','Cordoba');

insert into clientes
values ('Garcia Juan','Rivadavia 333','Villa del Rosario','Cordoba');

insert into clientes
values ('Perez Luis','Sarmiento 444','Rosario','Santa Fe');

insert into clientes
values ('Pereyra Lucas','San Martin 555','Cruz del Eje','Cordoba');

insert into clientes
values ('Gomez Ines','San Martin 666','Santa Fe','Santa Fe');

insert into clientes
values ('Torres Fabiola','Alem 777','Villa del Rosario','Cordoba');

insert into clientes
values ('Lopez Carlos',null,'Cruz del Eje','Cordoba');

insert into clientes
values ('Ramos Betina','San Martin 999','Cordoba','Cordoba');

insert into clientes
values ('Lopez Lucas','San Martin 1010','Posadas','Misiones');

```
## 3
```sql
SELECT DISTINCT provincia
FROM clientes;
```
## 4
```sql
SELECT COUNT(DISTINCT provincia) AS cantidad_provincias
FROM clientes;

```
## 5
```sql
SELECT DISTINCT ciudad
FROM clientes;
```
## 6
```sql
SELECT COUNT(DISTINCT ciudad) AS cantidad_ciudades
FROM clientes;
```
## 7
```sql
SELECT DISTINCT ciudad
FROM clientes
WHERE provincia = 'Cordoba';

```
## 8
```sql
SELECT provincia, COUNT(DISTINCT ciudad) AS cantidad_ciudades
FROM clientes
GROUP BY provincia;

```

## ejercicio numero 2

## 1
```sql
drop table inmuebles;

```
## 2
```sql
create table inmuebles (
    documento varchar2(8) not null,
    apellido varchar2(30),
    nombre varchar2(30),
    domicilio varchar2(20),
    barrio varchar2(20),
    ciudad varchar2(20),
    tipo char(1),..b=baldio, e: edificado
    superficie number(8,2)
);

```
## 3
```sql
insert into inmuebles
values ('11000000','Perez','Alberto','San Martin 800','Centro','Cordoba','e',100);

insert into inmuebles
values ('11000000','Perez','Alberto','Sarmiento 245','Gral. Paz','Cordoba','e',200);

insert into inmuebles
values ('12222222','Lopez','Maria','San Martin 202','Centro','Cordoba','e',250);

insert into inmuebles
values ('13333333','Garcia','Carlos','Paso 1234','Alberdi','Cordoba','b',200);

insert into inmuebles
values ('13333333','Garcia','Carlos','Guemes 876','Alberdi','Cordoba','b',300);

insert into inmuebles
values ('14444444','Perez','Mariana','Caseros 456','Flores','Cordoba','b',200);

insert into inmuebles
values ('15555555','Lopez','Luis','San Martin 321','Centro','Carlos Paz','e',500);

insert into inmuebles
values ('15555555','Lopez','Luis','Lopez y Planes 853','Flores','Carlos Paz','e',350);

insert into inmuebles
values ('16666666','Perez','Alberto','Sucre 1877','Flores','Cordoba','e',150);

```
## 4
```sql
SELECT DISTINCT apellido
FROM inmuebles;

```
## 5
```sql
SELECT DISTINCT documento
FROM inmuebles
WHERE documento NOT IN (
    SELECT DISTINCT documento
    FROM inmuebles
    WHERE ROWNUM <= 3
);

```
## 6
```sql
SELECT COUNT(DISTINCT CONCAT(apellido, nombre)) AS cantidad_propietarios
FROM inmuebles
WHERE ciudad = 'Cordoba';

```
## 7
```sql
SELECT COUNT(*) AS cantidad_inmuebles
FROM inmuebles
WHERE domicilio LIKE '%San Martin%';

```
## 8
```sql
SELECT COUNT(DISTINCT ciudad) AS cantidad_ciudades
FROM inmuebles
WHERE domicilio LIKE '%San Martin%';

```
## 9
```sql
SELECT apellido, nombre
FROM inmuebles;

```
## 10
```sql
SELECT DISTINCT apellido, nombre
FROM inmuebles;

```
## 11
```sql
SELECT documento, COUNT(*) AS cantidad_inmuebles
FROM inmuebles
WHERE barrio IS NOT NULL
GROUP BY documento;

```
## 12
```sql
SELECT documento, COUNT(DISTINCT barrio) AS cantidad_barrios
FROM inmuebles
GROUP BY documento;

```