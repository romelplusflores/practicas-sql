# [19.alias ](solucionario/solucionario19.md)
# solucionario numero 19 fecha 8/6/2023
## ejercicio 
## 1
```sql
drop table articulos;

```
## 2
```sql
create table articulos(
    codigo number(4),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(8,2),
    cantidad number(3) default 0,
    primary key (codigo)
);

```
## 3
```sql
insert into articulos
values (101,'impresora','Epson Stylus C45',400.80,20);

insert into articulos
values (203,'impresora','Epson Stylus C85',500,30);

insert into articulos
values (205,'monitor','Samsung 14',800,10);

insert into articulos
values (300,'teclado','ingles Biswal',100,50);

```
## 4
```sql
select codigo,nombre,descripcion, precio*0.82 as descuento, precio-(precio*0.82) as "precio final"
from articulos;
```
## 5
```sql
select  nombre as "stock ARTICULOS", descripcion
from articulos;
```
## 6
```sql
select codigo,nombre, descripcion,precio,cantidad as"stock monto total"
from articulos;
```
## 7
```sql
select codigo,nombre, descripcion,precio,cantidad as"impresora con recargo"
from articulos
where nombre = 'impresora';
```