# [109. Disparador de actualización - lista de campos (update trigger)](solucionario/solucionario109.md)
# solucionario numero 109 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table control;
drop table libros;

```
## 2
```sql
create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table control(
    usuario varchar2(30),
    fecha date
);

```
## 3
```sql
insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(103,'El aleph','Borges','Emece',28);
insert into libros values(105,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
insert into libros values(145,'Alicia en el pais de las maravillas','Carroll','Planeta',35);

```
## 4
```sql
alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';

```
## 5
```sql
 create or replace trigger tr_actualizar_precio_libros
  before update of precio,editorial
  on libros
 begin
  insert into control values(user,sysdate);
 end tr_actualizar_precio_libros;
```
## 6
```sql
 select *from user_triggers where trigger_name ='TR_ACTUALIZAR_PRECIO_LIBROS';

```
## 7
```sql
 update libros set precio=precio+precio*0.1 where editorial='Nuevo siglo';

```
## 8
```sql
 select *from control;

```
## 9
```sql
 update libros set editorial='Sudamericana' where editorial='Planeta';

```
## 10
```sql
 select *from control;

```
## 11
```sql
 update libros set autor='Lewis Carroll' where autor='Carroll';

```
## 12
```sql
 select *from libros;

```
## 13
```sql
 select *from control;

```