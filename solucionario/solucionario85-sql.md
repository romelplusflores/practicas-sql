# [85. Vistas (with read only)](solucionario/solucionario85.md)
# solucionario numero 85 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table clientes cascade constraints;

```
## 2
```sql
create table clientes(
    nombre varchar2(40),
    documento char(8),
    domicilio varchar2(30),
    ciudad varchar2(30)
);

```
## 3
```sql
insert into clientes values('Juan Perez','22222222','Colon 1123','Cordoba');
insert into clientes values('Karina Lopez','23333333','San Martin 254','Cordoba');
insert into clientes values('Luis Garcia','24444444','Caseros 345','Cordoba');
insert into clientes values('Marcos Gonzalez','25555555','Sucre 458','Santa Fe');
insert into clientes values('Nora Torres','26666666','Bulnes 567','Santa Fe');
insert into clientes values('Oscar Luque','27777777','San Martin 786','Santa Fe');
insert into clientes values('Pedro Perez','28888888','Colon 234','Buenos Aires');
insert into clientes values('Rosa Rodriguez','29999999','Avellaneda 23','Buenos Aires');

```
## 4
```sql
 create or replace view vista_clientes
 as
  select nombre, ciudad
  from clientes;
```
## 5
```sql
 create or replace view vista_clientes2
 as
  select nombre, ciudad
  from clientes
  with read only;
```
## 6
```sql
 select *from vista_clientes;
 select *from vista_clientes2;
```
## 7
```sql
 insert into vista_clientes2 values ('Ana Acosta','Salta');

```
## 8
```sql
 insert into vista_clientes values ('Ana Acosta','Salta');

```
## 9
```sql
 update vista_clientes2 set ciudad='Salta' where nombre='Juan Perez';

```
## 10
```sql
 update vista_clientes set ciudad='Salta' where nombre='Juan Perez';

```
## 11
```sql
 delete from vista_clientes2 where ciudad='Buenos Aires';

```
## 12
```sql
 delete from vista_clientes2 where ciudad='Buenos Aires';

```