# [70.  Agregar campos y restricciones (alter table)](solucionario/solucionario70.md)
# solucionario numero 70 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table empleados;

create table empleados(
    documento char(8) not null,
    nombre varchar2(10),
    domicilio varchar2(30),
    ciudad varchar2(20) default 'Buenos Aires'
);

```
## 2
```sql
alter table empleados
  add legajo number(3)
  constraint PK_empleados_legajo primary key;
```
## 3
```sql
 describe empleados;
 select *from user_constraints where table_name='EMPLEADOS';
```
## 4
```sql
alter table empleados
  add hijos number(2)
  constraint CK_empleados_hijos check (hijos<=30);
```
## 5
```sql
insert into empleados values('22222222','Juan Lopez','Colon 123','Cordoba',100,2);
insert into empleados values('23333333','Ana Garcia','Sucre 435','Cordoba',200,3);

```
## 6
```sql
alter table empleados
  add sueldo number(6,2) not null
  constraint CK_empleados_sueldo check (sueldo>=0);
```
## 7
```sql
alter table empleados
  add sueldo number(6,2) default 0 not null
  constraint CK_empleados_sueldo check (sueldo>=0);
```
## 8
```sql
select *from empleados;

```
## 9
```sql
 describe empleados;

```
## 10
```sql
 select *from user_constraints where table_name='EMPLEADOS';

```