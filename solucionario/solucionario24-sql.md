# [24. Operadores lógicos (and - or - not)](solucionario/solucionario24.md)
# solucionario numero 24 fecha 8/6/2023 
## ejercicio numero 1

## 1
```sql
drop table medicamentos;



```
## 2
```sql
create table medicamentos(
    codigo number(5),
    nombre varchar2(20),
    laboratorio varchar2(20),
    precio number(5,2),
    cantidad number(3),
    primary key(codigo)
);

```
## 3
```sql
insert into medicamentos
values(100,'Sertal','Roche',5.2,100);

insert into medicamentos
values(102,'Buscapina','Roche',4.10,200);

insert into medicamentos
values(205,'Amoxidal 500','Bayer',15.60,100);

insert into medicamentos
values(230,'Paracetamol 500','Bago',1.90,200);

insert into medicamentos
values(345,'Bayaspirina','Bayer',2.10,150);

insert into medicamentos
values(347,'Amoxidal jarabe','Bayer',5.10,250);
```
## 4
```sql
SELECT codigo, nombre
FROM medicamentos
WHERE laboratorio = 'Roche' AND precio < 5;
       
```
## 5
```sql
SELECT *
FROM medicamentos
WHERE laboratorio = 'Roche' OR precio < 5;
```
## 6
```sql
SELECT *
FROM medicamentos
WHERE laboratorio = 'Bayer' AND cantidad <> 100;

```
## 7
```sql
SELECT nombre
FROM medicamentos
WHERE precio >= 2 AND precio <= 5
LIMIT 2;

```
## 8
```sql
DELETE FROM medicamentos
WHERE laboratorio = 'Bayer' AND precio > 10;

```
## 9
```sql
UPDATE medicamentos
SET cantidad = 200
WHERE laboratorio = 'Roche' AND precio > 5;

```
## 10
```sql
SELECT *
FROM medicamentos;

```
## 11
```sql
DELETE FROM medicamentos
WHERE laboratorio = 'Bayer' OR precio < 3;

```

## ejercicio numero 2

## 1
```sql
drop table peliculas;

create table peliculas(
    codigo number(4),
    titulo varchar2(40) not null,
    actor varchar2(20),
    duracion number(3),
    primary key (codigo)
);

```
## 2
```sql
insert into peliculas
values(1020,'Mision imposible','Tom Cruise',120);

insert into peliculas
values(1021,'Harry Potter y la piedra filosofal','Daniel R.',180);

insert into peliculas
values(1022,'Harry Potter y la camara secreta','Daniel R.',190);

insert into peliculas
values(1200,'Mision imposible 2','Tom Cruise',120);

insert into peliculas
values(1234,'Mujer bonita','Richard Gere',120);

insert into peliculas
values(900,'Tootsie','D. Hoffman',90);

insert into peliculas
values(1300,'Un oso rojo','Julio Chavez',100);

insert into peliculas
values(1301,'Elsa y Fred','China Zorrilla',110);

```
## 3
```sql
SELECT *
FROM peliculas
WHERE actor = 'Tom Cruise' OR actor = 'Richard Gere';

```
## 4
```sql
SELECT *
FROM peliculas
WHERE actor = 'Tom Cruise' AND duracion < 100;

```
## 5
```sql
SELECT titulo
FROM peliculas
WHERE duracion BETWEEN 100 AND 120;

```
## 6
```sql
UPDATE peliculas
SET duracion = 200
WHERE actor = 'Daniel R.' AND duracion = 180;

```
## 7
```sql
SELECT *
FROM peliculas;

```
## 8
```sql
DELETE FROM peliculas
WHERE actor <> 'Tom Cruise' AND duracion >= 100;

```

