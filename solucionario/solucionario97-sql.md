# [97. Control de flujo (if)](solucionario/solucionario97.md)
# solucionario numero 97 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table productos;

```
## 2
```sql
 create table productos(
  codigo number(5),
  precio number(6,2),
  stockminimo number(4),
  stockactual number(4)
 );

```
## 3
```sql
insert into productos values(100,10,100,200);
insert into productos values(102,15,200,500);
insert into productos values(130,8,100,0);
insert into productos values(240,23,100,20);
insert into productos values(356,12,100,250);
insert into productos values(360,7,100,100);
insert into productos values(400,18,150,100);

```
## 4
```sql
create or replace function f_estado(aactual number,aminimo number)
  return varchar2
 is
  estado varchar2(20);
 begin
  estado:='normal';
   if aactual>=aminimo then
    estado:='normal';
   else
    if aactual=0 then estado:='faltante';
    else
      estado:='reponer';
    end if;
   end if;
   return estado;
 end;
```
## 5
```sql
 select codigo, stockactual, stockminimo, f_estado(stockactual,stockminimo) as estado from productos;

```
## 6
```sql
 create or replace function f_estado(aactual number,aminimo number)
  return varchar2
 is
  estado varchar2(20);
 begin
  estado:='normal';
   if aactual>=aminimo then
    estado:='normal';
   elsif
    aactual=0 then estado:='faltante';
   else
      estado:='reponer';
   end if;
   return estado;
 end;
```
## 7
```sql
 select codigo, stockactual, stockminimo, f_estado(stockactual,stockminimo) as estado from productos;

```
## 8
```sql
 create or replace function f_estado(aactual number,aminimo number)
  return varchar2
 is
  estado varchar2(20);
 begin
  estado:='normal';
   if aactual>=aminimo then
    estado:='normal';
   elsif
    aactual=0 then estado:='faltante '||to_char(aminimo);
   else
      estado:='reponer '||to_char(aminimo-aactual);
   end if;
   return estado;
 end;
```
## 9
```sql
 select codigo, stockactual, stockminimo, f_estado(stockactual,stockminimo) as estado from productos;

```