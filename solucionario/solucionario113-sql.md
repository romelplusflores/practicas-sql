# [113. Disparador de actualizacion - campos (updating)](solucionario/solucionario113.md)
# solucionario numero 113 fecha 15/6/2023 
## ejercicio numero 1

## 1
```sql
drop table articulos;
drop table pedidos;
drop table controlPrecios;

```
## 2
```sql
create table articulos(
    codigo number(4),
    descripcion varchar2(40),
    precio number (6,2),
    stock number(4)
);

create table pedidos(
    codigo number(4),
    cantidad number(4)
);

create table controlPrecios(
    fecha date,
    codigo number(4),
    anterior number(6,2),
    nuevo number(6,2)
);

```
## 3
```sql
insert into articulos values(100,'cuaderno rayado 24h',4.5,100);
insert into articulos values(102,'cuaderno liso 12h',3.5,150);
insert into articulos values(104,'lapices color x6',8.4,60);
insert into articulos values(160,'regla 20cm.',6.5,40);
insert into articulos values(173,'compas xxx',14,35);
insert into articulos values(234,'goma lapiz',0.95,200);

```
## 4
```sql
 insert into pedidos (codigo)
   select (codigo) from articulos;
 update pedidos set cantidad=0;
```
## 5
```sql
set serveroutput on;
execute dbms_output.enable(20000);

```
## 6
```sql
create or replace trigger tr_articulos
 before insert or delete or update of stock, precio
 on articulos
 for each row
 begin
  dbms_output.put_line('Trigger disparado');
  if (inserting) then
    insert into pedidos values(:new.codigo,0);
    dbms_output.put_line(' insercion');
  end if; 
  if (deleting) then
    delete from pedidos where codigo = :old.codigo;    
    dbms_output.put_line(' borrado');
  end if; 
  if updating ('STOCK') then
    update pedidos set cantidad=cantidad+(:old.stock - :new.stock) where codigo = :old.codigo;
    dbms_output.put_line(' actualizacion de stock');
  end if;
  if updating('PRECIO') then
    insert into controlPrecios values(sysdate,:old.codigo,:old.precio,:new.precio);
    dbms_output.put_line(' actualizacion de precio');
  end if;
 end tr_articulos;
```
## 7
```sql
 update articulos set stock=30 where codigo=100;

```
## 8
```sql
 select *from pedidos;

```
## 9
```sql
 insert into articulos values(280,'carpeta oficio',15,50);

```
## 10
```sql
 select *from pedidos;

```
## 11
```sql
 delete articulos where codigo=234;

```
## 12
```sql
 select *from pedidos;

```
## 13
```sql
 update articulos set precio=4.8 where codigo=102;

```
## 14
```sql
 select *from controlPrecios;

```
## 15
```sql
 update articulos set descripcion='compas metal xxx' where codigo=173;

```
## 16
```sql
 update articulos set precio=10, stock=55, descripcion='lapices colorx6 Faber' where codigo=104;

```
## 17
```sql
 select *from controlPrecios;
 select *from pedidos;
```
## 18
```sql
 update articulos set stock=10 where codigo>=104;

```
## 19
```sql
 select *from pedidos;

```
## 20
```sql
 update articulos set precio=precio+precio*0.1 where codigo>=104;

```
## 21
```sql
 select *from controlPrecios;

```
## 22
```sql
 delete from articulos where codigo>=104;

```
## 23
```sql
 select *from pedidos;

```
