# [38. Restricción unique](solucionario/solucionario38.md)
# solucionario numero 38 fecha 14/6/2023 
## ejercicio numero 1

## 1
```sql
drop table remis;

```
## 2
```sql
create table remis(
    numero number(5),
    patente char(6),
    marca varchar2(15),
    modelo char(4)
);

```
## 3
```sql
INSERT INTO remis(numero, patente, marca, modelo) VALUES (1, 'ABC123', 'Marca1', 'Mod1');
INSERT INTO remis(numero, patente, marca, modelo) VALUES (2, 'DEF456', 'Marca2', 'Mod2');
INSERT INTO remis(numero, patente, marca, modelo) VALUES (3, 'GHI789', 'Marca3', 'Mod3');
INSERT INTO remis(numero, patente, marca, modelo) VALUES (4, 'ABC123', 'Marca4', 'Mod4'); -- Patente repetida
INSERT INTO remis(numero, patente, marca, modelo) VALUES (5, NULL, 'Marca5', 'Mod5'); -- Patente nula
INSERT INTO remis(numero, patente, marca, modelo) VALUES (6, 'JKL012', 'Marca6', 'Mod6');
INSERT INTO remis(numero, patente, marca, modelo) VALUES (7, 'DEF456', 'Marca7', 'Mod7'); -- Patente repetida

```
## 4
```sql
CREATE TABLE remis (
    numero NUMBER(5),
    patente CHAR(6),
    marca VARCHAR2(15),
    modelo CHAR(4),
    CONSTRAINT pk_remis PRIMARY KEY (numero)
);

```
## 5
```sql
DELETE FROM remis
WHERE ROWID NOT IN (
    SELECT MIN(ROWID)
    FROM remis
    GROUP BY patente
);

```
## 6
```sql
DELETE FROM remis
WHERE ROWID NOT IN (
    SELECT MIN(ROWID)
    FROM remis
    WHERE patente IS NOT NULL
    GROUP BY patente
);

```
## 7
```sql
INSERT INTO remis(numero, patente, marca, modelo) VALUES (8, 'ABC123', 'Marca8', 'Mod8');

```
## 8
```sql
INSERT INTO remis(numero, patente, marca, modelo) VALUES (9, NULL, 'Marca9', 'Mod9');

```
## 9
```sql
SELECT constraint_name, constraint_type, search_condition, r_constraint_name
FROM user_constraints
WHERE table_name = 'REMIS';

```
